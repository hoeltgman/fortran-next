page_dir:   ./pages
src_dir:    ./build/src/
            ./build/bin/apps/
media_dir:  ./media
output_dir: ./doc
project: PDE-based Image Inpainting
project_website: https://gitlab.com/hoeltgman/fortran-next
version: 0.1
summary: Fortran Implementation of Optimal Inpainting Masks
author: Laurent Hoeltgen
author_description: Mathematician and Engineer
email: contact@laurenthoeltgen.name
fpp_extensions: f90
predocmark: >
docmark_alt: #
predocmark_alt: <
display: public
         protected
         private
print_creation_date: true
source: false
graph: false
sort: alpha
coloured_edges: false
search: false
macro: TEST
       LOGIC=.true.
license: gfdl
extra_filetypes: c h

# Introduction

This project provides mathematical image processing tools with a
strong focus on partial differential equation based modelling for
image compression.
\begin{equation}
c (u - f) + (1 - c) \Delta u = 0
\end{equation}

# Installation

This information can also be found in the README file in the top level
folder of the repository.

## Requirements

To compile the code you need a C and a Fortran compiler. The code is
developed with gcc and gfortran. Furthermore, you need

- [fypp](https://github.com/aradi/fypp): fypp is a preprocessor for
  Fortran files. It can easily be installed through pip. Version 2.0.1
  and 2.1 are known to work.

    pip install fypp

- [ford](https://github.com/cmacmackin/ford): The documentation is
  automatically extracted from the Fortran source code files with
  ford. It can be installed with pip. Version 5.0.6 is known to work.

    pip install ford

- [gcovr](<http://gcovr.com/>): Code coverage can be inspected with
  gcovr.

- [fruit](https://sourceforge.net/projects/fortranxunit/): We use
  fruit for unit testing. The necessary code comes with the
  repository. No additional installation is necessary.

## Dependencies

# License

All the code in /src and /bin Folders of the repository is licensed
GPLv3 or later. External libraries are subject to their own licenses.
