! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.

module inpainting_gui
    !! OpenGL GUI for the inpainting app
    use :: iso_c_binding
    use :: iso_fortran_env

    use :: opengl_gl
    use :: opengl_glu
    use :: opengl_glut

    implicit none
    save
    public

    integer(kind=c_int) :: gui_window_width
    integer(kind=c_int) :: gui_window_height
    integer(kind=c_int) :: gui_num_images
    integer(kind=GLint) :: gui_win
    character(len=*), parameter :: gui_window_name = "Inpainting"//char(0)

    real(kind=GLfloat), dimension(:), allocatable, target :: glinputimg !! image data for opengl viewer
    real(kind=GLfloat), dimension(:), allocatable, target :: glmaskimg  !! image data for opengl viewer
    real(kind=GLfloat), dimension(:), allocatable, target :: glrecoimg  !! image data for opengl viewer
    real(kind=GLfloat), dimension(:), allocatable, target :: gldiffimg  !! image data for opengl viewer

contains

    subroutine gui_init()
        call glutinitdisplaymode(GLUT_DOUBLE + GLUT_RGB)
        call glutinitwindowsize(gui_window_width, gui_window_height);
        gui_win = glutcreatewindow(gui_window_name)

        call glviewport(0_GLint, 0_GLint, int(gui_window_width, GLsizei), int(gui_window_width, GLsizei))
        call glmatrixmode(GL_PROJECTION)
        call glloadidentity()
        call glortho(0.0_GLdouble, real(gui_window_width, GLdouble), &
                0.0_GLdouble, real(gui_window_height, GLdouble), -1.0_GLdouble, 1.0_GLdouble)
        call glmatrixmode(GL_MODELVIEW)
        call gldisable(GL_DITHER)
        call glpixelzoom(1.0_GLfloat, 1.0_GLfloat)

        call glclear(GL_COLOR_BUFFER_BIT + GL_DEPTH_BUFFER_BIT)
        call glclearcolor(0.0_GLfloat, 0.0_GLfloat, 0.0_GLfloat, 1.0_GLfloat)

        ! call glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION)
    end subroutine gui_init

    subroutine idle() bind(c)
        call glutPostRedisplay()
    end subroutine idle

    subroutine display() bind(c)
        ! Input image
        call glrasterpos3f(0.0_GLfloat, 0.0_GLfloat, 0.0_GLfloat)
        call gldrawpixels(int(gui_window_width/gui_num_images, GLsizei), int(gui_window_height, GLsizei), &
                GL_LUMINANCE, GL_FLOAT, C_loc(glinputimg))

        ! Mask
        call glrasterpos3f(real(0.25*gui_window_width, GLfloat), 0.0_GLfloat, 0.0_GLfloat)
        call gldrawpixels(int(gui_window_width/gui_num_images, GLsizei), int(gui_window_height, GLsizei), &
                GL_LUMINANCE, GL_FLOAT, C_loc(glmaskimg))

        ! Reconsutruction
        call glrasterpos3f(real(0.5*gui_window_width, GLfloat), 0.0_GLfloat, 0.0_GLfloat)
        call gldrawpixels(int(gui_window_width/gui_num_images, GLsizei), int(gui_window_height, GLsizei), &
                GL_LUMINANCE, GL_FLOAT, C_loc(glrecoimg))

        ! Difference
        call glrasterpos3f(real(0.75*gui_window_width, GLfloat), 0.0_GLfloat, 0.0_GLfloat)
        call gldrawpixels(int(gui_window_width/gui_num_images, GLsizei), int(gui_window_height, GLsizei), &
                GL_LUMINANCE, GL_FLOAT, C_loc(gldiffimg))

        call glutswapbuffers()

        call glutPostRedisplay()
    end subroutine display

end module inpainting_gui

program inpainting
    !! Perform Laplace interpolation
    !!
    !!    ./inpainting -i image.pgm -m mask.pgm -o output.pgm -g
    !!
    use :: iso_fortran_env
    use :: iso_c_binding

    use :: opengl_gl
    use :: opengl_glu
    use :: opengl_glut

    use :: inpainting_gui

    use :: m_option_parser
    use :: bin_utils
    use :: io

    use :: inpainting
    use :: image

    implicit none

    ! option parser variables

    integer :: argc = 0 !! Number of command line parameters.
    integer :: arglen   !! length of command line parameters.
    logical :: gui      !! Whether to launch the gui
    logical :: help     !! Whether to print the help message
    logical :: version  !! Whether to print the version message
    logical :: license  !! Whether to print the license message

    type(option_t), dimension(:), allocatable :: program_options !! storage for command line parameters
    namelist /parameters/ inputfile, outputfile, maskfile        !! namelist to simplify reading parameters

    ! i/o variables

    character(len=256) :: buf  !! Temporary buffer.
    character(len=64)  :: buf2 !! Temporary buffer.
    character(len=64)  :: fmt  !! Format string for I/O.

    character(len=256) :: inputfile     !! Name of input image
    character(len=256) :: outputfile    !! Name of output image
    character(len=256) :: maskfile      !! Name of mask image
    character(len=256) :: parameterfile !! Name of parameterfile

    integer(kind=c_long) :: nr = -1 !! number of rows of input image.
    integer(kind=c_long) :: nc = -1 !! number of columns of input image.
    integer(kind=c_long) :: nd = -1 !! number of channels of input image.

    ! generic variables

    integer              :: ierr          !! return value to check status.
    character(len=256)   :: error_message !! corresponding error message
    integer(kind=c_long) :: ii            !! loop variable
    integer(kind=c_long) :: jj            !! loop variable
    integer(kind=c_long) :: kk            !! loop variable

    real(kind=REAL64),  dimension(:), allocatable :: inputimg !! input image i.e. f
    real(kind=REAL64),  dimension(:), allocatable :: maskimg  !! mask image  i.e. c
    real(kind=REAL64),  dimension(:), allocatable :: recoimg  !! mask image  i.e. u
    real(kind=REAL64),  dimension(:), allocatable :: diffimg  !! mask image  i.e. f-u

    real(kind=REAL64) :: mse, psnr, residual ! Error measures

    allocate(program_options(8), stat = ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    call set_option (program_options, "--input",   "-i", "in.pgm",  "input image")
    call set_option (program_options, "--output",  "-o", "out.pgm", "output image")
    call set_option (program_options, "--param",   "-p", "",        "parameter file")
    call set_option (program_options, "--mask",    "-m", "",        "mask image")
    call set_option (program_options, "--version", "-v", .false. ,  "print version info to screen")
    call set_option (program_options, "--help",    "-h", .false. ,  "print help message and quit")
    call set_option (program_options, "--license", "-l", .false. ,  "print license and quit")
    call set_option (program_options, "--gui",     "-g", .false. ,  "enable opengl gui")

    call check_options (program_options, ierr)

    write (OUTPUT_UNIT, *) ""

    argc = command_argument_count ()
    if ((argc == 0) .or. (ierr /= 0))  then
        write (OUTPUT_UNIT, *) "No command line options provided. Exiting"
        write (OUTPUT_UNIT, *) ""
        call print_help_message (program_options, &
                "Inpainting with Homogeneous Diffusion" , "0.1" , "Laurent Hoeltgen <contact@laurenthoeltgen.name>" , "2019")
        deallocate(program_options)
        stop
    end if

    call parse_options (program_options)

    call get_option_value (program_options, "-h", help)
    call get_option_value (program_options, "-v", version)
    call get_option_value (program_options, "-l", license)
    call get_option_value (program_options, "-g", gui)

    if ( help .or. version) then
        call print_help_message (program_options, &
                "Inpainting with Homogeneous Diffusion" , "0.1" , "Laurent Hoeltgen <contact@laurenthoeltgen.name>" , "2019")
        write(OUTPUT_UNIT, *) "This program was compiled by ", compiler_version(), " using the options:"
        write(OUTPUT_UNIT, *) compiler_options()
        deallocate(program_options)
        stop
    end if

    if (license) then
        call print_license()
        deallocate(program_options)
        stop
    end if

    call get_option_value (program_options, "-i", inputfile)
    call get_option_value (program_options, "-o", outputfile)
    call get_option_value (program_options, "-m", maskfile)

    ! The parameter file overwrites the commandline parameters.
    call get_option_value (program_options, "-p", parameterfile)

    if (parameterfile /= "") then

        write (fmt,*) '(1X,A29,1X,A', len_trim(parameterfile), ')'
        write (OUTPUT_UNIT, fmt) "Reading parameters from file:", adjustl(trim(parameterfile))
        open (UNIT=101, FILE=parameterfile, STATUS='OLD', ACTION='READ', IOSTAT=ierr, IOMSG=buf)
        if (ierr /= 0) then
            write (fmt,*) '(1X,A24,6X,A', len_trim(parameterfile), ')'
            write (OUTPUT_UNIT, fmt) "Error reading from file:", adjustl(trim(parameterfile))
            write (OUTPUT_UNIT, *) "Return code was: ", ierr
            write (OUTPUT_UNIT, *) "Error Message is: "
            write (OUTPUT_UNIT, *) adjustl(trim(buf))
        else
            read (UNIT=101, NML=parameters)
            close (UNIT=101)
        end if
    end if

    call get_command_argument(0, buf, arglen, ierr)

    write (fmt,*) '(1X,A19,11X,A', len_trim(buf), ')'
    write (OUTPUT_UNIT, fmt) "Name of executable:", adjustl(trim(buf))
    write (fmt,*) '(1X,A13,17X,A', len_trim(inputfile), ')'
    write (OUTPUT_UNIT, fmt) "source image:", adjustl(trim(inputfile))
    write (fmt,*) '(1X,A13,17X,A', len_trim(outputfile), ')'
    write (OUTPUT_UNIT, fmt) "target image:", adjustl(trim(outputfile))

    if (maskfile /= "") then
        write (fmt,*) '(1X,A11,19X,A', len_trim(maskfile), ')'
        write (OUTPUT_UNIT, fmt) "Mask image:", adjustl(trim(maskfile))
    end if

    call get_image_dimensions(trim(inputfile), nr, nc, nd)

    write (OUTPUT_UNIT, '(/,1X,A17,13X,I4.4,1X,A1,1X,I4.4)') "Image dimensions:", nr, "x", nc
    write (OUTPUT_UNIT, '(1X,A19,11X,I1.1)') "Number of channels:", nd

    allocate(inputimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    call get_image (trim(inputfile), nr, nc, nd, inputimg)

    write (buf,'(F4.2)')  minval(inputimg)
    write (buf2,'(F4.2)') maxval(inputimg)
    write (OUTPUT_UNIT, '(1X,A11,19X,A4,1X,A1,1X,A4)') "Data range:", adjustl(trim(buf)), "-", adjustl(trim(buf2))

    call get_image_dimensions(trim(maskfile), nr, nc, nd)

    write (OUTPUT_UNIT, '(1X,A16,14X,I4.4,1X,A1,1X,I4.4)') "Mask dimensions:", nr, "x", nc
    write (OUTPUT_UNIT, '(1X,A19,11X,I1.1)') "Number of channels:", nd

    allocate(maskimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    call get_image (trim(maskfile), nr, nc, nd, maskimg)

    write (buf,'(F4.2)')  minval(maskimg)
    write (buf2,'(F4.2)') maxval(maskimg)
    write (OUTPUT_UNIT, '(1X,A11,19X,A4,1X,A1,1X,A4)') "Mask range:", adjustl(trim(buf)), "-", adjustl(trim(buf2))

    allocate(recoimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    write (OUTPUT_UNIT, '(/,1X,A29,1X)', advance="no") "Solving the inpainting PDE..."
    call solve_inpainting ([int(nr,INT64), int(nc,INT64)], maskimg, inputimg, .false., recoimg, 2_INT64)
    write (OUTPUT_UNIT, '(A8,/)') "Success!"

    allocate(diffimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    if (gui) then

        gui_window_width  = int(4*nc, c_int)
        gui_window_height = int(nr, c_int)
        gui_num_images = 4

        allocate(glinputimg(nr*nc*nd), stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if
        allocate(glmaskimg(nr*nc*nd), stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if
        allocate(glrecoimg(nr*nc*nd), stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if
        allocate(gldiffimg(nr*nc*nd), stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if

        diffimg = inputimg - recoimg
        call normalise_range(nr*nc*nd, diffimg)
        kk = 1
        do jj = nr, 1, -1
            do ii = 0, (nc-1)
                glinputimg(kk) = real(inputimg(ii*nr + jj), GLfloat)
                glmaskimg(kk)  = real(maskimg(ii*nr + jj),  GLfloat)
                glrecoimg(kk)  = real(recoimg(ii*nr + jj),  GLfloat)
                gldiffimg(kk)  = real(diffimg(ii*nr + jj),  GLfloat)
                kk = kk + 1
            end do
        end do

        call glutinit()
        call gui_init()

        call glutdisplayfunc(display)
        call glutidlefunc(idle)

        call glutmainloop()

        deallocate(glinputimg, glmaskimg, glrecoimg, gldiffimg, stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if

    end if

    mse = mse_error(nr*nc*nd, 255*inputimg, 255*recoimg)
    psnr = psnr_error(nr*nc*nd, inputimg, recoimg)
    write (buf,'(F9.4)')  mse
    write (buf2,'(F9.4)') psnr
    write (OUTPUT_UNIT, '(1X,A13,17X,A9,1X,A1,1X,A9)') "MSE and PSNR:", adjustl(trim(buf)), "/", adjustl(trim(buf2))

    diffimg = eval_inpainting_pde ([nr, nc], recoimg, maskimg, inputimg)
    residual = sum(diffimg**2)
    write (buf,'(F9.4)')  residual
    write (OUTPUT_UNIT, '(1X,A9,21X,A5,1X,A1,1X,A9)') "Residual:", adjustl(trim(buf))

    if (parameterfile /= "") then
        write (fmt,*) '(1X,A20,10X,A', len_trim(outputfile), ')'
        write (OUTPUT_UNIT, fmt) "Writing solution to:", adjustl(trim(outputfile))

        call write_image(adjustl(trim(outputfile)), nr, nc, nd, recoimg)
    end if

    deallocate(inputimg, maskimg, recoimg, diffimg, stat=ierr)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    deallocate(program_options, stat=ierr)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    write (OUTPUT_UNIT, '(/,1X,A5,/)') "Done."

end program inpainting
