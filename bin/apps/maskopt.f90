! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.

module maskopt_gui
    !! OpenGL GUI for the inpainting app
    use :: iso_c_binding

    use :: opengl_gl
    use :: opengl_glu
    use :: opengl_glut

    implicit none
    save
    public

    integer(kind=c_int) :: gui_window_width
    integer(kind=c_int) :: gui_window_height
    integer(kind=c_int) :: gui_num_images
    integer(kind=GLint) :: gui_win
    character(len=*), parameter :: gui_window_name = "Inpainting"//char(0)

    real(kind=GLfloat), dimension(:), allocatable, target :: glinputimg ! image data for opengl viewer
    real(kind=GLfloat), dimension(:), allocatable, target :: glmaskimg  ! image data for opengl viewer
    real(kind=GLfloat), dimension(:), allocatable, target :: glrecoimg  ! image data for opengl viewer
    real(kind=GLfloat), dimension(:), allocatable, target :: gldiffimg  ! image data for opengl viewer

contains

    subroutine gui_init()
        call glutinitdisplaymode(GLUT_DOUBLE + GLUT_RGB)
        call glutinitwindowsize(gui_window_width, gui_window_height);
        gui_win = glutcreatewindow(gui_window_name)

        call glviewport(0_GLint, 0_GLint, int(gui_window_width, GLsizei), int(gui_window_width, GLsizei))
        call glmatrixmode(GL_PROJECTION)
        call glloadidentity()
        call glortho(0.0_GLdouble, real(gui_window_width, GLdouble), &
                0.0_GLdouble, real(gui_window_height, GLdouble), -1.0_GLdouble, 1.0_GLdouble)
        call glmatrixmode(GL_MODELVIEW)
        call gldisable(GL_DITHER)
        call glpixelzoom(1.0_GLfloat, 1.0_GLfloat)

        call glclear(GL_COLOR_BUFFER_BIT + GL_DEPTH_BUFFER_BIT)
        call glclearcolor(0.0_GLfloat, 0.0_GLfloat, 0.0_GLfloat, 1.0_GLfloat)

        ! call glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION)
    end subroutine gui_init

    subroutine idle() bind(c)
        call glutPostRedisplay()
    end subroutine idle

    subroutine display() bind(c)
        ! Input image
        call glrasterpos3f(0.0_GLfloat, 0.0_GLfloat, 0.0_GLfloat)
        call gldrawpixels(int(gui_window_width/gui_num_images, GLsizei), int(gui_window_height, GLsizei), &
                GL_LUMINANCE, GL_FLOAT, C_loc(glinputimg))

        ! Mask
        call glrasterpos3f(real(gui_window_height, GLfloat), 0.0_GLfloat, 0.0_GLfloat)
        call gldrawpixels(int(gui_window_width/gui_num_images, GLsizei), int(gui_window_height, GLsizei), &
                GL_LUMINANCE, GL_FLOAT, C_loc(glmaskimg))

        ! Reconsutruction
        call glrasterpos3f(real(2*gui_window_height, GLfloat), 0.0_GLfloat, 0.0_GLfloat)
        call gldrawpixels(int(gui_window_width/gui_num_images, GLsizei), int(gui_window_height, GLsizei), &
                GL_LUMINANCE, GL_FLOAT, C_loc(glrecoimg))

        ! Difference
        call glrasterpos3f(real(3*gui_window_height, GLfloat), 0.0_GLfloat, 0.0_GLfloat)
        call gldrawpixels(int(gui_window_width/gui_num_images, GLsizei), int(gui_window_height, GLsizei), &
                GL_LUMINANCE, GL_FLOAT, C_loc(gldiffimg))

        call glutswapbuffers()

        call glutPostRedisplay()
    end subroutine display

    subroutine print_image_stats (arr)
        use :: iso_fortran_env, only: OUTPUT_UNIT, REAL64
        use :: miscfun,         only: array_stats
        implicit none

        real(REAL64), dimension(:), intent(in) :: arr
        real(REAL64), dimension(4)             :: img_stats

        img_stats = array_stats(arr)
        write (OUTPUT_UNIT, '(4X,A14,16X,F5.2)') "Minimum Value:", img_stats(1)
        write (OUTPUT_UNIT, '(4X,A14,16X,F5.2)') "Maximum Value:", img_stats(2)
        write (OUTPUT_UNIT, '(4X,A14,16X,F5.2)') "Average Value:", img_stats(3)
        write (OUTPUT_UNIT, '(4X,A9,21X,F5.2)')  "Variance:",    img_stats(4)
        write (OUTPUT_UNIT, *) ""
    end subroutine print_image_stats

    subroutine print_image_errors(f,g)
        use :: iso_fortran_env, only: OUTPUT_UNIT, REAL64
        use :: image,           only: psnr_error, mse_error
        implicit none

        real(REAL64), dimension(:),       intent(in) :: f
        real(REAL64), dimension(size(f)), intent(in) :: g

        real(REAL64) :: mse
        real(REAL64) :: psnr

        mse  = mse_error  (size(f), 255*f, 255*g)
        psnr = psnr_error (size(f),     f,     g)

        write (OUTPUT_UNIT, '(1X,A4,26X,F5.2)') "MSE:",  mse
        write (OUTPUT_UNIT, '(1X,A5,25X,F5.2)') "PSNR:", psnr
        write (OUTPUT_UNIT, *) ""
    end subroutine print_image_errors

end module maskopt_gui

program maskopt
    use :: iso_fortran_env
    use :: iso_c_binding

    use :: opengl_gl
    use :: opengl_glu
    use :: opengl_glut

    use :: maskopt_gui

    use :: m_option_parser
    use :: bin_utils
    use :: io

    use :: inpainting
    use :: image
    use :: maskopt,    only: opt_mask_nGrad, opt_mask_nGrad_pc
    use :: grayvalopt, only: apply_gvo

    implicit none

    ! option parser variables

    integer :: argc = 0 ! Number of command line parameters.
    integer :: arglen   ! length of command line parameters.
    logical :: gui      ! Whether to launch the gui
    logical :: help     ! Whether to print the help message
    logical :: version  ! Whether to print the version message
    logical :: license  ! Whether to print the license message

    type(option_t), dimension(:), allocatable :: program_options ! storage for command line parameters

    ! i/o variables

    character(len=256) :: buf  ! Temporary buffer.
    character(len=64)  :: buf2 ! Temporary buffer.
    character(len=64)  :: fmt ! Format string for I/O.

    character(len=256) :: inputfile     ! Name of input image
    character(len=256) :: outputfile    ! Name of output image
    character(len=256) :: maskfile      ! Name of mask image
    character(len=256) :: parameterfile ! Name of parameterfile

    integer(kind=c_long) :: nr = -1 ! number of rows of input image.
    integer(kind=c_long) :: nc = -1 ! number of columns of input image.
    integer(kind=c_long) :: nd = -1 ! number of channels of input image.

    character(8)  :: date
    character(10) :: time

    ! generic variables

    integer              :: ierr ! return value to check status.
    character(len=256)   :: error_message
    integer(kind=c_long) :: ii   ! loop variable
    integer(kind=c_long) :: jj   ! loop variable
    integer(kind=c_long) :: kk   ! loop variable

    real(kind=REAL64),  dimension(:), allocatable :: inputimg    ! input image i.e. f
    real(kind=REAL64),  dimension(:), allocatable :: maskimg     ! mask image  i.e. c
    real(kind=REAL64),  dimension(:), allocatable :: bin_maskimg ! binarised mask image  i.e. c
    real(kind=REAL64),  dimension(:), allocatable :: recoimg     ! reconstruction image  i.e. u
    real(kind=REAL64),  dimension(:), allocatable :: bin_recoimg ! reconstruction image from binarised mask
    real(kind=REAL64),  dimension(:), allocatable :: solvimg     ! inpainted from mask
    real(kind=REAL64),  dimension(:), allocatable :: diffimg     ! mask image  i.e. f-u

    real(kind=REAL64)   :: lambda, epsi, mu, nu, tol_lin, tol_opt
    integer(kind=INT32) :: num_lin, maxit
    real(kind=REAL64)   :: mse, psnr, residual ! Error measures

    namelist /parameters/ inputfile, outputfile, maskfile, lambda, epsi, mu, nu, tol_lin, tol_opt, num_lin, maxit
    !! namelist to simplify reading parameters


    allocate(program_options(16), stat = ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    call set_option (program_options, "--input",   "-i", "",        "input image")
    call set_option (program_options, "--output",  "-o", "",        "output reconstruction image")
    call set_option (program_options, "--param",   "-p", "",        "parameter file")
    call set_option (program_options, "--mask",    "-m", "",        "output mask image")
    call set_option (program_options, "--version", "-v", .false. ,  "print version info to screen")
    call set_option (program_options, "--help",    "-h", .false. ,  "print help message and quit")
    call set_option (program_options, "--license", "-l", .false. ,  "print license and quit")
    call set_option (program_options, "--gui",     "-g", .false. ,  "enable opengl gui")

    call set_option (program_options, "--epsilon", "", 1.0D-9,     "epsilon regularisation weight")
    call set_option (program_options, "--lambda",  "", 1.0_REAL64, "sparsity weight")
    call set_option (program_options, "--mu",      "", 1.0_REAL64, "proximal weight")
    call set_option (program_options, "--nu",      "", 1.0_REAL64, "mask smoothnes regularisation weight")
    call set_option (program_options, "--num_lin", "", 1_INT32,    "number of linearisations")
    call set_option (program_options, "--maxit",   "", 1_INT32,    "number of convex optimisation iterations")
    call set_option (program_options, "--tol_lin", "", 1.0D-10,    "tolerance threshold for linearisation")
    call set_option (program_options, "--tol_opt", "", 1.0D-10,    "tolerance threshold for optimisation")

    call check_options (program_options, ierr)

    write (OUTPUT_UNIT, *) ""

    argc = command_argument_count ()
    if ((argc == 0) .or. (ierr /= 0))  then
        write (OUTPUT_UNIT, *) "No command line options provided. Exiting"
        write (OUTPUT_UNIT, *) ""
        call print_help_message (program_options, &
                "Mask optimisation through optimal control" , "0.1" , "Laurent Hoeltgen <contact@laurenthoeltgen.name>" , "2019")
        deallocate(program_options)
        stop
    end if

    call parse_options (program_options)

    call get_option_value (program_options, "-h", help)
    call get_option_value (program_options, "-v", version)
    call get_option_value (program_options, "-l", license)
    call get_option_value (program_options, "-g", gui)

    if (help .or. version) then
        call print_help_message (program_options, &
                "Mask optimisation through optimal control" , "0.1" , "Laurent Hoeltgen <contact@laurenthoeltgen.name>" , "2019")
        deallocate(program_options)
        stop
    end if

    if (license) then
        call print_license()
        deallocate(program_options)
        stop
    end if

    call get_option_value (program_options, "-i", inputfile)
    call get_option_value (program_options, "-o", outputfile)
    call get_option_value (program_options, "-m", maskfile)

    call get_option_value (program_options, "--epsilon", epsi)
    call get_option_value (program_options, "--lambda",  lambda)
    call get_option_value (program_options, "--mu",      mu)
    call get_option_value (program_options, "--nu",      nu)
    call get_option_value (program_options, "--num_lin", num_lin)
    call get_option_value (program_options, "--maxit",   maxit)
    call get_option_value (program_options, "--tol_lin", tol_lin)
    call get_option_value (program_options, "--tol_opt", tol_opt)

    !! The parameter file overwrites the commandline parameters.
    call get_option_value (program_options, "-p", parameterfile)

    if (parameterfile /= "") then

        write (fmt,*) '(1X,A29,1X,A', len_trim(parameterfile), ')'
        write (OUTPUT_UNIT, fmt) "Reading parameters from file:", adjustl(trim(parameterfile))
        open (UNIT=101, FILE=parameterfile, STATUS='OLD', ACTION='READ', IOSTAT=ierr, IOMSG=buf)
        if (ierr /= 0) then
            write (fmt,*) '(1X,A24,6X,A', len_trim(parameterfile), ')'
            write (OUTPUT_UNIT, fmt) "Error reading from file:", adjustl(trim(parameterfile))
            write (OUTPUT_UNIT, *)   "Return code was: ", ierr
            write (OUTPUT_UNIT, *)   "Error Message is: "
            write (OUTPUT_UNIT, *) adjustl(trim(buf))
        else
            read (UNIT=101, NML=parameters)
            close (UNIT=101)
        end if
    end if

    call get_command_argument(0, buf, arglen, ierr)

    write (fmt,*) '(1X,A19,11X,A', len_trim(buf), ')'
    write (OUTPUT_UNIT, fmt) "Name of executable:", adjustl(trim(buf))
    write (fmt,*) '(1X,A13,17X,A', len_trim(inputfile), ')'
    write (OUTPUT_UNIT, fmt) "source image:", adjustl(trim(inputfile))

    if (outputfile /= "") then
        write (fmt,*) '(1X,A21,9X,A', len_trim(outputfile), ')'
        write (OUTPUT_UNIT, fmt) "reconstruction image:", adjustl(trim(outputfile))
    end if

    if (maskfile /= "") then
        write (fmt,*) '(1X,A11,19X,A', len_trim(maskfile), ')'
        write (OUTPUT_UNIT, fmt) "mask image:", adjustl(trim(maskfile))
    end if

    call get_image_dimensions(trim(inputfile), nr, nc, nd)

    write (OUTPUT_UNIT, '(/,1X,A17,13X,I4.4,1X,A1,1X,I4.4)') "image dimensions:", nr, "x", nc
    write (OUTPUT_UNIT, '(1X,A19,11X,I1.1)') "number of channels:", nd

    allocate(inputimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    allocate(solvimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    call get_image (trim(inputfile), nr, nc, nd, inputimg)

    write (buf,'(F4.2)')  minval(inputimg)
    write (buf2,'(F4.2)') maxval(inputimg)
    write (OUTPUT_UNIT, '(1X,A11,19X,A4,1X,A1,1X,A4)') "Data range:", adjustl(trim(buf)), "-", adjustl(trim(buf2))

    allocate(maskimg(nr*nc*nd), bin_maskimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    allocate(recoimg(nr*nc*nd), bin_recoimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    write (OUTPUT_UNIT, '(1X,A7,22X,F9.6)')  "Lambda:",                  lambda
    write (OUTPUT_UNIT, '(1X,A8,21X,F9.6)')  "Epsilon:",                 epsi
    write (OUTPUT_UNIT, '(1X,A3,26X,F9.6)')  "Mu:",                      mu
    write (OUTPUT_UNIT, '(1X,A3,26X,F9.6)')  "Nu:",                      nu
    write (OUTPUT_UNIT, '(1X,A15,15X,I7.7)') "Linearisations:",          num_lin
    write (OUTPUT_UNIT, '(1X,A14,16X,I7.7)') "PD Iterations:",           maxit
    write (OUTPUT_UNIT, '(1X,A24,5X,F9.6)')  "Tolerance linearisation:", tol_lin
    write (OUTPUT_UNIT, '(1X,A23,6X,F9.6)')  "Tolerance optimisation:",  tol_opt

    write (OUTPUT_UNIT, '(/,1X,A37,1X)', advance="no") "Computing optimal inpainting masks..."

    ! Compute optimal mask and corresponding solution of the linearised PDE
    ! maskimg contains optimal mask
    ! recoimg contains corresponding solution of the linearised PDE
    call opt_mask_nGrad (int([nr, nc], INT32), inputimg, maskimg, recoimg, lambda, epsi, mu, &
            0.0_REAL64, num_lin, maxit, (epsi + mu)/2.0_REAL64, tol_lin, tol_opt)
    ! call opt_mask_nGrad_pc (int([nr, nc], INT32), inputimg, maskimg, recoimg, lambda, epsi, mu, &
    !         0.0_REAL64, num_lin, maxit, 0.0_REAL64, tol_lin, tol_opt)

    write (OUTPUT_UNIT, *) "Mask Statistics:"
    call print_image_stats(maskimg)

    write (OUTPUT_UNIT, *) "Reconstruction Statistics:"
    call print_image_stats(recoimg)

    ! Solve inpainting problem with optimal (non-binary) mask
    ! solvimg contains the corresponding solution
    call solve_inpainting ([int(nr,INT64), int(nc,INT64)], maskimg, inputimg, .false., solvimg, 2_INT64)

    write (OUTPUT_UNIT, *) "Solution Statistics:"
    call print_image_stats(recoimg)

    ! binarise mask. We threshold at 0.1 Everything above is set to 1.0 and the rest to 0
    bin_maskimg = maskimg
    where(abs(maskimg) >  0.1D0) bin_maskimg = 1.0D0
    where(abs(maskimg) <= 0.1D0) bin_maskimg = 0.0D0

    write (OUTPUT_UNIT, *) "Binary Mask Statistics:"
    call print_image_stats(bin_maskimg)

    ! Solve inpainting problem with binarised mask
    ! bin_recoimg contains the corresponding solution
    call solve_inpainting ([int(nr,INT64), int(nc,INT64)], bin_maskimg, inputimg, .false., bin_recoimg, 2_INT64)

    write (OUTPUT_UNIT, *) "Binary Solution Statistics:"
    call print_image_stats(bin_recoimg)

    write (OUTPUT_UNIT, '(A8,/)') "Success!"

    allocate(diffimg(nr*nc*nd), stat=ierr, errmsg=error_message)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    if (gui) then

        gui_window_width  = int(4*nc, c_int)
        gui_window_height = int(nr,   c_int)
        gui_num_images = 4

        allocate(glinputimg(nr*nc*nd), stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if
        allocate(glmaskimg(nr*nc*nd), stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if
        allocate(glrecoimg(nr*nc*nd), stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if
        allocate(gldiffimg(nr*nc*nd), stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if

        ! diffimg contains the difference between the correct inpainted solution and the linearised solution.
        diffimg = solvimg - recoimg
        call normalise_range(nr*nc*nd, diffimg)
        kk = 1
        do jj = nr, 1, -1
            do ii = 0, (nc-1)
                glinputimg(kk) = real(inputimg(ii*nr + jj), GLfloat)
                glmaskimg(kk)  = real(maskimg(ii*nr + jj),  GLfloat)
                glrecoimg(kk)  = real(recoimg(ii*nc + jj),  GLfloat)
                gldiffimg(kk)  = real(diffimg(ii*nc + jj),  GLfloat)
                kk = kk + 1
            end do
        end do

        call glutinit()
        call gui_init()

        call glutdisplayfunc(display)
        call glutidlefunc(idle)

        call glutmainloop()

        deallocate(glinputimg, glmaskimg, glrecoimg, gldiffimg, stat=ierr, errmsg=error_message)
        if (ierr /= 0) then
            call print_allocation_fail(ierr, error_message)
            stop
        end if

    end if

    ! Output the error measures of the individual solutions:

    ! Residual of the PDE

    diffimg = eval_inpainting_pde ([nr, nc], recoimg, maskimg, inputimg)
    residual = sum(diffimg**2)

    write (buf,'(F5.2)')  residual
    write (OUTPUT_UNIT, '(1X,A9,21X,A5,1X,A1,1X,A5)') "Residual:", adjustl(trim(buf))
    write (OUTPUT_UNIT, *) ""

    ! Solution of the linearised PDE

    write (OUTPUT_UNIT, '(1X,A26)') "MSE and PSNR (linearised):"
    call print_image_errors(inputimg, recoimg)

    ! Inpainted solution from optimal non-binary mask

    write (OUTPUT_UNIT, '(1X,A25)') "MSE and PSNR (inpainted):"
    call print_image_errors (inputimg, solvimg)

    ! Inpainted solution from binarised mask

    write (OUTPUT_UNIT, '(1X,A25)') "MSE and PSNR (binarised):"
    call print_image_errors (inputimg, bin_recoimg)

    ! Perform grey value optimisation and output error

    write (OUTPUT_UNIT, *) "Performing tonal optimisation."
    write (OUTPUT_UNIT, *) ""

    ! Apply GVO with optimal non-binary mask
    ! Optimal data is stored in solvimg
    call apply_gvo([int(nr,INT64), int(nc,INT64)], 10000_INT64, maskimg, inputimg, solvimg, 1_INT64)

    ! Solve inpainting problem with optimal (non-binary) mask and optimised data
    ! solvimg contains the corresponding solution after the swap.
    call solve_inpainting ([int(nr,INT64), int(nc,INT64)], maskimg, solvimg, .false., recoimg, 2_INT64)
    solvimg = recoimg

    write (OUTPUT_UNIT, *) "GVO solution from optimal mask statistics:"
    call print_image_stats(solvimg)

    write (OUTPUT_UNIT, '(1X,A23)') "MSE and PSNR (inp+gvo):"
    call print_image_errors (inputimg, solvimg)

    ! Apply GVO with binarised mask
    ! Optimal data is stored in solvimg
    call apply_gvo([int(nr,INT64), int(nc,INT64)], 10000_INT64, bin_maskimg, inputimg, bin_recoimg, 1_INT64)

    ! Solve inpainting problem with binarised mask and optimised data
    ! bin_recoimg contains the corresponding solution after the swap.
    call solve_inpainting ([int(nr,INT64), int(nc,INT64)], bin_maskimg, bin_recoimg, .false., recoimg, 2_INT64)
    bin_recoimg = recoimg

    write (OUTPUT_UNIT, *) "GVO solution from binary mask statistics:"
    call print_image_stats(bin_recoimg)

    write (OUTPUT_UNIT, '(1X,A23)') "MSE and PSNR (bin+gvo):"
    call print_image_errors (inputimg, bin_recoimg)

    ! Output Data

    ! Solution
    write (fmt,*) '(1X,A26,4X,A', len_trim(outputfile), ')'
    write (OUTPUT_UNIT, fmt) "Writing reconstruction to:", adjustl(trim(outputfile))
    call write_image(adjustl(trim(outputfile)), nr, nc, nd, solvimg)

    ! Solution from binarised mask with GVO
    arglen = len_trim(outputfile)
    write (buf,*) outputfile(1:(arglen-4)), "-bin", outputfile((arglen-3):arglen)
    write (fmt,*) '(1X,A24,6X,A', len_trim(buf), ')'
    write (OUTPUT_UNIT,fmt) "Writing GVO reconst. to:", adjustl(trim(buf))
    call write_image(adjustl(trim(buf)), nr, nc, nd, bin_recoimg)

    ! Non-binary optimal mask
    write (fmt,*) '(1X,A16,14X,A', len_trim(maskfile), ')'
    write (OUTPUT_UNIT, fmt) "Writing mask to:", adjustl(trim(maskfile))
    call write_image(adjustl(trim(maskfile)), nr, nc, nd, maskimg)

    ! Binarised mask
    arglen = len_trim(maskfile)
    write (buf,*) maskfile(1:(arglen-4)), "-bin", maskfile((arglen-3):arglen)
    write (fmt,*) '(1X,A23,7X,A', len_trim(buf), ')'
    write (OUTPUT_UNIT,fmt) "Writing binary mask to:", adjustl(trim(buf))
    call write_image(adjustl(trim(buf)), nr, nc, nd, bin_maskimg)

    ! Parameters
    call date_and_time (DATE=date,TIME=time)
    write (buf, '(A8,"-",A10,".nml")') date, time
    open  (UNIT=999, FILE=adjustl(trim(buf)), STATUS='NEW')
    write (UNIT=999, NML=parameters)
    close (UNIT=999)

    deallocate(inputimg, maskimg, bin_maskimg, recoimg, bin_recoimg, diffimg, solvimg, stat=ierr)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    deallocate(program_options, stat=ierr)
    if (ierr /= 0) then
        call print_allocation_fail(ierr, error_message)
        stop
    end if

    write (OUTPUT_UNIT, '(/,1X,A5,/)') "Done."

end program maskopt
