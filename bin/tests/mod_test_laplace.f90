! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

module test_laplace
    use :: fruit
    use :: laplace
    use :: stencil
    use :: iso_fortran_env
    implicit none

contains

    ! setup_before_all
    ! setup = setup_before_each
    subroutine setup_test_laplace
    end subroutine setup_test_laplace

    ! teardown_before_all
    ! teardown = teardown_before_each
    subroutine teardown_test_laplace
    end subroutine teardown_test_laplace

    subroutine check_stencil_laplace_5p
        implicit none

        call assertEquals(real([1, -2, 1], REAL64), stencil_laplace_5p_REAL64 (1), 3)
        call assertEquals(real([0, 1, 0, 1, -4, 1, 0, 1, 0], REAL64), stencil_laplace_5p_REAL64 (2), 9)
        call assertEquals( &
                real([0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, -6, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], REAL64), &
                stencil_laplace_5p_REAL64 (3), 27)
    end subroutine check_stencil_laplace_5p

    subroutine check_laplace_5p_sparse_coo
        implicit none

        integer(INT32), dimension(13) :: ir, jc
        real(REAL64),   dimension(13) :: a

        integer(INT32), dimension(20) :: ir2, jc2
        real(REAL64),   dimension(20) :: a2

        call laplace_5p_sparse_coo([5], ir, jc, a, .true.)
        call assertEquals([1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5], ir, 13)
        call assertEquals([1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 5, 4, 5], jc, 13)
        call assertEquals(real([-1, 1, 1, -2, 1, 1, -2, 1, 1, -2, 1, 1, -1], REAL64), a, 13)

        call laplace_5p_sparse_coo([2, 3], ir2, jc2, a2, .true.)
        call assertEquals( [1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6], ir2, 20)
        call assertEquals( [1, 2, 3, 1, 2, 4, 1, 3, 4, 5, 2, 3, 4, 6, 3, 5, 6, 4, 5, 6], jc2, 20)
        call assertEquals( real([-2, 1, 1, 1, -2, 1, 1, -3, 1, 1, 1, 1, -3, 1, 1, -2, 1, 1, 1, -2], REAL64), a2, 20)

        call laplace_5p_sparse_coo([3, 2], ir2, jc2, a2, .true.)
        call assertEquals( [1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6], ir2, 20)
        call assertEquals( [1, 2, 4, 1, 2, 3, 5, 2, 3, 6, 1, 4, 5, 2, 4, 5, 6, 3, 5, 6], jc2, 20)
        call assertEquals( real([-2, 1, 1, 1, -3, 1, 1, 1, -2, 1, 1, -2, 1, 1, 1, -3, 1, 1, 1, -2], REAL64), a2, 20)
    end subroutine check_laplace_5p_sparse_coo

    subroutine check_apply_laplace_5p
        implicit none

        call assertEquals (real([0, 0, 0, 0, -6], REAL64), apply_laplace_5p([5], real([1, 2, 3, 4, 5], REAL64)), 5)
        call assertEquals (real([-1, 1, 0, 0, 0, -1, -5], REAL64), &
                apply_laplace_5p([7], real([1, 1, 2, 3, 4, 5, 5], REAL64)), 7)

        call assertEquals (real([0, 0, 0, 0, -6], REAL64), apply_laplace_5p([5], real([1, 2, 3, 4, 5], REAL64), .false.), 5)
        call assertEquals (real([-1, 1, 0, 0, 0, -1, -5], REAL64), &
                apply_laplace_5p([7], real([1, 1, 2, 3, 4, 5, 5], REAL64), .false.), 7)

        call assertEquals (real([1, 0, 0, 0, -1], REAL64), apply_laplace_5p([5], real([1, 2, 3, 4, 5], REAL64), .true.), 5)
        call assertEquals (real([0, 1, 0, 0, 0, -1, 0], REAL64), &
                apply_laplace_5p([7], real([1, 1, 2, 3, 4, 5, 5], REAL64), .true.), 7)

        call assertEquals (real([0, 1, -2, 1, 0], REAL64), apply_laplace_5p([5], real([0, 0, 1, 0, 0], REAL64)), 5)
        call assertEquals (real([-2, 1, 0, 0, 0], REAL64), apply_laplace_5p([5], real([1, 0, 0, 0, 0], REAL64)), 5)
        call assertEquals (real([0, 0, 0, 1, -2], REAL64), apply_laplace_5p([5], real([0, 0, 0, 0, 1], REAL64)), 5)

        call assertEquals (real([0, 1, 0, 1, -4, 1, 0, 1, 0], REAL64), &
                apply_laplace_5p([3, 3], real([0, 0, 0, 0, 1, 0, 0, 0, 0], REAL64)), 9)
        call assertEquals (real([-4,1,0,1,0,0,0,0,0], REAL64), &
                apply_laplace_5p([3, 3], real([1,0,0,0,0,0,0,0,0], REAL64)), 9)
        call assertEquals (real([0,1,-4,0,0,1,0,0,0], REAL64), &
                apply_laplace_5p([3, 3], real([0,0,1,0,0,0,0,0,0], REAL64)), 9)
        call assertEquals (real([0,0,0,1,0,0,-4,1,0], REAL64), &
                apply_laplace_5p([3, 3], real([0,0,0,0,0,0,1,0,0], REAL64)), 9)
        call assertEquals (real([0,0,0,0,0,1,0,1,-4], REAL64), &
                apply_laplace_5p([3, 3], real([0,0,0,0,0,0,0,0,1], REAL64)), 9)

        call assertEquals (real([-2,1,0,1,0,0,0,0,0], REAL64), &
                apply_laplace_5p([3, 3], real([1,0,0,0,0,0,0,0,0], REAL64), .true.), 9)
        call assertEquals (real([1,-3,1,0,1,0,0,0,0], REAL64), &
                apply_laplace_5p([3, 3], real([0,1,0,0,0,0,0,0,0], REAL64), .true.), 9)
        call assertEquals (real([0,1,-2,0,0,1,0,0,0], REAL64), &
                apply_laplace_5p([3, 3], real([0,0,1,0,0,0,0,0,0], REAL64), .true.), 9)
        call assertEquals (real([1,0,0,-3,1,0,1,0,0], REAL64), &
                apply_laplace_5p([3, 3], real([0,0,0,1,0,0,0,0,0], REAL64), .true.), 9)
        call assertEquals (real([0,0,0,1,0,0,-2,1,0], REAL64), &
                apply_laplace_5p([3, 3], real([0,0,0,0,0,0,1,0,0], REAL64), .true.), 9)
        call assertEquals (real([0,0,0,0,0,1,0,1,-2], REAL64), &
                apply_laplace_5p([3, 3], real([0,0,0,0,0,0,0,0,1], REAL64), .true.), 9)
    end subroutine check_apply_laplace_5p

    subroutine check_coo_amux_apply_laplace
        use :: sparse, only: amux, coocsr
        use :: stencil, only: stencil2sparse_size
        implicit none

        integer :: ii, nnz, siz_test

        real(REAL64), dimension(:), allocatable :: data, resu
        integer,      dimension(:), allocatable :: ir, jc, ir2, jc2
        real(REAL64), dimension(:), allocatable :: a, a2

        do ii = 0, 4
            siz_test = 2**(ii+10)
            nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])

            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))
            allocate(data(siz_test), resu(siz_test))

            call random_number(data)

            call laplace_5p_sparse_coo ([siz_test], ir, jc, a, .true.)
            call coocsr (siz_test, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call assertEquals (apply_laplace_5p([siz_test], data, .true.), resu, siz_test, 1.0D-9)

            deallocate(ir, jc, a, ir2, jc2, a2, data, resu)
        end do

        do ii = 0, 4
            siz_test = 2**(ii+10)
            nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])

            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))
            allocate(data(siz_test), resu(siz_test))

            call random_number(data)

            call laplace_5p_sparse_coo ([siz_test], ir, jc, a, .false.)
            call coocsr (siz_test, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call assertEquals (apply_laplace_5p([siz_test], data, .false.), resu, siz_test, 1.0D-9)

            deallocate(ir, jc, a, ir2, jc2, a2, data, resu)
        end do

        do ii = 0, 4, 2
            siz_test = 2**(ii+6)
            allocate(data(siz_test), resu(siz_test))
            siz_test = 2**((ii+6)/2)

            nnz = stencil2sparse_size ([3, 3], [siz_test, siz_test], &
                    [.false., .true., .false., .true., .true., .true., .false., .true., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**2 + 1), jc2(nnz), a2(nnz))

            call random_number(data)

            call laplace_5p_sparse_coo ([siz_test, siz_test], ir, jc, a, .true.)
            call coocsr (siz_test**2, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**2, siz_test**2, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_laplace_5p ([siz_test, siz_test], data, .true.), resu, siz_test**2, 1.0D-9)

            deallocate(data, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 4, 2
            siz_test = 2**(ii+6)
            allocate(data(siz_test), resu(siz_test))
            siz_test = 2**((ii+6)/2)

            nnz = stencil2sparse_size ([3, 3], [siz_test, siz_test], &
                    [.false., .true., .false., .true., .true., .true., .false., .true., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**2 + 1), jc2(nnz), a2(nnz))

            call random_number(data)

            call laplace_5p_sparse_coo ([siz_test, siz_test], ir, jc, a, .false.)
            call coocsr (siz_test**2, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**2, siz_test**2, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_laplace_5p ([siz_test, siz_test], data, .false.), resu, siz_test**2, 1.0D-9)

            deallocate(data, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 3, 3
            siz_test = 2**(ii+3)
            allocate(data(siz_test), resu(siz_test))
            siz_test = 2**((ii+3)/3)

            nnz = stencil2sparse_size ([3, 3, 3], [siz_test, siz_test, siz_test], &
                    [.false., .false., .false., .false., .true., .false., .false., .false., .false., &
                    .false., .true., .false., .true., .true., .true., .false., .true., .false., &
                    .false., .false., .false., .false., .true., .false., .false., .false., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**3 + 1), jc2(nnz), a2(nnz))

            call random_number(data)

            call laplace_5p_sparse_coo ([siz_test, siz_test, siz_test], ir, jc, a, .true.)
            call coocsr (siz_test**3, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**3, siz_test**3, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_laplace_5p ([siz_test, siz_test, siz_test], data, .true.), resu, siz_test**3, 1.0D-9)

            deallocate(data, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 3, 3
            siz_test = 2**(ii+3)
            allocate(data(siz_test), resu(siz_test))
            siz_test = 2**((ii+3)/3)

            nnz = stencil2sparse_size ([3, 3, 3], [siz_test, siz_test, siz_test], &
                    [.false., .false., .false., .false., .true., .false., .false., .false., .false., &
                    .false., .true., .false., .true., .true., .true., .false., .true., .false., &
                    .false., .false., .false., .false., .true., .false., .false., .false., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**3 + 1), jc2(nnz), a2(nnz))

            call random_number(data)

            call laplace_5p_sparse_coo ([siz_test, siz_test, siz_test], ir, jc, a, .false.)
            call coocsr (siz_test**3, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**3, siz_test**3, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_laplace_5p ([siz_test, siz_test, siz_test], data, .false.), resu, siz_test**3, 1.0D-9)

            deallocate(data, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

    end subroutine check_coo_amux_apply_laplace
end module test_laplace
