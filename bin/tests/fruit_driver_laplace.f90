! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver
        use :: fruit
        use :: test_laplace
        implicit none

        integer :: fail_counter

        call init_fruit

        call setup_test_laplace
        write (*,*) ".. running test: check_stencil_laplace_5p"
        call set_unit_name('check_stencil_laplace_5p')
        call run_test_case(check_stencil_laplace_5p, "check_stencil_laplace_5p")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_laplace

        call setup_test_laplace
        write (*,*) ".. running test: check_laplace_5p_sparse_coo"
        call set_unit_name('check_laplace_5p_sparse_coo')
        call run_test_case(check_laplace_5p_sparse_coo, "check_laplace_5p_sparse_coo")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_laplace

        call setup_test_laplace
        write (*,*) ".. running test: check_apply_laplace_5p"
        call set_unit_name('check_apply_laplace_5p')
        call run_test_case(check_apply_laplace_5p, "check_apply_laplace_5p")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_laplace

        call setup_test_laplace
        write (*,*) ".. running test: check_coo_amux_apply_laplace"
        call set_unit_name('check_coo_amux_apply_laplace')
        call run_test_case(check_coo_amux_apply_laplace, "check_coo_amux_apply_laplace")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_laplace

        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver
