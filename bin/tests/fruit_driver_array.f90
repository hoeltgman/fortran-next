! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver_array
        use :: fruit
        use :: test_array
        implicit none

        integer :: fail_counter

        call init_fruit

        call setup_test_array
        write (*,*) ".. running test: check_add0padding"
        call set_unit_name('check_add0padding')
        call run_test_case(check_add0padding, "check_add0padding")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_array

        call setup_test_array
        write (*,*) ".. running test: check_addmirror"
        call set_unit_name('check_addmirror')
        call run_test_case(check_addmirror, "check_addmirror")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_array

        call setup_test_array
        write (*,*) ".. running test: check_rmpadding"
        call set_unit_name('check_rmpadding')
        call run_test_case(check_rmpadding, "check_rmpadding")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_array

        call setup_test_array
        write (*,*) ".. running test: check_ind2sub"
        call set_unit_name('check_ind2sub')
        call run_test_case(check_ind2sub, "check_ind2sub")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_array

        call setup_test_array
        write (*,*) ".. running test: check_sub2ind"
        call set_unit_name('check_sub2ind')
        call run_test_case(check_sub2ind, "check_sub2ind")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_array

        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver_array
