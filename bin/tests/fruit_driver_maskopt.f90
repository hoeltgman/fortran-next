! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver
        use :: fruit
        use :: test_maskopt
        implicit none

        integer :: fail_counter

        call init_fruit

        call setup_test_maskopt
        write (*,*) ".. running test: check_primal_energy"
        call set_unit_name('check_primal_energy')
        call run_test_case(check_primal_energy, "check_primal_energy")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_maskopt

        call setup_test_maskopt
        write (*,*) ".. running test: check_resF_c_nGrad"
        call set_unit_name('check_resF_c_nGrad')
        call run_test_case(check_resF_c_nGrad, "check_resF_c_nGrad")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_maskopt

        call setup_test_maskopt
        write (*,*) ".. running test: check_resG_nGrad"
        call set_unit_name('check_resG_nGrad')
        call run_test_case(check_resG_nGrad, "check_resG_nGrad")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_maskopt

        call setup_test_maskopt
        write (*,*) ".. running test: check_opK_nGrad"
        call set_unit_name('check_opK_nGrad')
        call run_test_case(check_opK_nGrad, "check_opK_nGrad")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_maskopt

        call setup_test_maskopt
        write (*,*) ".. running test: check_opt_mask_nGrad"
        call set_unit_name('check_opt_mask_nGrad')
        call run_test_case(check_opt_mask_nGrad, "check_opt_mask_nGrad")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_maskopt

        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver
