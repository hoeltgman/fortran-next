! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

module test_inpainting
    use :: fruit
    use :: inpainting
    use :: iso_fortran_env
    implicit none

contains

    ! setup_before_all
    ! setup = setup_before_each
    subroutine setup_test_inpainting
    end subroutine setup_test_inpainting

    ! teardown_before_all
    ! teardown = teardown_before_each
    subroutine teardown_test_inpainting
    end subroutine teardown_test_inpainting

    subroutine check_apply_inpainting_5p
        implicit none

        call assertEquals (real(-1*[0, 0, 0, 0, -6], REAL64), apply_inpainting_5p([5], real([1, 2, 3, 4, 5], REAL64), &
                real([0, 0, 0, 0, 0], REAL64)), 5)
        call assertEquals (real(-1*[-1, 1, 0, 0, 0, -1, -5], REAL64), &
                apply_inpainting_5p([7], real([1, 1, 2, 3, 4, 5, 5], REAL64), &
                real([0, 0, 0, 0, 0, 0, 0], REAL64)), 7)

        call assertEquals (real([1, 2, 0, 0, 1], REAL64), apply_inpainting_5p([5], real([1, 2, 3, 4, 5], REAL64), &
                real([1, 1, 0, 0, 0], REAL64), .true.), 5)
        call assertEquals (real([-1, 0, 0, 0, 5], REAL64), apply_inpainting_5p([5], real([1, 2, 3, 4, 5], REAL64), &
                real([0, 0, 0, 0, 1], REAL64), .true.), 5)

        call assertEquals (real(-1*[0, 1, -2, 1, 0], REAL64), apply_inpainting_5p([5], real([0, 0, 1, 0, 0], REAL64), &
                real([0, 0, 0, 0, 0], REAL64)), 5)
        call assertEquals (real(-1*[-2, 1, 0, 0, 0], REAL64), apply_inpainting_5p([5], real([1, 0, 0, 0, 0], REAL64), &
                real([0, 0, 0, 0, 0], REAL64)), 5)
        call assertEquals (real(-1*[0, 0, 0, 1, -2], REAL64), apply_inpainting_5p([5], real([0, 0, 0, 0, 1], REAL64), &
                real([0, 0, 0, 0, 0], REAL64)), 5)

        call assertEquals (real(-1*[0,1,0,1,-4,1,0,1,0], REAL64), &
                apply_inpainting_5p([3, 3], real([0,0,0,0,1,0,0,0,0], REAL64), &
                real([0, 0, 0, 0, 0, 0, 0, 0, 0], REAL64)), 9)
        call assertEquals (real(-1*[-4,1,0,1,0,0,0,0,0], REAL64), &
                apply_inpainting_5p([3, 3], real([1,0,0,0,0,0,0,0,0], REAL64), &
                real([0, 0, 0, 0, 0, 0, 0, 0, 0], REAL64)), 9)
        call assertEquals (real(-1*[0,1,-4,0,0,1,0,0,0], REAL64), &
                apply_inpainting_5p([3, 3], real([0,0,1,0,0,0,0,0,0], REAL64), &
                real([0, 0, 0, 0, 0, 0, 0, 0, 0], REAL64)), 9)
        call assertEquals (real(-1*[0,0,0,1,0,0,-4,1,0], REAL64), &
                apply_inpainting_5p([3, 3], real([0,0,0,0,0,0,1,0,0], REAL64), &
                real([0, 0, 0, 0, 0, 0, 0, 0, 0], REAL64)), 9)
        call assertEquals (real(-1*[0,0,0,0,0,1,0,1,-4], REAL64), &
                apply_inpainting_5p([3, 3], real([0,0,0,0,0,0,0,0,1], REAL64), &
                real([0, 0, 0, 0, 0, 0, 0, 0, 0], REAL64)), 9)
    end subroutine check_apply_inpainting_5p

    subroutine check_apply_inpainting_T_5p
        implicit none

        call assertEquals (real([1, -1, 2, 0, 1], REAL64), apply_inpainting_T_5p([5], real([1, 2, 3, 4, 5], REAL64), &
                real([1, 1, 0, 0, 0], REAL64), .true.), 5)
        call assertEquals (real([-1, 0, 0, 5, 1], REAL64), apply_inpainting_T_5p([5], real([1, 2, 3, 4, 5], REAL64), &
                real([0, 0, 0, 0, 1], REAL64), .true.), 5)

        call assertEquals (real([1, 2, 3, 4, 5], REAL64), apply_inpainting_T_5p([5], &
                real([1, 2, 3, 4, 5], REAL64), real([1, 1, 1, 1, 1], REAL64)), 5)

        call assertEquals (real([ &
                0.219791666666667D0, -0.1156250D0, 4.3750D-002, -0.213541666666667D0, &
                0.186458333333333D0, -9.374999999999994D-003, 0.132291666666667D0, &
                -0.108333333333333D0, -0.239583333333333D0, 0.1656250D0, &
                7.604166666666673D-002, 1.3156250D0, 0.308333333333333D0, &
                1.018750D0, 3.333333333333338D-002], REAL64), apply_inpainting_T_5p([3, 5], &
                real([9, 3, 4, 1, 6, 3, 7, 2, 0, 13, 15, 63, 25, 64, 19], REAL64)/64.0D0, &
                real([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], REAL64)/15.0D0, .true.), 15, 1.0D-8)

    end subroutine check_apply_inpainting_T_5p

    subroutine check_norm_lin_pde_op
        implicit none

        call assertEquals(1.0D0, 1.0D0, 1.0D-6)

    end subroutine check_norm_lin_pde_op

    subroutine check_inpainting_5p_sparse_coo
        implicit none

        integer(INT32), dimension(13) :: ir, jc
        real(REAL64),   dimension(13) :: a

        integer(INT32), dimension(20) :: ir2, jc2
        real(REAL64),   dimension(20) :: a2

        call inpainting_5p_sparse_coo([5], real([0, 0, 0, 0, 0], REAL64), ir, jc, a, .true.)
        call assertEquals([1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5], ir, 13)
        call assertEquals([1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 5, 4, 5], jc, 13)
        call assertEquals(real([1, -1, -1, 2, -1, -1, 2, -1, -1, 2, -1, -1, 1], REAL64), a, 13)

        call inpainting_5p_sparse_coo([5], real([1, 1, 1, 1, 1], REAL64), ir, jc, a, .true.)
        call assertEquals([1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5], ir, 13)
        call assertEquals([1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 5, 4, 5], jc, 13)
        call assertEquals(real([1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1], REAL64), a, 13)

        call inpainting_5p_sparse_coo([5], real([0, 1, 0, 1, 0], REAL64), ir, jc, a, .true.)
        call assertEquals([1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5], ir, 13)
        call assertEquals([1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 5, 4, 5], jc, 13)
        call assertEquals(real([1, -1, 0, 1, 0, -1, 2, -1, 0, 1, 0, -1, 1], REAL64), a, 13)

        call inpainting_5p_sparse_coo([2, 3], real([0, 0, 0, 0, 0, 0], REAL64), ir2, jc2, a2, .true.)
        call assertEquals( [1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6], ir2, 20)
        call assertEquals( [1, 2, 3, 1, 2, 4, 1, 3, 4, 5, 2, 3, 4, 6, 3, 5, 6, 4, 5, 6], jc2, 20)
        call assertEquals( real(-1*[-2, 1, 1, 1, -2, 1, 1, -3, 1, 1, 1, 1, -3, 1, 1, -2, 1, 1, 1, -2], REAL64), a2, 20)

        call inpainting_5p_sparse_coo([2, 3], real([0, 1, 0, 1, 1, 1], REAL64), ir2, jc2, a2, .true.)
        call assertEquals( [1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6], ir2, 20)
        call assertEquals( [1, 2, 3, 1, 2, 4, 1, 3, 4, 5, 2, 3, 4, 6, 3, 5, 6, 4, 5, 6], jc2, 20)
        call assertEquals( real([2, -1, -1, 0, 1, 0, -1, 3, -1, -1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1], REAL64), a2, 20)

        call inpainting_5p_sparse_coo([3, 2], real([0, 1, 0, 1, 1, 1], REAL64), ir2, jc2, a2, .true.)
        call assertEquals( [1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6], ir2, 20)
        call assertEquals( [1, 2, 4, 1, 2, 3, 5, 2, 3, 6, 1, 4, 5, 2, 4, 5, 6, 3, 5, 6], jc2, 20)
        call assertEquals( real([2, -1, -1, 0, 1, 0, 0, -1, 2, -1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1], REAL64), a2, 20)
    end subroutine check_inpainting_5p_sparse_coo

    subroutine check_solve_inpainting
        implicit none
        real(REAL64), dimension(5) :: u1D, c1D, f1D, v1D
        real(REAL64), dimension(5) :: c, u, f, solgvo, solinp

        c1D = real([1, 0, 1, 0, 1], REAL64)
        f1D = real([1, 2, 3, 4, 5], REAL64)
        v1D = real([1, 2, 3, 4, 5], REAL64)

        call solve_inpainting ([5], c1D, f1D, .false., u1D, 2)
        call assertEquals(real([1, 2, 3, 4, 5], REAL64), u1D, 5, 1D-8)

        v1D = real([2, 0, 6, 0, 7], REAL64)

        call solve_inpainting([5], c1D, f1D, .true., u1D, 2)
        call assertEquals(real([2, 0, 6, 0, 7], REAL64), u1D, 5, 1D-8)

        c1D = real([0.1D0, 0.0D0, 0.1D0, 0.0D0, 1.0D0], REAL64)
        f1D = real([1, 2, 3, 4, 5], REAL64)
        v1D = real([3.5899280575539568345D0, 3.8776978417266187050D0, 4.1654676258992805755D0, &
                4.5827338129496402878D0, 5.0D0], REAL64)

        call solve_inpainting([5], c1D, f1D, .false., u1D, 2)
        call assertEquals(v1D, u1D, 5, 1D-8)

        v1D = real([1.3525179856115107914D0, 0.0D0, 1.2086330935251798561D0, 0.0D0, &
                12.438848920863309353D0], REAL64)

        call solve_inpainting([5], c1D, f1D, .true., u1D, 2)
        call assertEquals(v1D, u1D, 5, 1D-8)

        c = [0.0D0, 1.0D0, 0.0D0, 0.0D0, 1.0D0]
        solgvo = [0.0D0, 0.46764705882352941176D0, 0.0D0, 0.0D0, 0.68602941176470588235D0]
        solinp = [0.46764705882352941176D0, 0.46764705882352941176D0, 0.54044117647058823529D0, 0.61323529411764705882D0, &
                0.68602941176470588235D0]

        call solve_inpainting([5], c, solgvo, .false., u, 2)
        call assertEquals(solinp, u, 5, 1D-8)

        c = [0.0D0, 0.0D0, 1.0D0, 0.0D0, 1.0D0]
        solgvo = [0.0D0, 0.0D0, 0.4453125D0, 0.0D0, 0.8109375D0]
        solinp = [0.4453125D0, 0.4453125D0, 0.4453125D0, 0.628125D0, 0.8109375D0]

        call solve_inpainting([5], c, solgvo, .false., u, 2)
        call assertEquals(solinp, u, 5, 1D-8)

        c = [1.0D0, 1.0D0, 0.0D0, 0.0D0, 0.0D0]
        solgvo = [0.3D0, 0.61875D0, 0.0D0, 0.0D0, 0.0D0]
        solinp = [0.3D0, 0.61875D0, 0.61875D0, 0.61875D0, 0.61875D0]

        call solve_inpainting([5], c, solgvo, .false., u, 2)
        call assertEquals(solinp, u, 5, 1D-8)
    end subroutine check_solve_inpainting

    subroutine check_solve_inpainting_2d
        implicit none
        real(REAL64), dimension(3*4) :: u, c, f, v

        !! Empty Mask. Solution is 0
        !! 2D Setup: 3x4 Image, data is enter 1D column-wise
        !! Mask
        c = real([0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        !! Image
        f = real([1, 5, 3, &
                  2, 4, 4, &
                  3, 3, 5, &
                  4, 2, 6], REAL64)
        !! Reference solution (exact, computed with Mathematica 11)
        v = real([0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        u = real([1, 1, 1, &
                  1, 1, 1, &
                  1, 1, 1, &
                  1, 1, 1], REAL64)
        call solve_inpainting ([3, 4], c, f, .false., u, 2)
        call assertEquals(v, u, 12, 1D-8)

        !! Full mask solution is input data
        !! 2D Setup: 3x4 Image, data is enter 1D column-wise
        !! Mask
        c = real([1, 1, 1, &
                  1, 1, 1, &
                  1, 1, 1, &
                  1, 1, 1], REAL64)
        !! Image
        f = real([1, 5, 3, &
                  2, 4, 4, &
                  3, 3, 5, &
                  4, 2, 6], REAL64)
        !! Reference solution (exact, computed with Mathematica 11)
        v = real([1, 5, 3, &
                  2, 4, 4, &
                  3, 3, 5, &
                  4, 2, 6], REAL64)
        u = real([0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        call solve_inpainting ([3, 4], c, f, .false., u, 2)
        call assertEquals(v, u, 12, 1D-8)

        !! Sparse mask.
        !! 2D Setup: 3x4 Image, data is enter 1D column-wise
        !! Mask
        c = real([1, 0, 0, &
                  0, 1, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        !! Image
        f = real([1, 5, 3, &
                  2, 4, 4, &
                  3, 3, 5, &
                  4, 2, 6], REAL64)
        !! Reference solution (exact, computed with Mathematica 11)
        v = real([        1.0, 5699.0/2102, 6587.0/2102, &
                  2875.0/1051,         4.0, 7475.0/2102, &
                  3370.0/1051, 7463.0/2102, 3715.0/1051, &
                  7007.0/2102, 3637.0/1051, 3676.0/1051], REAL64)
        u = real([0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        call solve_inpainting ([3, 4], c, f, .false., u, 2)
        call assertEquals(v, u, 12, 1D-4)

        !! Sparse non-binary mask.
        !! 2D Setup: 3x4 Image, data is enter 1D column-wise
        !! Mask
        c = real([  1, 0, 0, &
                    0, 0, 0, &
                    0, 0, 0, &
                    0, 0, 1], REAL64)/2.0
        !! Image
        f = real([1, 5, 3, &
                  2, 4, 4, &
                  3, 3, 5, &
                  4, 2, 6], REAL64)
        !! Reference solution (exact, computed with Mathematica 11)
        v = real([ 604.0/259,  764.0/259,  834.0/259, &
                   789.0/259,  122.0/37 ,  904.0/259, &
                   909.0/259,  137.0/37 , 1024.0/259, &
                   979.0/259, 1049.0/259, 1209.0/259], REAL64)
        u = real([0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        call solve_inpainting ([3, 4], c, f, .false., u, 2)
        call assertEquals(v, u, 12, 1D-4)

        !! Sparse non-binary mask with 1s.
        !! 2D Setup: 3x4 Image, data is enter 1D column-wise
        !! Mask
        c = real([  2, 0, 0, &
                    0, 0, 0, &
                    0, 0, 0, &
                    0, 0, 1], REAL64)/2.0
        !! Image
        f = real([1, 5, 3, &
                  2, 4, 4, &
                  3, 3, 5, &
                  4, 2, 6], REAL64)
        !! Reference solution (exact, computed with Mathematica 11)
        v = real([     1.0,  35.0/19,  42.0/19, &
                   75.0/38,  44.0/19,  49.0/19, &
                   99.0/38, 109.0/38,  61.0/19, &
                  113.0/38, 127.0/38, 159.0/38], REAL64)
        u = real([0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        call solve_inpainting ([3, 4], c, f, .false., u, 2)
        call assertEquals(v, u, 12, 1D-4)

        !! Sparse non-binary mask with values exceeding 1.
        !! 2D Setup: 3x4 Image, data is enter 1D column-wise
        !! Mask
        c = real([  0, 0, 1, &
                    0, 0, 0, &
                    0, 0, 0, &
                    8, 0, 0], REAL64)/4.0
        !! Image
        f = real([1, 5, 3, &
                  2, 4, 4, &
                  3, 3, 5, &
                  4, 2, 6], REAL64)
        !! Reference solution (exact, computed with Mathematica 11)
        v = real([2267, 2239, 2175, &
                  2295, 2275, 2249, &
                  2343, 2317, 2297, &
                  2417, 2353, 2325]/587.0, REAL64)
        u = real([0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        call solve_inpainting ([3, 4], c, f, .false., u, 2)
        call assertEquals(v, u, 12, 1D-4)

        !! Sparse non-binary mask with values below 0.
        !! 2D Setup: 3x4 Image, data is enter 1D column-wise
        !! Mask
        c = real([ 0, 0, 1, &
                   0, 0, 0, &
                   0, 0, 0, &
                  -1, 0, 0], REAL64)/4.0
        !! Image
        f = real([1, 5, 3, &
                  2, 4, 4, &
                  3, 3, 5, &
                  4, 2, 6], REAL64)
        !! Reference solution (exact, computed with Mathematica 11)
        v = real([202, 188, 156, &
                  216, 206, 193, &
                  240, 227, 217, &
                  277, 245, 231]/(-17.0), REAL64)
        u = real([0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0, &
                  0, 0, 0], REAL64)
        call solve_inpainting ([3, 4], c, f, .false., u, 2)
        call assertEquals(v, u, 12, 1D-4)
    end subroutine check_solve_inpainting_2d

    subroutine check_coo_amux_apply_inpainting
        use :: sparse, only: amux, coocsr
        use :: stencil, only: stencil2sparse_size
        implicit none

        integer :: ii, nnz, siz_test

        real(REAL64), dimension(:), allocatable :: data, data2, resu
        integer,      dimension(:), allocatable :: ir, jc, ir2, jc2
        real(REAL64), dimension(:), allocatable :: a, a2

        do ii = 0, 4
            siz_test = 2**(ii+10)
            nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])

            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))
            allocate(data(siz_test), data2(siz_test), resu(siz_test))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test], data2, ir, jc, a, .true.)
            call coocsr (siz_test, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call assertEquals (apply_inpainting_5p([siz_test], data, data2, .true.), resu, siz_test, 1.0D-9)

            deallocate(ir, jc, a, ir2, jc2, a2, data, data2, resu)
        end do

        do ii = 0, 4
            siz_test = 2**(ii+10)
            nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])

            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))
            allocate(data(siz_test), data2(siz_test), resu(siz_test))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test], data2, ir, jc, a, .false.)
            call coocsr (siz_test, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call assertEquals (apply_inpainting_5p([siz_test], data, data2, .false.), resu, siz_test, 1.0D-9)

            deallocate(ir, jc, a, ir2, jc2, a2, data, data2, resu)
        end do

        do ii = 0, 4, 2
            siz_test = 2**(ii+6)
            allocate(data(siz_test), data2(siz_test), resu(siz_test))
            siz_test = 2**((ii+6)/2)

            nnz = stencil2sparse_size ([3, 3], [siz_test, siz_test], &
                    [.false., .true., .false., .true., .true., .true., .false., .true., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**2 + 1), jc2(nnz), a2(nnz))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test, siz_test], data2, ir, jc, a, .true.)
            call coocsr (siz_test**2, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**2, siz_test**2, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_inpainting_5p ([siz_test, siz_test], data, data2, .true.), resu, siz_test**2, 1.0D-9)

            deallocate(data, data2, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 4, 2
            siz_test = 2**(ii+6)
            allocate(data(siz_test), data2(siz_test), resu(siz_test))
            siz_test = 2**((ii+6)/2)

            nnz = stencil2sparse_size ([3, 3], [siz_test, siz_test], &
                    [.false., .true., .false., .true., .true., .true., .false., .true., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**2 + 1), jc2(nnz), a2(nnz))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test, siz_test], data2, ir, jc, a, .false.)
            call coocsr (siz_test**2, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**2, siz_test**2, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_inpainting_5p ([siz_test, siz_test], data, data2, .false.), resu, siz_test**2, 1.0D-9)

            deallocate(data, data2, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 3, 3
            siz_test = 2**(ii+3)
            allocate(data(siz_test), data2(siz_test), resu(siz_test))
            siz_test = 2**((ii+3)/3)

            nnz = stencil2sparse_size ([3, 3, 3], [siz_test, siz_test, siz_test], &
                    [.false., .false., .false., .false., .true., .false., .false., .false., .false., &
                    .false., .true., .false., .true., .true., .true., .false., .true., .false., &
                    .false., .false., .false., .false., .true., .false., .false., .false., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**3 + 1), jc2(nnz), a2(nnz))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test, siz_test, siz_test], data2, ir, jc, a, .true.)
            call coocsr (siz_test**3, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**3, siz_test**3, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_inpainting_5p ([siz_test, siz_test, siz_test], data, data2, .true.), resu, siz_test**3, 1.0D-9)

            deallocate(data, data2, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 3, 3
            siz_test = 2**(ii+3)
            allocate(data(siz_test), data2(siz_test), resu(siz_test))
            siz_test = 2**((ii+3)/3)

            nnz = stencil2sparse_size ([3, 3, 3], [siz_test, siz_test, siz_test], &
                    [.false., .false., .false., .false., .true., .false., .false., .false., .false., &
                    .false., .true., .false., .true., .true., .true., .false., .true., .false., &
                    .false., .false., .false., .false., .true., .false., .false., .false., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**3 + 1), jc2(nnz), a2(nnz))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test, siz_test, siz_test], data2, ir, jc, a, .false.)
            call coocsr (siz_test**3, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**3, siz_test**3, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_inpainting_5p ([siz_test, siz_test, siz_test], data, data2, .false.), resu, siz_test**3, 1.0D-9)

            deallocate(data, data2, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

    end subroutine check_coo_amux_apply_inpainting

    subroutine check_coo_amux_apply_inpainting_T
        use :: sparse, only: amux, coocsr
        use :: stencil, only: stencil2sparse_size
        implicit none

        integer :: ii, nnz, siz_test

        real(REAL64), dimension(:), allocatable :: data, data2, resu
        integer,      dimension(:), allocatable :: ir, jc, ir2, jc2
        real(REAL64), dimension(:), allocatable :: a, a2

        do ii = 0, 4
            siz_test = 2**(ii+10)
            nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])

            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))
            allocate(data(siz_test), data2(siz_test), resu(siz_test))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test], data2, ir, jc, a, .true.)
            call coocsr (siz_test, nnz, jc, ir, a, ir2, jc2, a2)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call assertEquals (apply_inpainting_T_5p([siz_test], data, data2, .true.), resu, siz_test, 1.0D-9)

            deallocate(ir, jc, a, ir2, jc2, a2, data, data2, resu)
        end do

        do ii = 0, 4
            siz_test = 2**(ii+10)
            nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])

            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))
            allocate(data(siz_test), data2(siz_test), resu(siz_test))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test], data2, ir, jc, a, .false.)
            call coocsr (siz_test, nnz, jc, ir, a, ir2, jc2, a2)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call assertEquals (apply_inpainting_T_5p([siz_test], data, data2, .false.), resu, siz_test, 1.0D-9)

            deallocate(ir, jc, a, ir2, jc2, a2, data, data2, resu)
        end do

        do ii = 0, 4, 2
            siz_test = 2**(ii+10)
            allocate(data(siz_test), data2(siz_test), resu(siz_test))
            siz_test = 2**((ii+10)/2)

            nnz = stencil2sparse_size ([3, 3], [siz_test, siz_test], &
                    [.false., .true., .false., .true., .true., .true., .false., .true., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**2 + 1), jc2(nnz), a2(nnz))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test, siz_test], data2, ir, jc, a, .true.)
            call coocsr (siz_test**2, nnz, jc, ir, a, ir2, jc2, a2)
            call amux ( siz_test**2, siz_test**2, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_inpainting_T_5p ([siz_test, siz_test], data, data2, .true.), resu, siz_test**2, 1.0D-9)

            deallocate(data, data2, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 4, 2
            siz_test = 2**(ii+6)
            allocate(data(siz_test), data2(siz_test), resu(siz_test))
            siz_test = 2**((ii+6)/2)

            nnz = stencil2sparse_size ([3, 3], [siz_test, siz_test], &
                    [.false., .true., .false., .true., .true., .true., .false., .true., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**2 + 1), jc2(nnz), a2(nnz))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test, siz_test], data2, ir, jc, a, .false.)
            call coocsr (siz_test**2, nnz, jc, ir, a, ir2, jc2, a2)
            call amux ( siz_test**2, siz_test**2, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_inpainting_T_5p ([siz_test, siz_test], data, data2, .false.), resu, siz_test**2, 1.0D-9)

            deallocate(data, data2, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 3, 3
            siz_test = 2**(ii+3)
            allocate(data(siz_test), data2(siz_test), resu(siz_test))
            siz_test = 2**((ii+3)/3)

            nnz = stencil2sparse_size ([3, 3, 3], [siz_test, siz_test, siz_test], &
                    [.false., .false., .false., .false., .true., .false., .false., .false., .false., &
                    .false., .true., .false., .true., .true., .true., .false., .true., .false., &
                    .false., .false., .false., .false., .true., .false., .false., .false., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**3 + 1), jc2(nnz), a2(nnz))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test, siz_test, siz_test], data2, ir, jc, a, .true.)
            call coocsr (siz_test**3, nnz, jc, ir, a, ir2, jc2, a2)
            call amux ( siz_test**3, siz_test**3, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_inpainting_T_5p ([siz_test, siz_test, siz_test], data, data2, .true.), resu, siz_test**3, 1.0D-9)

            deallocate(data, data2, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

        do ii = 0, 3, 3
            siz_test = 2**(ii+3)
            allocate(data(siz_test), data2(siz_test), resu(siz_test))
            siz_test = 2**((ii+3)/3)

            nnz = stencil2sparse_size ([3, 3, 3], [siz_test, siz_test, siz_test], &
                    [.false., .false., .false., .false., .true., .false., .false., .false., .false., &
                    .false., .true., .false., .true., .true., .true., .false., .true., .false., &
                    .false., .false., .false., .false., .true., .false., .false., .false., .false.])
            allocate(ir(nnz), jc(nnz), a(nnz))
            allocate(ir2(siz_test**3 + 1), jc2(nnz), a2(nnz))

            call random_number(data)
            call random_number(data2)

            call inpainting_5p_sparse_coo ([siz_test, siz_test, siz_test], data2, ir, jc, a, .false.)
            call coocsr (siz_test**3, nnz, jc, ir, a, ir2, jc2, a2)
            call amux ( siz_test**3, siz_test**3, nnz, ir2, jc2, a2, data, resu )

            call assertEquals (apply_inpainting_T_5p ([siz_test, siz_test, siz_test], data, data2, .false.), resu, siz_test**3, 1.0D-9)

            deallocate(data, data2, resu)
            deallocate(ir, jc, a)
            deallocate(ir2, jc2, a2)
        end do

    end subroutine check_coo_amux_apply_inpainting_T

end module test_inpainting
