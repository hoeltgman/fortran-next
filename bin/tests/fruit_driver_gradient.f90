! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver
        use :: fruit
        use :: test_gradient
        implicit none

        integer :: fail_counter

        call init_fruit

        call setup_test_gradient
        write (*,*) ".. running test: check_gradient_fd_stencil"
        call set_unit_name('check_gradient_fd_stencil')
        call run_test_case(check_gradient_fd_stencil, "check_gradient_fd_stencil")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_gradient

        call setup_test_gradient
        write (*,*) ".. running test: check_gradient_fd_sparse_coo"
        call set_unit_name('check_gradient_fd_sparse_coo')
        call run_test_case(check_gradient_fd_sparse_coo, "check_gradient_fd_sparse_coo")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_gradient

        call setup_test_gradient
        write (*,*) ".. running test: check_gradient_fd_sparse_coo_size"
        call set_unit_name('check_gradient_fd_sparse_coo_size')
        call run_test_case(check_gradient_fd_sparse_coo_size, "check_gradient_fd_sparse_coo_size")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_gradient

        call setup_test_gradient
        write (*,*) ".. running test: check_apply_gradient_fd"
        call set_unit_name('check_apply_gradient_fd')
        call run_test_case(check_apply_gradient_fd, "check_apply_gradient_fd")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_gradient

        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver
