! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

module test_convexopt
    use :: fruit
    use :: convexopt
    use :: iso_fortran_env
    implicit none

    type, private, extends(cvx_opt_problem) :: simple_1_data
        real(REAL64) :: a
        real(REAL64) :: f
        real(REAL64) :: g
        real(REAL64) :: l
    end type simple_1_data

    private :: simple_1_resF_c
    private :: simple_1_resG
    private :: simple_1_opK

contains

    ! setup_before_all
    ! setup = setup_before_each
    subroutine setup_test_convexopt
    end subroutine setup_test_convexopt

    ! teardown_before_all
    ! teardown = teardown_before_each
    subroutine teardown_test_convexopt
    end subroutine teardown_test_convexopt

    function simple_1_resF_c (s, x, d) result(y)
        implicit none
        real(REAL64),               intent(in) :: s
        real(REAL64), dimension(:), intent(in) :: x
        class(cvx_opt_problem),     intent(in) :: d

        real(REAL64), dimension(size(x)) :: y

        select type (d)
        type is (simple_1_data)
            y = (x - s*d%f)/(1.0D0+s)
        end select
    end function simple_1_resF_c

    function simple_1_resG (t, x, d) result(y)
        implicit none
        real(REAL64),               intent(in) :: t
        real(REAL64), dimension(:), intent(in) :: x
        class(cvx_opt_problem),     intent(in) :: d

        real(REAL64), dimension(size(x)) :: y

        select type (d)
        type is (simple_1_data)
            y = (x + t*d%l*d%g)/(1+t*d%l)
        end select
    end function simple_1_resG

    function simple_1_opK (x, s, t, d) result(y)
        implicit none
        real(REAL64),   dimension(:), intent(in) :: x
        integer(INT32),               intent(in) :: s
        logical,                      intent(in) :: t
        class(cvx_opt_problem),       intent(in) :: d

        real(REAL64), dimension(size(x)) :: y

        select type (d)
        type is (simple_1_data)
            if (t) then
                y = d%a * x
            else
                y = d%a * x
            end if
        end select
    end function simple_1_opK

    subroutine check_pd_pock_chambolle
        implicit none
        type(simple_1_data)        :: data
        real(REAL64)               :: tau, sigma, L, theta, tol_it, accel, err
        real(REAL64), dimension(1) :: x, x0, y0, sol
        integer(INT32)             :: maxit, its

        data%a =  3.0D0
        data%f =  4.0D0
        data%g = -1.0D0
        data%l =  5.0D0

        L = abs(data%a)
        tau =   1.0D0
        sigma = 1.0D0/(tau*L**2 + 1.0D-4)
        theta = 1.0D0

        maxit = 10000
        accel = 0.0D0

        x0 = 0.0D0
        y0 = 0.0D0
        call pd_pock_chambolle_REAL64_INT32 (data, tau, sigma, theta, x0, y0, &
                                             simple_1_resF_c, simple_1_resG, simple_1_opK, &
                                             maxit, tol_it, x, its, err, accel)

        sol = (data%a*data%f + data%l*data%g)/(data%a*data%a + data%l)
        call assertEquals(sol, x, 1, 1.0D-9)

        !! --------------------

        data%a = -3.0D0
        data%f =  2.0D0
        data%g =  5.0D0
        data%l =  1.0D0

        L = abs(data%a)
        tau =   1.0D0
        sigma = 1.0D0/(tau*L**2 + 1.0D-4)
        theta = 0.0D0

        maxit = 10000
        accel = 0.0D0

        x0 = 0.0D0
        y0 = 0.0D0
        call pd_pock_chambolle (data, tau, sigma, theta, x0, y0, &
                simple_1_resF_c, simple_1_resG, simple_1_opK, &
                maxit, tol_it, x, its, err, accel)

        sol = (data%a*data%f + data%l*data%g)/(data%a*data%a + data%l)
        call assertEquals(sol, x, 1, 1.0D-9)

        !! --------------------

        data%a = -3.0D0
        data%f = -7.0D0
        data%g =  5.0D0
        data%l =  1.0D0

        L = abs(data%a)
        tau =   1.0D0
        sigma = 1.0D0/(tau*L**2 + 1.0D-4)
        theta = 1.0D0

        maxit = 10000
        accel = data%l

        x0 = 0.0D0
        y0 = 0.0D0
        call pd_pock_chambolle (data, tau, sigma, theta, x0, y0, &
                simple_1_resF_c, simple_1_resG, simple_1_opK, &
                maxit, tol_it, x, its, err, accel)

        sol = (data%a*data%f + data%l*data%g)/(data%a*data%a + data%l)
        call assertEquals(sol, x, 1, 1.0D-9)

        !! --------------------

        data%a = -3.0D0
        data%f = -2.0D0
        data%g = -5.0D0
        data%l =  1.0D0

        L = abs(data%a)
        tau =   1.0D0
        sigma = 1.0D0/(tau*L**2 + 1.0D-4)
        theta = 0.0D0

        maxit = 10000
        accel = data%l

        x0 = 0.0D0
        y0 = 0.0D0
        call pd_pock_chambolle (data, tau, sigma, theta, x0, y0, &
                simple_1_resF_c, simple_1_resG, simple_1_opK, &
                maxit, tol_it, x, its, err, accel)

        sol = (data%a*data%f + data%l*data%g)/(data%a*data%a + data%l)
        call assertEquals(sol, x, 1, 1.0D-9)

        !! --------------------

        data%a = -3.0D0
        data%f =  2.0D0
        data%g = -5.0D0
        data%l =  1.0D0

        L = abs(data%a)
        tau =   1.0D0
        sigma = 1.0D0/(tau*L**2 + 1.0D-4)
        theta = 0.5D0

        maxit = 10000
        accel = 0.0D0

        x0 = 0.0D0
        y0 = 0.0D0
        call pd_pock_chambolle (data, tau, sigma, theta, x0, y0, &
                simple_1_resF_c, simple_1_resG, simple_1_opK, &
                maxit, tol_it, x, its, err, accel)

        sol = (data%a*data%f + data%l*data%g)/(data%a*data%a + data%l)
        call assertEquals(sol, x, 1, 1.0D-9)

        !! --------------------

        data%a = -3.0D0
        data%f = -7.0D0
        data%g = -5.0D0
        data%l =  1.0D0

        L = abs(data%a)
        tau =   1.0D0
        sigma = 1.0D0/(tau*L**2 + 1.0D-4)
        theta = 0.5D0

        maxit = 10000
        accel = data%l

        x0 = 0.0D0
        y0 = 0.0D0
        call pd_pock_chambolle (data, tau, sigma, theta, x0, y0, &
                simple_1_resF_c, simple_1_resG, simple_1_opK, &
                maxit, tol_it, x, its, err, accel)

        sol = (data%a*data%f + data%l*data%g)/(data%a*data%a + data%l)
        call assertEquals(sol, x, 1, 1.0D-9)

    end subroutine check_pd_pock_chambolle
end module test_convexopt
