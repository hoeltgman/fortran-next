! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

module test_maskopt
    use :: fruit
    use :: maskopt
    use :: iso_fortran_env
    implicit none

contains

    ! setup_before_all
    ! setup = setup_before_each
    subroutine setup_test_maskopt
    end subroutine setup_test_maskopt

    ! teardown_before_all
    ! teardown = teardown_before_each
    subroutine teardown_test_maskopt
    end subroutine teardown_test_maskopt

    subroutine check_primal_energy
        implicit none
        call assertEquals(0.5D0, &
                primal_energy([1], [1.0D0], [1.0D0], [0.0D0], [1.0D0], [1.0D0], 0.0D0, 0.0D0, 0.0D0, 0.0D0), &
                1.0D-6)
        call assertEquals(2.0D0, &
                primal_energy([1], [2.0D0], [1.0D0], [0.0D0], [1.0D0], [1.0D0], 0.0D0, 0.0D0, 0.0D0, 0.0D0), &
                1.0D-6)
    end subroutine check_primal_energy

    subroutine check_resF_c_nGrad
        implicit none

        type(mask_opt_problem_REAL64_INT32) :: data
        integer(INT32), dimension(1), target :: dim
        real(REAL64),   dimension(5), target :: ubar, cbar, f, B, g

        dim = [5]

        ubar = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0]
        cbar = [1.0D0, 1.0D0, 1.0D0, 1.0D0, 1.0D0]
        f    = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0]
        B    = [1.0D0, 1.0D0, 1.0D0, 1.0D0, 1.0D0]
        g    = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0]

        data%data_dim => dim
        data%lambda   =  1.0D0
        data%epsi     =  1.0D0
        data%mu       =  1.0D0
        data%nu       =  1.0D0
        data%ubar     => ubar
        data%cbar     => cbar
        data%f        => f
        data%B        => B
        data%g        => g

        call assertEquals(f,   resF_c_nGrad(0.0D0, f, data), 5, 1.0D-9)
        call assertEquals(f-g, resF_c_nGrad(1.0D0, f, data), 5, 1.0D-9)
    end subroutine check_resF_c_nGrad

    subroutine check_resG_nGrad
        implicit none

        type(mask_opt_problem_REAL64_INT32) :: data
        integer(INT32), dimension(1), target :: dim
        real(REAL64),   dimension(6), target :: ubar, cbar, f, B, g

        dim = [6]

        ubar = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]
        cbar = [0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0]
        f    = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]
        B    = [1.0D0, 1.0D0, 1.0D0, 1.0D0, 1.0D0, 1.0D0]
        g    = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]

        data%data_dim => dim
        data%lambda   =  0.0D0
        data%epsi     =  0.0D0
        data%mu       =  0.0D0
        data%nu       =  0.0D0
        data%ubar     => ubar
        data%cbar     => cbar
        data%f        => f
        data%B        => B
        data%g        => g

        call assertEquals( &
                [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0], &
                resG_nGrad(0.0D0, f, data), &
                6, 1.0D-9)

        call assertEquals( &
                [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0], &
                resG_nGrad(1.0D0, f, data), &
                6, 1.0D-9)
    end subroutine check_resG_nGrad

    subroutine check_opK_nGrad
        implicit none

        type(mask_opt_problem_REAL64_INT32)               :: data
        integer(INT32), parameter                         :: n = 6
        integer(INT32), dimension(1), target              :: dim
        real(REAL64),   dimension(:), target, allocatable :: ubar, cbar, f, B, g
        real(REAL64),   dimension(:),         allocatable :: result, result_transp
        real(REAL64),   dimension(:),         allocatable :: input,  input_transp

        allocate(ubar(n), cbar(n), f(n), B(n), g(n))
        allocate(result(n), result_transp(2*n), input(2*n), input_transp(n))

        dim = [n]

        ubar = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]
        cbar = [1.0D0, 1.0D0, 1.0D0, 1.0D0, 1.0D0, 1.0D0]
        f    = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]
        B    = [0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0]
        g    = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]

        data%data_dim => dim
        data%lambda   =  0.0D0
        data%epsi     =  0.0D0
        data%mu       =  0.0D0
        data%nu       =  0.0D0
        data%ubar     => ubar
        data%cbar     => cbar
        data%f        => f
        data%B        => B
        data%g        => g

        input = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0, &
                 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0, 0.0D0]
        result = opK_nGrad(input, n, .false., data)
        call assertEquals([1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0], result, n, 1.0D-9)

        dim = [n]

        ubar = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]
        cbar = [1.0D0, 1.0D0, 1.0D0, 1.0D0, 1.0D0, 1.0D0]
        f    = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]
        B    = [2.0D0, 2.0D0, 2.0D0, 2.0D0, 2.0D0, 2.0D0]
        g    = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]

        data%data_dim => dim
        data%lambda   =  0.0D0
        data%epsi     =  0.0D0
        data%mu       =  0.0D0
        data%nu       =  0.0D0
        data%ubar     => ubar
        data%cbar     => cbar
        data%f        => f
        data%B        => B
        data%g        => g

        input_transp = [1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0]
        result_transp = opK_nGrad(input_transp, 2*n, .true., data)
        call assertEquals([1.0D0, 2.0D0, 3.0D0, 4.0D0, 5.0D0, 6.0D0, 2.0D0, 4.0D0, 6.0D0, 8.0D0, 10.0D0, 12.0D0], &
                result_transp, 2*n, 1.0D-9)

        deallocate(ubar, cbar, f, B, g)
        deallocate(result, result_transp, input, input_transp)
    end subroutine check_opK_nGrad

    subroutine check_opt_mask_nGrad
        implicit none
        call assertEquals(1.0D0, 1.0D0, 1.0D-6)
    end subroutine check_opt_mask_nGrad

end module test_maskopt
