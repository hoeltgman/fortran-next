! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver
        use :: fruit
        use :: test_stencil
        implicit none

        integer :: fail_counter

        call init_fruit

        !        call setup_test_stencil
        !        write (*,*) ".. running test: check_get_neighbour_mask_1"
        !        call set_unit_name('check_get_neighbour_mask_1')
        !        call run_test_case(check_get_neighbour_mask_1, "check_get_neighbour_mask_1")
        !        write(*,*) ""
        !        write(*,*) ".. done."
        !        write(*,*) ""
        !        call teardown_test_stencil
        !
        !        call setup_test_stencil
        !        write (*,*) ".. running test: check_get_neighbour_mask_2"
        !        call set_unit_name('check_get_neighbour_mask_2')
        !        call run_test_case(check_get_neighbour_mask_2, "check_get_neighbour_mask_2")
        !        write(*,*) ""
        !        write(*,*) ".. done."
        !        write(*,*) ""
        !        call teardown_test_stencil
        !
        !        call setup_test_stencil
        !        write (*,*) ".. running test: check_stencilcoo_1"
        !        call set_unit_name('check_stencilcoo_1')
        !        call run_test_case(check_stencilcoo_1, "check_stencilcoo_1")
        !        write(*,*) ""
        !        write(*,*) ".. done."
        !        write(*,*) ""
        !        call teardown_test_stencil
        !
        !        call setup_test_stencil
        !        write (*,*) ".. running test: check_stencilcoo_2"
        !        call set_unit_name('check_stencilcoo_2')
        !        call run_test_case(check_stencilcoo_2, "check_stencilcoo_2")
        !        write(*,*) ""
        !        write(*,*) ".. done."
        !        write(*,*) ""
        !        call teardown_test_stencil
        !
        call setup_test_stencil
        write (*,*) ".. running test: check_stencillocs"
        call set_unit_name('check_stencillocs')
        call run_test_case(check_stencillocs, "check_stencillocs")
        write(*,*) ""
        write(*,*) ".. done."
        write(*,*) ""
        call teardown_test_stencil

        call setup_test_stencil
        write (*,*) ".. running test: check_stencilmask"
        call set_unit_name('check_stencilmask')
        call run_test_case(check_stencilmask, "check_stencilmask")
        write(*,*) ""
        write(*,*) ".. done."
        write(*,*) ""
        call teardown_test_stencil

        call setup_test_stencil
        write (*,*) ".. running test: check_stencil2sparse_size"
        call set_unit_name('check_stencil2sparse_size')
        call run_test_case(check_stencil2sparse_size, "check_stencil2sparse_size")
        write(*,*) ""
        write(*,*) ".. done."
        write(*,*) ""
        call teardown_test_stencil

        call setup_test_stencil
        write (*,*) ".. running test: check_const_stencil2sparse"
        call set_unit_name('check_const_stencil2sparse')
        call run_test_case(check_const_stencil2sparse, "check_const_stencil2sparse")
        write(*,*) ""
        write(*,*) ".. done."
        write(*,*) ""
        call teardown_test_stencil

        call setup_test_stencil
        write (*,*) ".. running test: check_stencil2sparse"
        call set_unit_name('check_stencil2sparse')
        call run_test_case(check_stencil2sparse, "check_stencil2sparse")
        write(*,*) ""
        write(*,*) ".. done."
        write(*,*) ""
        call teardown_test_stencil

        call setup_test_stencil
        write (*,*) ".. running test: check_convolve"
        call set_unit_name('check_convolve')
        call run_test_case(check_convolve, "check_convolve")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_stencil

        call setup_test_stencil
        write (*,*) ".. running test: check_create_5p_stencil"
        call set_unit_name('check_create_5p_stencil')
        call run_test_case(check_create_5p_stencil, "check_create_5p_stencil")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_stencil

        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver
