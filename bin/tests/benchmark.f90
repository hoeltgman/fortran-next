module reference
    use :: iso_fortran_env
    use :: iso_c_binding
    implicit none
    private

    public :: time_laplace_1
    public :: time_laplace_2
contains

    function time_laplace_1 (n) result (t)
        integer, intent(in) :: n
        real                :: t

        INTEGER :: &
                nb_ticks_initial, & ! initial value of the clock tick counter
                nb_ticks_final,   & ! final value of the clock tick counter
                nb_ticks_max,     & ! maximum value of the clock counter
                nb_ticks_sec,     & ! number of clock ticks per second
                nb_ticks            ! number of clock ticks of the code
        real :: start, finish
        real(c_double), dimension(:), allocatable :: data, resu
        integer :: jj

        allocate(data(n), resu(n))

        t = 0.0
        do jj = 1, 10
            call random_number(data)
            resu = 0.0
            call SYSTEM_clock(COUNT_RATE=nb_ticks_sec, COUNT_MAX=nb_ticks_max)
            call SYSTEM_clock(COUNT=nb_ticks_initial)
            call cpu_time(start)
            call apply_laplacian_1 (int(n, c_long), data, resu)
            call cpu_time(finish)
            call SYSTEM_clock(COUNT=nb_ticks_final)
            nb_ticks = nb_ticks_final - nb_ticks_initial
            if (nb_ticks_final < nb_ticks_initial) nb_ticks = nb_ticks + nb_ticks_max

            ! t = t + real(nb_ticks) / real(nb_ticks_sec)
            t = t + finish - start
        end do

        t = t/10.0
        deallocate(data, resu)
    end function time_laplace_1

    function time_laplace_2 (nr, nc) result (t)
        integer, intent(in) :: nr, nc
        real                :: t

        INTEGER :: &
                nb_ticks_initial, & ! initial value of the clock tick counter
                nb_ticks_final,   & ! final value of the clock tick counter
                nb_ticks_max,     & ! maximum value of the clock counter
                nb_ticks_sec,     & ! number of clock ticks per second
                nb_ticks            ! number of clock ticks of the code
        real :: start, finish
        real(c_double), dimension(:,:), allocatable :: data, resu
        integer :: jj

        allocate(data(nr, nc), resu(nr, nc))

        t = 0.0
        do jj = 1, 10
            call random_number(data(nr, nc))
            resu = 0.0
            call SYSTEM_clock(COUNT_RATE=nb_ticks_sec, COUNT_MAX=nb_ticks_max)
            call SYSTEM_clock(COUNT=nb_ticks_initial)
            call cpu_time(start)
            call apply_laplacian_2 (int(nr, c_long), int(nc, c_long), data, resu)
            call cpu_time(finish)
            call SYSTEM_clock(COUNT=nb_ticks_final)
            nb_ticks = nb_ticks_final - nb_ticks_initial
            if (nb_ticks_final < nb_ticks_initial) nb_ticks = nb_ticks + nb_ticks_max

            ! t = t + real(nb_ticks) / real(nb_ticks_sec)
            t = t + finish - start
        end do

        t = t/10.0
        deallocate(data, resu)
    end function time_laplace_2

    pure subroutine mirror_1(n, x, y)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                  :: n
        real(kind=c_double),  intent(in),  dimension(n)   :: x
        real(kind=c_double),  intent(out), dimension(n+2) :: y

        y = 0.0D0

        y(2:(n+1)) = x

        y(  1) = x(1)
        y(n+2) = x(n)
    end subroutine mirror_1

    pure subroutine mirror_2(nr, nc, x, y)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                        :: nr
        integer(kind=c_long), intent(in)                        :: nc
        real(kind=c_double),  intent(in),  dimension(nr,  nc)   :: x
        real(kind=c_double),  intent(out), dimension(nr+2,nc+2) :: y

        y = 0.0D0

        y(2:(nr+1),2:(nc+1)) = x

        y(2:(nr+1), 1   ) = x(1:nr,  1)
        y(2:(nr+1), nc+2) = x(1:nr, nc)

        y(1,    2:(nc+1)) = x(1 , 1:nc)
        y(nr+2, 2:(nc+1)) = x(nr, 1:nc)

        y(1,       1) = x(1,   1)
        y(1,    nc+2) = x(1,  nc)
        y(nr+2,    1) = x(nr,  1)
        y(nr+2, nc+2) = x(nr, nc)
    end subroutine mirror_2

    pure subroutine mirror_3(nr, nc, nd, x, y)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                             :: nr
        integer(kind=c_long), intent(in)                             :: nc
        integer(kind=c_long), intent(in)                             :: nd
        real(kind=c_double),  intent(in),  dimension(nr,  nc,  nd)   :: x
        real(kind=c_double),  intent(out), dimension(nr+2,nc+2,nd+2) :: y

        integer(kind=c_long) :: ii

        y = 0.0D0

        !! Mirror all interior frames.
        do ii = 1, nd
            call mirror_2(nr, nc, x(1:nr, 1:nc, ii), y(1:(nr+2), 1:(nc+2), ii+1))
        end do

        !! Mirror 1st and last frame.
        y(1:(nr+2), 1:(nc+2),    1) = y(1:(nr+2), 1:(nc+2),    2)
        y(1:(nr+2), 1:(nc+2), nd+2) = y(1:(nr+2), 1:(nc+2), nd+1)
    end subroutine mirror_3


    pure subroutine apply_laplacian_1 (n, in, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                :: n
        real(kind=c_double),  intent(in),  dimension(n) :: in
        real(kind=c_double),  intent(out), dimension(n) :: out

        integer(kind=c_long)                            :: ii

        forall ( ii = 2:(n-1) )
            out(ii) = in(ii-1) + in(ii+1) - 2.0D0*in(ii)
        end forall

        out(1) = in(2)   - in(1)
        out(n) = in(n-1) - in(n)
    end subroutine apply_laplacian_1

    pure subroutine apply_laplacian_2 (nr, nc, img, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                    :: nr
        integer(kind=c_long), intent(in)                    :: nc
        real(kind=c_double),  intent(in),  dimension(nr,nc) :: img
        real(kind=c_double),  intent(out), dimension(nr,nc) :: out

        integer(kind=c_long)                                :: ii, jj

        forall ( ii = 2:(nc-1) )
            out(1, ii) = img(1, ii-1) + img(1, ii+1) + img(2,   ii) - 3.0d0*img(1, ii)
            out(nr,ii) = img(nr,ii-1) + img(nr,ii+1) + img(nr-1,ii) - 3.0d0*img(nr,ii)
        end forall

        forall ( ii = 2:(nr-1) )
            out(ii, 1) = img(ii-1, 1) + img(ii+1, 1) + img(ii,   2) - 3.0d0*img(ii, 1)
            out(ii,nc) = img(ii-1,nc) + img(ii+1,nc) + img(ii,nc-1) - 3.0d0*img(ii,nc)
        end forall

        forall ( jj = 2:(nc-1), ii = 2:(nr-1) )
            out(ii,jj) = img(ii,jj-1) + img(ii,jj+1) + img(ii+1,jj) + img(ii-1,jj) - 4.0d0*img(ii,jj)
        end forall

        out( 1, 1) = img(   1,   2) + img(   2,   1) - 2.0d0*img( 1, 1)
        out( 1,nc) = img(   1,nc-1) + img(   2,  nc) - 2.0d0*img( 1,nc)
        out(nr, 1) = img(  nr,   2) + img(nr-1,   1) - 2.0d0*img(nr, 1)
        out(nr,nc) = img(nr-1,  nc) + img(  nr,nc-1) - 2.0d0*img(nr,nc)
    end subroutine apply_laplacian_2

    pure subroutine apply_laplacian_3(nr, nc, nd, img, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                       :: nr
        integer(kind=c_long), intent(in)                       :: nc
        integer(kind=c_long), intent(in)                       :: nd
        real(kind=c_double),  intent(in),  dimension(nr,nc,nd) :: img
        real(kind=c_double),  intent(out), dimension(nr,nc,nd) :: out

        real(kind=c_double), dimension(:,:,:), allocatable :: tmp1, tmp2

        integer(kind=c_long)                                :: ii, jj, kk

        allocate(tmp1(nr+2,nc+2,nd+2), tmp2(nr+2,nc+2,nd+2))

        call mirror_3(nr, nc, nd, img, tmp1)

        forall( kk = 2:(nd+1), jj = 2:(nc+1), ii = 2:(nr+1) )
            tmp2(ii, jj, kk) = tmp1(ii, jj-1, kk) + tmp1(ii, jj+1, kk) + &
                    tmp1(ii+1, jj, kk) + tmp1(ii-1, jj, kk) + &
                    tmp1(ii, jj, kk+1) + tmp1(ii, jj, kk-1) - &
                    6.0d0*tmp1(ii, jj, kk)
        end forall

        out = tmp2(2:(nr+1), 2:(nc+1), 2:(nd+1))
        deallocate(tmp1, tmp2)

    end subroutine apply_laplacian_3

    pure subroutine apply_inpainting_1 (n, c, in, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                :: n
        real(kind=c_double),  intent(in),  dimension(n) :: c
        real(kind=c_double),  intent(in),  dimension(n) :: in
        real(kind=c_double),  intent(out), dimension(n) :: out

        integer(kind=c_long)                            :: ii

        forall (ii = 2:(n-1))
            out(ii) = (c(ii)-1.0D0)*in(ii-1) + (c(ii)-1.0D0)*in(ii+1) + (2.0D0-c(ii))*in(ii)
        end forall

        out(1) = in(1) + (c(1)-1.0D0)*in(2)
        out(n) = in(n) + (c(n)-1.0D0)*in(n-1)
    end subroutine apply_inpainting_1

    pure subroutine apply_inpainting_2 (nr, nc, c, img, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                    :: nr
        integer(kind=c_long), intent(in)                    :: nc
        real(kind=c_double),  intent(in),  dimension(nr,nc) :: c
        real(kind=c_double),  intent(in),  dimension(nr,nc) :: img
        real(kind=c_double),  intent(out), dimension(nr,nc) :: out

        integer(kind=c_long)                                :: ii, jj

        forall( ii = 2:(nc-1) )
            ! Handle 1st row for non-corner pixels.
            ! Existing neighboring pixels: (1, jj-1), (1, jj+1), (2, jj)
            out(1, ii) &
                    = (c( 1, ii)-1.0d0)*img(1, ii-1) &
                    + (c( 1, ii)-1.0d0)*img(1, ii+1) &
                    + (c( 1, ii)-1.0d0)*img(2,   ii) &
                    + (3.0d0-2.0d0*c( 1, ii))*img(1, ii)

            ! Handle last row for non-corner pixels.
            ! Existing neighboring pixels: (nr, jj-1), (nr, jj+1), (nr-1, jj)
            out(nr, ii) &
                    = (c(nr, ii)-1.0d0)*img(nr, ii-1) &
                    + (c(nr, ii)-1.0d0)*img(nr, ii+1) &
                    + (c(nr, ii)-1.0d0)*img(nr-1, ii) &
                    + (3.0d0-2.0d0*c(nr, ii))*img(nr, ii)
        end forall

        forall( ii = 2:(nr-1) )
            out(ii, 1) &
                    = (c(ii, 1)-1.0d0)*img(ii-1, 1) &
                    + (c(ii, 1)-1.0d0)*img(ii+1, 1) &
                    + (c(ii, 1)-1.0d0)*img(ii,   2) &
                    + (3.0d0-2.0d0*c(ii, 1))*img(ii, 1)

            out(ii,nc) &
                    = (c(ii, nc)-1.0d0)*img(ii-1, nc)   &
                    + (c(ii, nc)-1.0d0)*img(ii+1, nc)   &
                    + (c(ii, nc)-1.0d0)*img(ii,   nc-1) &
                    + (3.0d0-2.0d0*c(ii, nc))*img(ii, nc)
        end forall

        forall( jj = 2:(nc-1), ii = 2:(nr-1) )
            out(ii,jj) = (c(ii,jj)-1.0d0)*img(ii,jj-1) &
                    + (c(ii,jj)-1.0d0)*img(ii,jj+1) &
                    + (c(ii,jj)-1.0d0)*img(ii+1,jj) &
                    + (c(ii,jj)-1.0d0)*img(ii-1,jj) &
                    + (4.0d0-3.0d0*c(ii,jj))*img(ii,jj)
        end forall

        out( 1, 1) = (c( 1, 1)-1.0d0)*img(   1,   2) &
                + (c( 1, 1)-1.0d0)*img(   2,   1) &
                + (2.0d0-c( 1, 1))*img( 1, 1)

        out( 1,nc) = (c( 1,nc)-1.0d0)*img(   1,nc-1) &
                + (c( 1,nc)-1.0d0)*img(   2,  nc) &
                + (2.0d0-c( 1,nc))*img( 1,nc)

        out(nr, 1) = (c(nr, 1)-1.0d0)*img(  nr,   2) &
                + (c(nr, 1)-1.0d0)*img(nr-1,   1) &
                + (2.0d0-c(nr, 1))*img(nr, 1)

        out(nr,nc) = (c(nr,nc)-1.0d0)*img(nr-1,  nc) &
                + (c(nr,nc)-1.0d0)*img(  nr,nc-1) &
                + (2.0d0-c(nr,nc))*img(nr,nc)
    end subroutine apply_inpainting_2

    pure subroutine apply_inpainting_3 (nr, nc, nd, c, img, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                       :: nr
        integer(kind=c_long), intent(in)                       :: nc
        integer(kind=c_long), intent(in)                       :: nd
        real(kind=c_double),  intent(in),  dimension(nr,nc,nd) :: c
        real(kind=c_double),  intent(in),  dimension(nr,nc,nd) :: img
        real(kind=c_double),  intent(out), dimension(nr,nc,nd) :: out

        real(kind=c_double), dimension(:,:,:), allocatable :: tmp1, tmp2, tmp3

        integer(kind=c_long)                                :: ii, jj, kk

        allocate(tmp1(nr+2,nc+2,nd+2), tmp2(nr+2,nc+2,nd+2), tmp3(nr+2, nc+2, nd+2))

        call mirror_3(nr, nc, nd, img, tmp1)
        call mirror_3(nr, nc, nd,   c, tmp3)

        forall( kk = 2:(nd+1), jj = 2:(nc+1), ii = 2:(nr+1) )
            tmp2(ii, jj, kk) = &
                    (tmp3(ii, jj, kk)-1.0D0)*tmp1(ii,   jj-1, kk)   + &
                    (tmp3(ii, jj, kk)-1.0D0)*tmp1(ii,   jj+1, kk)   + &
                    (tmp3(ii, jj, kk)-1.0D0)*tmp1(ii-1, jj,   kk)   + &
                    (tmp3(ii, jj, kk)-1.0D0)*tmp1(ii+1, jj,   kk)   + &
                    (tmp3(ii, jj, kk)-1.0D0)*tmp1(ii,   jj,   kk-1) + &
                    (tmp3(ii, jj, kk)-1.0D0)*tmp1(ii,   jj,   kk+1) + &
                    (6.0d0-5.0d0*tmp3(ii, jj, kk))*tmp1(ii, jj, kk)
        end forall

        out = tmp2(2:(nr+1), 2:(nc+1), 2:(nd+1))
        deallocate(tmp1, tmp2, tmp3)
    end subroutine apply_inpainting_3

    pure subroutine apply_inpainting_1_T (n, c, in, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                :: n
        real(kind=c_double),  intent(in),  dimension(n) :: c
        real(kind=c_double),  intent(in),  dimension(n) :: in
        real(kind=c_double),  intent(out), dimension(n) :: out

        integer(kind=c_long)                            :: ii

        forall (ii = 2:(n-1))
            out(ii) = (c(ii-1)-1.0D0)*in(ii-1) + (c(ii+1)-1.0D0)*in(ii+1) + (2.0D0-c(ii))*in(ii)
        end forall

        out(1) = in(1) + (c(2)-1.0D0)*in(2)
        out(n) = in(n) + (c(n-1)-1.0D0)*in(n-1)
    end subroutine apply_inpainting_1_T

    pure subroutine apply_inpainting_2_T(nr, nc, c, img, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                    :: nr
        integer(kind=c_long), intent(in)                    :: nc
        real(kind=c_double),  intent(in),  dimension(nr,nc) :: c
        real(kind=c_double),  intent(in),  dimension(nr,nc) :: img
        real(kind=c_double),  intent(out), dimension(nr,nc) :: out

        integer(kind=c_long)                                :: ii, jj

        forall( ii = 2:(nc-1) )
            out(1, ii) = (c( 1,ii-1)-1.0d0)*img(1, ii-1) &
                    + (c( 1,ii+1)-1.0d0)*img(1, ii+1) &
                    + (c( 2,ii)-1.0d0)*img(2,   ii) &
                    + (3.0d0-2.0d0*c( 1,ii))*img(1, ii)

            out(nr,ii) = (c(nr,ii-1)-1.0d0)*img(nr,ii-1) &
                    + (c(nr,ii+1)-1.0d0)*img(nr,ii+1) &
                    + (c(nr-1,ii)-1.0d0)*img(nr-1,ii) &
                    + (3.0d0-2.0d0*c(nr,ii))*img(nr,ii)
        end forall

        forall( ii = 2:(nr-1) )
            out(ii, 1) = (c(ii-1, 1)-1.0d0)*img(ii-1, 1) &
                    + (c(ii+1, 1)-1.0d0)*img(ii+1, 1) &
                    + (c(ii, 2)-1.0d0)*img(ii,   2) &
                    + (3.0d0-2.0d0*c(ii, 1))*img(ii, 1)

            out(ii,nc) = (c(ii-1,nc)-1.0d0)*img(ii-1,nc) &
                    + (c(ii+1,nc)-1.0d0)*img(ii+1,nc) &
                    + (c(ii,nc-1)-1.0d0)*img(ii,nc-1) &
                    + (3.0d0-2.0d0*c(ii,nc))*img(ii,nc)
        end forall

        forall( jj = 2:(nc-1), ii = 2:(nr-1) )
            out(ii,jj) = (c(ii,jj-1)-1.0d0)*img(ii,jj-1) &
                    + (c(ii,jj+1)-1.0d0)*img(ii,jj+1) &
                    + (c(ii+1,jj)-1.0d0)*img(ii+1,jj) &
                    + (c(ii-1,jj)-1.0d0)*img(ii-1,jj) &
                    + (4.0d0-3.0d0*c(ii,jj))*img(ii,jj)

        end forall

        out( 1, 1) = (c( 1, 2)-1.0d0)*img(   1,   2) &
                + (c( 2, 1)-1.0d0)*img(   2,   1) &
                + (2.0d0-c( 1, 1))*img( 1, 1)

        out( 1,nc) = (c( 1,nc-1)-1.0d0)*img(   1,nc-1) &
                + (c( 2,nc)-1.0d0)*img(   2,  nc) &
                + (2.0d0-c( 1,nc))*img( 1,nc)

        out(nr, 1) = (c(nr, 2)-1.0d0)*img(  nr,   2) &
                + (c(nr-1, 1)-1.0d0)*img(nr-1,   1) &
                + (2.0d0-c(nr, 1))*img(nr, 1)

        out(nr,nc) = (c(nr-1,nc)-1.0d0)*img(nr-1,  nc) &
                + (c(nr,nc-1)-1.0d0)*img(  nr,nc-1) &
                + (2.0d0-c(nr,nc))*img(nr,nc)
    end subroutine apply_inpainting_2_T

    pure subroutine apply_inpainting_3_T(nr, nc, nd, c, img, out)
        use :: iso_c_binding
        implicit none

        integer(kind=c_long), intent(in)                       :: nr
        integer(kind=c_long), intent(in)                       :: nc
        integer(kind=c_long), intent(in)                       :: nd
        real(kind=c_double),  intent(in),  dimension(nr,nc,nd) :: c
        real(kind=c_double),  intent(in),  dimension(nr,nc,nd) :: img
        real(kind=c_double),  intent(out), dimension(nr,nc,nd) :: out

        real(kind=c_double), dimension(:,:,:), allocatable :: tmp1, tmp2, tmp3

        integer(kind=c_long)                                :: ii, jj, kk

        allocate(tmp1(nr+2,nc+2,nd+2), tmp2(nr+2,nc+2,nd+2), tmp3(nr+2, nc+2, nd+2))

        call mirror_3(nr, nc, nd, img, tmp1)
        call mirror_3(nr, nc, nd,   c, tmp3)

        forall( kk = 2:(nd+1), jj = 2:(nc+1), ii = 2:(nr+1) )
            tmp2(ii, jj, kk) = &
                    (tmp3(ii,   jj-1, kk  )-1.0D0)*tmp1(ii,   jj-1, kk)   + &
                    (tmp3(ii,   jj+1, kk  )-1.0D0)*tmp1(ii,   jj+1, kk)   + &
                    (tmp3(ii+1, jj,   kk  )-1.0D0)*tmp1(ii+1, jj,   kk)   + &
                    (tmp3(ii-1, jj,   kk  )-1.0D0)*tmp1(ii-1, jj,   kk)   + &
                    (tmp3(ii,   jj,   kk+1)-1.0D0)*tmp1(ii,   jj,   kk+1) + &
                    (tmp3(ii,   jj,   kk-1)-1.0D0)*tmp1(ii,   jj,   kk-1) + &
                    (6.0d0-5.0d0*tmp3(ii, jj, kk))*tmp1(ii, jj, kk)
        end forall

        out = tmp2(2:(nr+1), 2:(nc+1), 2:(nd+1))
        deallocate(tmp1, tmp2, tmp3)
    end subroutine apply_inpainting_3_T

end module reference

program benchmark
    use :: iso_fortran_env, only: OUTPUT_UNIT, REAL64, INT64
    use :: laplace,         only: apply_laplace_5p, laplace_5p_sparse_coo
    use :: inpainting,      only: apply_inpainting_5p
    use :: stencil,         only: stencil2sparse_size
    use :: sparse,          only: amux, coocsr
    use :: reference,       only: time_laplace_1, time_laplace_2
    implicit none

    real(REAL64),   dimension(:), allocatable :: data, data2, resu
    integer,         dimension(:), allocatable :: ir, jc, ir2, jc2
    real(REAL64),   dimension(:), allocatable :: a, a2
    integer                                   :: ii, jj
    integer                                   :: num_test, rep_test, siz_test
    integer                                   :: nnz
    real                                      :: start, finish, tot_time

    num_test = 10
    rep_test = 16

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating apply_laplace_5p (with Neumann) in 1D..."

    do ii = 0, num_test-1
        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call cpu_time(start)
            resu = apply_laplace_5p ([siz_test], data, .true.)
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, resu)
        write (OUTPUT_UNIT,*) "Evaluating apply_laplace_5p in 1D on ", siz_test, " elements in ", tot_time, " seconds."
        write (OUTPUT_UNIT,*) "Reference time: ", time_laplace_1(siz_test)
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo (with Neumann) and amux in 1D..."

    do ii = 0, num_test-1
        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))

        nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])
        allocate(ir(nnz), jc(nnz), a(nnz))
        allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            ir = 0
            jc = 0
            a  = 0.0D0
            call cpu_time(start)
            call laplace_5p_sparse_coo ([siz_test], ir, jc, a, .true.)
            call coocsr (siz_test + 1, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call cpu_time(finish)
            if (maxval(abs( resu - apply_laplace_5p ([siz_test], data, .true.))) .gt. 1.0D-8) then
                write(OUTPUT_UNIT,*) "!!!!!!! Error !!!!!!", maxval(abs( resu - apply_laplace_5p ([siz_test], data, .true.)))
            end if
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, resu)
        deallocate(ir, jc, a)
        deallocate(ir2, jc2, a2)
        write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo and amux in 1D on ", siz_test, " elements in ", tot_time, " seconds."
        write (OUTPUT_UNIT,*) "Reference time: ", time_laplace_1(siz_test)
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo (with Neumann) and amux in 1D (saved alloc)..."

    do ii = 0, num_test-1
        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))

        nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])
        allocate(ir(nnz), jc(nnz), a(nnz))
        allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))

        ir = 0
        jc = 0
        a  = 0.0D0
        call laplace_5p_sparse_coo ([siz_test], ir, jc, a, .true.)
        call coocsr (siz_test + 1, nnz, ir, jc, a, ir2, jc2, a2)
        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call cpu_time(start)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call cpu_time(finish)
            if (maxval(abs( resu - apply_laplace_5p ([siz_test], data, .true.))) .gt. 1.0D-8) then
                write(OUTPUT_UNIT,*) "!!!!!!! Error !!!!!!", maxval(abs( resu - apply_laplace_5p ([siz_test], data, .true.)))
            end if
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, resu)
        deallocate(ir, jc, a)
        deallocate(ir2, jc2, a2)
        write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo and amux in 1D on ", siz_test, " elements in ", tot_time, " seconds."
        write (OUTPUT_UNIT,*) "Reference time: ", time_laplace_1(siz_test)
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating apply_laplace_5p (with Neumann) in 2D..."

    do ii = 0, num_test-1, 2

        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))
        siz_test = 2**((ii+10)/2)

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call cpu_time(start)
            resu = apply_laplace_5p ([siz_test, siz_test], data, .true.)
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test

        deallocate(data, resu)
        siz_test = 2**(ii+10)

        write (OUTPUT_UNIT,*) "Evaluating apply_laplace_5p in 2D on ", siz_test, " elements in ", tot_time, " seconds."
        write (OUTPUT_UNIT,*) "Reference time: ", time_laplace_2 (2**((ii+10)/2), 2**((ii+10)/2))
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo (with Neumann) and amux in 2D..."

    do ii = 0, num_test-1, 2
        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))
        siz_test = 2**((ii+10)/2)

        nnz = stencil2sparse_size ([3, 3], [siz_test, siz_test], &
                [.false., .true., .false., .true., .true., .true., .false., .true., .false.])
        allocate(ir(nnz), jc(nnz), a(nnz))
        allocate(ir2(siz_test**2 + 1), jc2(nnz), a2(nnz))

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            ir = 0
            jc = 0
            a  = 0.0D0
            call cpu_time(start)
            call laplace_5p_sparse_coo ([siz_test, siz_test], ir, jc, a, .true.)
            call coocsr (siz_test**2 + 1, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**2, siz_test**2, nnz, ir2, jc2, a2, data, resu )
            call cpu_time(finish)
            if (maxval(abs( resu - apply_laplace_5p ([siz_test, siz_test], data, .true.))) .gt. 1.0D-8) then
                write(OUTPUT_UNIT,*) "!!!!!!! Error !!!!!!", maxval(abs( resu - apply_laplace_5p ([siz_test, siz_test], data, .true.)))
            end if
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, resu)
        deallocate(ir, jc, a)
        deallocate(ir2, jc2, a2)
        siz_test = 2**(ii+10)

        write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo and amux in 2D on ", siz_test, " elements in ", tot_time, " seconds."
        write (OUTPUT_UNIT,*) "Reference time: ", time_laplace_2 (2**((ii+10)/2), 2**((ii+10)/2))
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating apply_laplace_5p (without Neumann) in 1D..."

    do ii = 0, num_test-1
        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call cpu_time(start)
            resu = apply_laplace_5p ([siz_test], data, .false.)
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, resu)
        write (OUTPUT_UNIT,*) "Evaluating apply_laplace_5p in 1D on ", siz_test, " elements in ", tot_time, " seconds."
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo (without Neumann) and amux in 1D..."

    do ii = 0, num_test-1
        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))

        nnz = stencil2sparse_size ([3], [siz_test], [.true., .true., .true.])
        allocate(ir(nnz), jc(nnz), a(nnz))
        allocate(ir2(siz_test + 1), jc2(nnz), a2(nnz))

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            ir = 0
            jc = 0
            a  = 0.0D0
            call cpu_time(start)
            call laplace_5p_sparse_coo ([siz_test], ir, jc, a, .false.)
            call coocsr (siz_test + 1, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test, siz_test, nnz, ir2, jc2, a2, data, resu )
            call cpu_time(finish)
            if (maxval(abs( resu - apply_laplace_5p ([siz_test], data, .false.))) .gt. 1.0D-8) then
                write(OUTPUT_UNIT,*) "!!!!!!! Error !!!!!!"
            end if
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, resu)
        deallocate(ir, jc, a)
        deallocate(ir2, jc2, a2)
        write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo and amux in 1D on ", siz_test, " elements in ", tot_time, " seconds."
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating apply_laplace_5p (without Neumann) in 2D..."

    do ii = 0, num_test-1, 2

        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))
        siz_test = 2**((ii+10)/2)

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call cpu_time(start)
            resu = apply_laplace_5p ([siz_test, siz_test], data, .false.)
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test

        deallocate(data, resu)
        siz_test = 2**(ii+10)

        write (OUTPUT_UNIT,*) "Evaluating apply_laplace_5p in 2D on ", siz_test, " elements in ", tot_time, " seconds."
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo (without Neumann) and amux in 2D..."

    do ii = 0, num_test-1, 2
        siz_test = 2**(ii+10)
        allocate(data(siz_test), resu(siz_test))
        siz_test = 2**((ii+10)/2)

        nnz = stencil2sparse_size ([3, 3], [siz_test, siz_test], &
                [.false., .true., .false., .true., .true., .true., .false., .true., .false.])
        allocate(ir(nnz), jc(nnz), a(nnz))
        allocate(ir2(siz_test**2 + 1), jc2(nnz), a2(nnz))

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            ir = 0
            jc = 0
            a  = 0.0D0
            call cpu_time(start)
            call laplace_5p_sparse_coo ([siz_test, siz_test], ir, jc, a, .false.)
            call coocsr (siz_test**2 + 1, nnz, ir, jc, a, ir2, jc2, a2)
            call amux ( siz_test**2, siz_test**2, nnz, ir2, jc2, a2, data, resu )
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, resu)
        deallocate(ir, jc, a)
        deallocate(ir2, jc2, a2)
        siz_test = 2**(ii+10)

        write (OUTPUT_UNIT,*) "Evaluating laplace_5p_sparse_coo and amux in 2D on ", siz_test, " elements in ", tot_time, " seconds."
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating apply_inpainting_5p (with Neumann) in 1D..."

    do ii = 0, num_test-1
        siz_test = 2**(ii+10)
        allocate(data(siz_test), data2(siz_test), resu(siz_test))

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call random_number(data2)
            call cpu_time(start)
            resu = apply_inpainting_5p ([siz_test], data, data2, .true.)
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, data2, resu)
        write (OUTPUT_UNIT,*) "Evaluating apply_inpainting_5p in 1D on ", siz_test, " elements in ", tot_time, " seconds."
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating apply_inpainting_5p (with Neumann) in 2D..."

    do ii = 0, num_test-1, 2
        siz_test = 2**(ii+10)
        allocate(data(siz_test), data2(siz_test), resu(siz_test))
        siz_test = 2**((ii+10)/2)

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call random_number(data2)
            call cpu_time(start)
            resu = apply_inpainting_5p ([siz_test, siz_test], data, data2, .true.)
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test

        deallocate(data, data2, resu)
        siz_test = 2**(ii+10)

        write (OUTPUT_UNIT,*) "Evaluating apply_inpainting_5p in 2D on ", siz_test, " elements in ", tot_time, " seconds."
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating apply_inpainting_5p (without Neumann) in 1D..."

    do ii = 0, num_test-1
        siz_test = 2**(ii+10)
        allocate(data(siz_test), data2(siz_test), resu(siz_test))

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call random_number(data2)
            call cpu_time(start)
            resu = apply_inpainting_5p ([siz_test], data, data2, .true.)
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test
        deallocate(data, data2, resu)
        write (OUTPUT_UNIT,*) "Evaluating apply_inpainting_5p in 1D on ", siz_test, " elements in ", tot_time, " seconds."
    end do

    ! -------------------------------------------------------------------- !

    write (OUTPUT_UNIT,*) "Evaluating apply_inpainting_5p (without Neumann) in 2D..."

    do ii = 0, num_test-1, 2
        siz_test = 2**(ii+10)
        allocate(data(siz_test), data2(siz_test), resu(siz_test))
        siz_test = 2**((ii+10)/2)

        tot_time = 0.0
        do jj = 1, rep_test
            call random_number(data)
            call random_number(data2)
            call cpu_time(start)
            resu = apply_inpainting_5p ([siz_test, siz_test], data, data2, .false.)
            call cpu_time(finish)
            tot_time = tot_time + (finish - start)
        end do
        tot_time = tot_time / rep_test

        deallocate(data, data2, resu)
        siz_test = 2**(ii+10)

        write (OUTPUT_UNIT,*) "Evaluating apply_inpainting_5p in 2D on ", siz_test, " elements in ", tot_time, " seconds."
    end do

    ! -------------------------------------------------------------------- !

end program benchmark
