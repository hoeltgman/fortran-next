! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver_imgfun
        use :: fruit
        use :: test_img_fun
        implicit none

        integer :: fail_counter

        call init_fruit

        call setup_test_img_fun
        write (*,*) ".. running test: check_mse_error"
        call set_unit_name('check_mse_error')
        call run_test_case(check_mse_error, "check_mse_error")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_img_fun

        call setup_test_img_fun
        write (*,*) ".. running test: check_psnr_error"
        call set_unit_name('check_psnr_error')
        call run_test_case(check_psnr_error, "check_psnr_error")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_img_fun

        call setup_test_img_fun
        write (*,*) ".. running test: check_get_image_dimensions"
        call set_unit_name('check_get_image_dimensions')
        call run_test_case(check_get_image_dimensions, "check_get_image_dimensions")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_img_fun

        call setup_test_img_fun
        write (*,*) ".. running test: check_get_image_pbm"
        call set_unit_name('check_get_image_pbm')
        call run_test_case(check_get_image_pbm, "check_get_image_pbm")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_img_fun

        call setup_test_img_fun
        write (*,*) ".. running test: check_get_image_pgm"
        call set_unit_name('check_get_image_pgm')
        call run_test_case(check_get_image_pgm, "check_get_image_pgm")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_img_fun

        call setup_test_img_fun
        write (*,*) ".. running test: check_get_image_ppm"
        call set_unit_name('check_get_image_ppm')
        call run_test_case(check_get_image_ppm, "check_get_image_ppm")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_img_fun

        call setup_test_img_fun
        write (*,*) ".. running test: check_write_image"
        call set_unit_name('check_write_image')
        call run_test_case(check_write_image, "check_write_image")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_img_fun

        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver_imgfun
