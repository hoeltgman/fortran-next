! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver
        use :: fruit
        use :: test_sparse
        implicit none

        integer :: fail_counter

        call init_fruit

        ! call setup_test_sparse
        ! write(*,*) ".. running test: check_coocsr_ops"
        ! call set_unit_name('check_coocsr_ops')
        ! call run_test_case(check_coocsr_ops, "check_coocsr_ops")
        ! write(*,*) ""
        ! write(*,*) ".. done."
        ! write(*,*) ""
        ! call teardown_test_sparse

        call setup_test_sparse
        write(*,*) ".. running test: check_fullnnz"
        call set_unit_name('check_fullnnz')
        call run_test_case(check_fullnnz, "check_fullnnz")
        write(*,*) ""
        write(*,*) ".. done."
        write(*,*) ""
        call teardown_test_sparse

        ! call setup_test_sparse
        ! write(*,*) ".. running test: check_full2coo"
        ! call set_unit_name('check_full2coo')
        ! call run_test_case(check_full2coo, "check_full2coo")
        ! write(*,*) ""
        ! write(*,*) ".. done."
        ! write(*,*) ""
        ! call teardown_test_sparse

        ! call setup_test_sparse
        ! write(*,*) ".. running test: check_full2coo"
        ! call set_unit_name('check_full2coo')
        ! call run_test_case(check_full2coo, "check_full2coo")
        ! write(*,*) ""
        ! write(*,*) ".. done."
        ! write(*,*) ""
        ! call teardown_test_sparse

        call setup_test_sparse
        write(*,*) ".. running test: check_amux"
        call set_unit_name('check_amux')
        call run_test_case(check_amux, "check_amux")
        write(*,*) ""
        write(*,*) ".. done."
        write(*,*) ""
        call teardown_test_sparse

        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver
