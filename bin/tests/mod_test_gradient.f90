! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

module test_gradient
    use :: fruit
    use :: gradient
    use :: iso_fortran_env
    implicit none
    public

contains

    ! setup_before_all
    ! setup = setup_before_each
    subroutine setup_test_gradient
    end subroutine setup_test_gradient

    ! teardown_before_all
    ! teardown = teardown_before_each
    subroutine teardown_test_gradient
    end subroutine teardown_test_gradient

    subroutine check_gradient_fd_stencil
        real(REAL64), dimension(1, 3) :: s1d
        real(REAL64), dimension(2, 9) :: s2d
        real(REAL64), dimension(3,27) :: s3d

        s1d = gradient_fd_stencil([1])
        call assertEquals(real([0, -1, 1], REAL64), s1d(1,:), 3)

        s1d = gradient_fd_stencil([2])
        call assertEquals(real([0, -1, 1], REAL64), s1d(1,:), 3)

        s1d = gradient_fd_stencil([3])
        call assertEquals(real([0, -1, 1], REAL64), s1d(1,:), 3)

        s1d = gradient_fd_stencil([4])
        call assertEquals(real([0, -1, 1], REAL64), s1d(1,:), 3)

        s1d = gradient_fd_stencil([5])
        call assertEquals(real([0, -1, 1], REAL64), s1d(1,:), 3)

        s2d = gradient_fd_stencil([1, 1])
        call assertEquals(real([0, 0, 0, 0, -1, 1, 0, 0, 0], REAL64), s2d(1,:), 9, 1.0D-10)
        call assertEquals(real([0, 0, 0, 0, -1, 0, 0, 1, 0], REAL64), s2d(2,:), 9, 1.0D-10)

        s2d = gradient_fd_stencil([2, 2])
        call assertEquals(real([0, 0, 0, 0, -1, 1, 0, 0, 0], REAL64), s2d(1,:), 9, 1.0D-10)
        call assertEquals(real([0, 0, 0, 0, -1, 0, 0, 1, 0], REAL64), s2d(2,:), 9, 1.0D-10)

        s2d = gradient_fd_stencil([3, 3])
        call assertEquals(real([0, 0, 0, 0, -1, 1, 0, 0, 0], REAL64), s2d(1,:), 9, 1.0D-10)
        call assertEquals(real([0, 0, 0, 0, -1, 0, 0, 1, 0], REAL64), s2d(2,:), 9, 1.0D-10)

        s2d = gradient_fd_stencil([4, 4])
        call assertEquals(real([0, 0, 0, 0, -1, 1, 0, 0, 0], REAL64), s2d(1,:), 9, 1.0D-10)
        call assertEquals(real([0, 0, 0, 0, -1, 0, 0, 1, 0], REAL64), s2d(2,:), 9, 1.0D-10)

        s2d = gradient_fd_stencil([4, 3])
        call assertEquals(real([0, 0, 0, 0, -1, 1, 0, 0, 0], REAL64), s2d(1,:), 9, 1.0D-10)
        call assertEquals(real([0, 0, 0, 0, -1, 0, 0, 1, 0], REAL64), s2d(2,:), 9, 1.0D-10)

        s2d = gradient_fd_stencil([3, 4])
        call assertEquals(real([0, 0, 0, 0, -1, 1, 0, 0, 0], REAL64), s2d(1,:), 9, 1.0D-10)
        call assertEquals(real([0, 0, 0, 0, -1, 0, 0, 1, 0], REAL64), s2d(2,:), 9, 1.0D-10)

        s2d = gradient_fd_stencil([5, 5])
        call assertEquals(real([0, 0, 0, 0, -1, 1, 0, 0, 0], REAL64), s2d(1,:), 9, 1.0D-10)
        call assertEquals(real([0, 0, 0, 0, -1, 0, 0, 1, 0], REAL64), s2d(2,:), 9, 1.0D-10)

        s3d = gradient_fd_stencil([5, 5, 5])
        call assertEquals(real([ &
                0, 0, 0, 0,  0, 0, 0, 0, 0, &
                0, 0, 0, 0, -1, 1, 0, 0, 0, &
                0, 0, 0, 0,  0, 0, 0, 0, 0], REAL64), s3d(1,:), 27, 1.0D-10)
        call assertEquals(real([ &
                0, 0, 0, 0,  0, 0, 0, 0, 0, &
                0, 0, 0, 0, -1, 0, 0, 1, 0, &
                0, 0, 0, 0,  0, 0, 0, 0, 0], REAL64), s3d(2,:), 27, 1.0D-10)
        call assertEquals(real([ &
                0, 0, 0, 0,  0, 0, 0, 0, 0, &
                0, 0, 0, 0, -1, 0, 0, 0, 0, &
                0, 0, 0, 0,  1, 0, 0, 0, 0], REAL64), s3d(3,:), 27, 1.0D-10)
    end subroutine check_gradient_fd_stencil

    subroutine check_gradient_fd_sparse_coo_size

        call assertEquals(1, gradient_fd_sparse_coo_size ([1], .true.))
        call assertEquals(1, gradient_fd_sparse_coo_size ([1], .false.))

        call assertEquals(3, gradient_fd_sparse_coo_size ([2], .true.))
        call assertEquals(3, gradient_fd_sparse_coo_size ([2], .false.))

        call assertEquals(9, gradient_fd_sparse_coo_size ([5], .true.))
        call assertEquals(9, gradient_fd_sparse_coo_size ([5], .false.))

        call assertEquals(19, gradient_fd_sparse_coo_size ([10], .true.))
        call assertEquals(19, gradient_fd_sparse_coo_size ([10], .false.))

        call assertEquals(2, gradient_fd_sparse_coo_size ([1, 1], .true.))
        call assertEquals(2, gradient_fd_sparse_coo_size ([1, 1], .false.))

        call assertEquals(41, gradient_fd_sparse_coo_size ([3, 4], .true.))
        call assertEquals(41, gradient_fd_sparse_coo_size ([3, 4], .false.))

        call assertEquals(41, gradient_fd_sparse_coo_size ([4, 3], .true.))
        call assertEquals(41, gradient_fd_sparse_coo_size ([4, 3], .false.))
    end subroutine check_gradient_fd_sparse_coo_size

    subroutine check_gradient_fd_sparse_coo

        integer(INT32), dimension(9) :: ir, jc
        real(REAL64), dimension(9) :: a

        integer(INT32), dimension(41) :: ir2, jc2
        real(REAL64), dimension(41) :: a2

        call gradient_fd_sparse_coo([5_INT32], ir, jc, a, .true.)

        call assertEquals(int([1, 1, 2, 2, 3, 3, 4, 4, 5], INT32), ir, 9)
        call assertEquals(int([1, 2, 2, 3, 3, 4, 4, 5, 5], INT32), jc, 9)
        call assertEquals(real([-1, 1, -1, 1, -1, 1, -1, 1, 0], REAL64), a, 9)

        call gradient_fd_sparse_coo([5_INT32], ir, jc, a, .false.)

        call assertEquals(int([1, 1, 2, 2, 3, 3, 4, 4, 5], INT32), ir, 9)
        call assertEquals(int([1, 2, 2, 3, 3, 4, 4, 5, 5], INT32), jc, 9)
        call assertEquals(real([-1, 1, -1, 1, -1, 1, -1, 1, -1], REAL64), a, 9)

        call gradient_fd_sparse_coo([3_INT32, 4_INT32], ir2, jc2, a2, .true.)

        call gradient_fd_sparse_coo([3_INT32, 4_INT32], ir2, jc2, a2, .false.)

        call gradient_fd_sparse_coo([4_INT32, 3_INT32], ir2, jc2, a2, .true.)
        call gradient_fd_sparse_coo([4_INT32, 3_INT32], ir2, jc2, a2, .false.)

    end subroutine check_gradient_fd_sparse_coo

    subroutine check_apply_gradient_fd

        call assertEquals(real([1, 1, 1, 1, -5], REAL64), apply_gradient_fd([5], real([1, 2, 3, 4, 5], REAL64), .false.), 5)
        call assertEquals(real([1, 1, 1, 1,  0], REAL64), apply_gradient_fd([5], real([1, 2, 3, 4, 5], REAL64), .true.), 5)

        call assertEquals(real([&
                -1, 0, 0, 0, 0, 0, 0, 1, -1, &
                -1, 0, 0, 0, 0, 1, 0, 0, -1], REAL64), &
                apply_gradient_fd([3, 3], real([1, 0, 0, 0, 0, 0, 0, 0, 1], REAL64), .false.), 18)

        call assertEquals(real([&
                -1, 0, 0, 0, 0, 0, 0, 1, 0, &
                -1, 0, 0, 0, 0, 1, 0, 0, 0], REAL64), &
                apply_gradient_fd([3, 3], real([1, 0, 0, 0, 0, 0, 0, 0, 1], REAL64), .true.), 18)

        call assertEquals(real([&
                0, 1, -1, 0, 0, 0, -1, 0, 0, &
                0, 0, -1, 1, 0, 0, -1, 0, 0], REAL64), &
                apply_gradient_fd([3, 3], real([0, 0, 1, 0, 0, 0, 1, 0, 0], REAL64), .false.), 18)

        call assertEquals(real([&
                0, 1, 0, 0, 0, 0, -1, 0, 0, &
                0, 0, -1, 1, 0, 0, 0, 0, 0], REAL64), &
                apply_gradient_fd([3, 3], real([0, 0, 1, 0, 0, 0, 1, 0, 0], REAL64), .true.), 18)

        call assertEquals(real([&
                0, 0, 0, 1, -1, 0, 0, 0, 0, &
                0, 1, 0, 0, -1, 0, 0, 0, 0], REAL64), &
                apply_gradient_fd([3, 3], real([0, 0, 0, 0, 1, 0, 0, 0, 0], REAL64), .false.), 18)

        call assertEquals(real([&
                0, 0, 0, 1, -1, 0, 0, 0, 0, &
                0, 1, 0, 0, -1, 0, 0, 0, 0], REAL64), &
                apply_gradient_fd([3, 3], real([0, 0, 0, 0, 1, 0, 0, 0, 0], REAL64), .true.), 18)

    end subroutine check_apply_gradient_fd
end module test_gradient
