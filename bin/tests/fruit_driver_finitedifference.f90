! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver
        use :: fruit
        use :: test_finitedifference
        implicit none

        integer :: fail_counter

        call init_fruit

        call setup_test_finitedifference
        write (*,*) ".. running test: check_fd_filter_1D"
        call set_unit_name('check_fd_filter_1D')
        call run_test_case(check_fd_filter_1D, "check_fd_filter_1D")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_finitedifference

        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver
