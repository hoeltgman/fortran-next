! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.
!

program fruit_driver
        use :: fruit
        use :: test_inpainting
        implicit none

        integer :: fail_counter

        call init_fruit

        call setup_test_inpainting
        write (*,*) ".. running test: check_apply_inpainting_5p"
        call set_unit_name('check_apply_inpainting_5p')
        call run_test_case(check_apply_inpainting_5p, "check_apply_inpainting_5p")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_inpainting

        call setup_test_inpainting
        write (*,*) ".. running test: check_apply_inpainting_T_5p"
        call set_unit_name('check_apply_inpainting_T_5p')
        call run_test_case(check_apply_inpainting_T_5p, "check_apply_inpainting_T_5p")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_inpainting

        call setup_test_inpainting
        write (*,*) ".. running test: check_norm_lin_pde_op"
        call set_unit_name('check_norm_lin_pde_op')
        call run_test_case(check_norm_lin_pde_op, "check_norm_lin_pde_op")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_inpainting

        call setup_test_inpainting
        write (*,*) ".. running test: check_inpainting_5p_sparse_coo"
        call set_unit_name('check_inpainting_5p_sparse_coo')
        call run_test_case(check_inpainting_5p_sparse_coo, "check_inpainting_5p_sparse_coo")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_inpainting

        call setup_test_inpainting
        write (*,*) ".. running test: check_solve_inpainting"
        call set_unit_name('check_solve_inpainting')
        call run_test_case(check_solve_inpainting, "check_solve_inpainting")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_inpainting

        call setup_test_inpainting
        write (*,*) ".. running test: check_solve_inpainting_2d"
        call set_unit_name('check_solve_inpainting_2d')
        call run_test_case(check_solve_inpainting_2d, "check_solve_inpainting_2d")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_inpainting

        call setup_test_inpainting
        write (*,*) ".. running test: check_coo_amux_apply_inpainting"
        call set_unit_name('check_coo_amux_apply_inpainting')
        call run_test_case(check_coo_amux_apply_inpainting, "check_coo_amux_apply_inpainting")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_inpainting

        call setup_test_inpainting
        write (*,*) ".. running test: check_coo_amux_apply_inpainting_T"
        call set_unit_name('check_coo_amux_apply_inpainting_T')
        call run_test_case(check_coo_amux_apply_inpainting_T, "check_coo_amux_apply_inpainting_T")
        write (*,*)
        write (*,*) ".. done."
        write (*,*) ""
        call teardown_test_inpainting


        call fruit_summary
        call fruit_finalize

        call get_failed_count(fail_counter)
        if (fail_counter > 0) then
                error stop fail_counter
        end if

end program fruit_driver
