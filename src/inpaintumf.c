// Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdio.h>
#include <umfpack.h>

#include <inpaintumf.h>

/*
 * alloc ==  2: routine will only be called a single time, perform everything (alloc, computations and free).
 * alloc ==  1: perform allocation and symbolic decomposition (alloc, computations, no free).
 * alloc ==  0: just update Ai, Ap, Ax (no alloc, computations, no free).
 * alloc == -1: free memory (no alloc, no computations, free).
 * int solve_inpainting_coo(long *n, long *nz, long *ir, long *jc, double *a, double *rhs, double *x, int alloc)
 */
void solve_inpainting_coo(long *n, long *nz, long *ir, long *jc, double *a, double *rhs, double *x, long *alloc)
{
  double Info[UMFPACK_INFO];
  double Control[UMFPACK_CONTROL];
  SuiteSparse_long status;
  static void *Symbolic;
  void *Numeric;
  static long   *Ap, *Ai;
  static double *Ax;

  /* Allocation must be done at first invocation (alloc == 1), bu we keep them in memory for the later calls. */
  if ((*alloc == 1)||(*alloc == 2))
  {
    Ap = calloc((*n)+1, sizeof(SuiteSparse_long));
    if (!Ap)
	  {
	    return;
	  }
    Ai = calloc(*nz, sizeof(SuiteSparse_long));
    if (!Ai)
	  {
	    free(Ap);
	    return;
	  }
    Ax = calloc(*nz, sizeof(double));
    if (!Ax)
	  {
	    free(Ap);
	    free(Ai);
	    return;
	  }
  }

  if (*alloc >= 0)
  {
    /* Set default parameters. */
    umfpack_dl_defaults(Control);

    /* Convert coo to csc coordinates. */
    status = umfpack_dl_triplet_to_col ((SuiteSparse_long) *n,  // n_row
                                        (SuiteSparse_long) *n,  // n_col
                                        (SuiteSparse_long) *nz, // nz
                                        ir, jc, a,              // Ti[nz], Tj[nz], Tx[nz]
                                        Ap, Ai, Ax,             // Ap[n_col+1], Ai[nz], Ax[nz]
                                        (long *) NULL);         // Map
    switch (status)
    {
      case UMFPACK_ERROR_argument_missing:
        printf("UMFPACK Error: conversion to triplet_to_col failed due to missing argument.\n");
        return;
      case UMFPACK_ERROR_n_nonpositive:
        printf("UMFPACK Error: conversion to triplet_to_col failed due to non-positive n.\n");
        return;
      case UMFPACK_ERROR_invalid_matrix:
        printf("UMFPACK Error: conversion to triplet_to_col failed due to bad matrix structure.\n");
        return;
      case UMFPACK_ERROR_out_of_memory:
        printf("UMFPACK Error: conversion to triplet_to_col failed due to insufficient memory.\n");
        return;
      case UMFPACK_OK:
        break;
      default:
        break;
    }

    if (*alloc >= 1)
	  {
	    status = umfpack_dl_symbolic ((SuiteSparse_long) *n, // n_row
                                    (SuiteSparse_long) *n, // n_col
                                    Ap, Ai, Ax,            // Ap[n_col+1], Ai[nz], Ax[nz]
                                    &Symbolic,
                                    Control,
                                    Info);
      if (Symbolic == NULL)
      {
        printf("UMFPACK Error: Symbolic is NULL.\n");
        return;
      }
      switch (status)
      {
        case UMFPACK_ERROR_argument_missing:
          printf("UMFPACK Error: symbolic failed due to missing argument.\n");
          return;
        case UMFPACK_ERROR_n_nonpositive:
          printf("UMFPACK Error: symbolic failed due to non-positive n.\n");
          return;
        case UMFPACK_ERROR_invalid_matrix:
          printf("UMFPACK Error: symbolic failed due to bad matrix structure.\n");
          return;
        case UMFPACK_ERROR_out_of_memory:
          printf("UMFPACK Error: symbolic failed due to insufficient memory.\n");
          return;
        case UMFPACK_ERROR_internal_error:
          printf("UMFPACK Error: symbolic failed due to internal error.\n");
          return;
        case UMFPACK_OK:
          break;
        default:
          break;
      }
	  }

    status = umfpack_dl_numeric (Ap, Ai, Ax, // Ap[n_col+1], Ai[nz], Ax[nz]
                                 Symbolic,
                                 &Numeric,
                                 Control,
                                 Info);
    switch (status)
    {
      case UMFPACK_ERROR_argument_missing:
        printf("UMFPACK Error: numeric failed due to missing argument.\n");
        return;
      case UMFPACK_WARNING_singular_matrix:
        printf("UMFPACK Error: numeric failed due singular matrix.\n");
        return;
      case UMFPACK_ERROR_out_of_memory:
        printf("UMFPACK Error: numeric failed due to insufficient memory.\n");
        return;
      case UMFPACK_ERROR_invalid_Symbolic_object:
        printf("UMFPACK Error: numeric failed due to invalid symbolic object.\n");
        return;
      case UMFPACK_ERROR_different_pattern:
        printf("UMFPACK Error: numeric failed changed pattern in Ap/Ai.\n");
        return;
      case UMFPACK_OK:
        break;
      default:
        break;
    }

    status = umfpack_dl_solve (UMFPACK_A,  // sys
                               Ap, Ai, Ax, // Ap[n], Ai[nz], Ax[nz]
                               x,          // Xx[n] n = n_row = n_col
                               rhs,        // Bx[n]
                               Numeric,
                               Control,
                               Info);
    switch (status)
    {
      case UMFPACK_ERROR_argument_missing:
        printf("UMFPACK Error: solve failed due to missing argument.\n");
        return;
      case UMFPACK_WARNING_singular_matrix:
        printf("UMFPACK Error: solve failed due singular matrix.\n");
        return;
      case UMFPACK_ERROR_out_of_memory:
        printf("UMFPACK Error: solve failed due to insufficient memory.\n");
        return;
      case UMFPACK_ERROR_invalid_system:
        printf("UMFPACK Error: solve failed due to invalid system.\n");
        return;
      case UMFPACK_ERROR_invalid_Numeric_object:
        printf("UMFPACK Error: solve failed due to invalid numeric object.\n");
        return;
      case UMFPACK_OK:
        break;
      default:
        break;
    }

    umfpack_dl_free_numeric(&Numeric);
  }

  if ((*alloc == -1)||(*alloc == 2 ))
  {
    free (Ap);
    free (Ai);
    free (Ax);
    umfpack_dl_free_symbolic (&Symbolic);
  }

  return;
}
