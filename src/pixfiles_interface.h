// Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef PIXFILES_INTERFACE_H
#define PIXFILES_INTERFACE_H

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

  long pixfiles_read_image (
			    long       *nx,
			    long       *ny,
			    long       *nd,
			    double     *c1,
			    double     *c2,
			    double     *c3,
			    const char *filename
			    );

  long pixfiles_write_image (
			     long       *nx,
			     long       *ny,
			     long       *nd,
			     double     *c1,
			     double     *c2,
			     double     *c3,
			     const char *filename
			     );

  long pixfiles_read_size (
			   long       *nx,
			   long       *ny,
			   long       *nd,
			   const char *filename
			   );

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif /* PIXFILES_INTERFACE_H */
