! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.

module blas_interface
        use :: iso_fortran_env
        implicit none

        interface nrm2
            real function snrm2 (n, x, incx)
                integer            :: n
                real, dimension(*) :: x
                integer            :: incx
            end function snrm2

            double precision function dnrm2 (n, x, incx)
                integer                        :: n
                double precision, dimension(*) :: x
                integer                        :: incx
            end function dnrm2
        end interface nrm2

        interface dot
            real function sdot (n, x, incx, y, incy)
                integer              :: n
                real,   dimension(*) :: x
                integer              :: incx
                real,   dimension(*) :: y
                integer              :: incy
            end function sdot

            double precision function ddot (n, x, incx, y, incy)
                integer                        :: n
                double precision, dimension(*) :: x
                integer                        :: incx
                double precision, dimension(*) :: y
                integer                        :: incy
            end function ddot
        end interface dot

        interface copy
            subroutine dcopy (n, x, incx, y, incy)
                integer, intent(in)                         :: n
                double precision, dimension(*), intent(in)  :: x
                integer, intent(in)                         :: incx
                double precision, dimension(*), intent(out) :: y
                integer, intent(in)                         :: incy
            end subroutine dcopy

            subroutine scopy (n, x, incx, y, incy)
                integer, intent(in)                         :: n
                real,    dimension(*), intent(in)           :: x
                integer, intent(in)                         :: incx
                real,    dimension(*), intent(out)          :: y
                integer, intent(in)                         :: incy
            end subroutine scopy
        end interface copy

        interface axpy
            subroutine saxpy (n, alpha, x, incx, y, incy)
                integer, intent(in)                         :: n
                real,    intent(in)                         :: alpha
                real,    dimension(*), intent(in)           :: x
                integer, intent(in)                         :: incx
                real,    dimension(*), intent(inout)        :: y
                integer, intent(in)                         :: incy
            end subroutine saxpy

            subroutine daxpy (n, alpha, x, incx, y, incy)
                integer, intent(in)                           :: n
                double precision, intent(in)                  :: alpha
                double precision, dimension(*), intent(in)    :: x
                integer, intent(in)                           :: incx
                double precision, dimension(*), intent(inout) :: y
                integer, intent(in)                           :: incy
            end subroutine daxpy
        end interface axpy

        interface scal
            subroutine sscal (n, alpha, x, incx)
                integer, intent(in)                         :: n
                real,    intent(in)                         :: alpha
                real,    dimension(*), intent(inout)        :: x
                integer, intent(in)                         :: incx
            end subroutine sscal

            subroutine dscal (n, alpha, x, incx)
                integer, intent(in)                           :: n
                double precision, intent(in)                  :: alpha
                double precision, dimension(*), intent(inout) :: x
                integer, intent(in)                           :: incx
            end subroutine dscal
        end interface scal
    contains
end module blas_interface
