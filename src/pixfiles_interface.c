// Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

#include <pixfiles.h>

#include "pixfiles_interface.h"

typedef union {
  struct {
    unsigned int b1 : 1;
    unsigned int b2 : 1;
    unsigned int b3 : 1;
    unsigned int b4 : 1;
    unsigned int b5 : 1;
    unsigned int b6 : 1;
    unsigned int b7 : 1;
    unsigned int b8 : 1;
  } bits;
  uint8_t byte;
} byte_t;

long pixfiles_read_size (
			 long       *nr,
			 long       *nc,
			 long       *nd,
			 const char *filename)
{
  PIXF_Info *im_info;
  int err;

  im_info = calloc(1, sizeof(PIXF_Info));
  if(im_info == NULL) {
    return -1;
  }

  err = pixf_open(filename, im_info);
  if(err != PIXF_ERR_OK) {
    return -1;
  }

  (*nc) = im_info->width;
  (*nr) = im_info->height;
  (*nd) = im_info->bpp/im_info->bps;

  fclose(im_info->f);
  free(im_info);

  return 0;
}

/**
 * Reads an image file.
 * @param filename The filename of the image.
 * @warning At the moment only pgm and ppm files are supported, although
 * the pixfiles library also supports pbm files. See libpixfiles library.
 */
long pixfiles_read_image (
			  long       *nr,
			  long       *nc,
			  long       *nd,
			  double     *c1,
			  double     *c2,
			  double     *c3,
			  const char *filename)
{
  PIXF_Info *im_info;
  unsigned char *data; // if maxval is > 255, then this is not enough
  int err;
  long i, k;
  byte_t buffer;

  im_info = calloc(1, sizeof(PIXF_Info));
  if(im_info == NULL) {
    return -1;
  }

  err = pixf_open(filename, im_info);
  if(err != PIXF_ERR_OK) {
    return -1;
  }

  err = pixf_read(im_info, (void *) &data);
  if(err != PIXF_ERR_OK) {
    return -1;
  }

  (*nc) = im_info->width;
  (*nr) = im_info->height;
  (*nd) = im_info->bpp/im_info->bps;

  /* For the moment there's no working support for pbm files. */
  /* http://netpbm.sourceforge.net/doc/pbm.html: 0 is white and 1 is black! */
  /* This code fails for ascii pbms. I believe the bug lies in pixfiles.c as a read/write with pixfiles yields different */
  /* images. */
  if(im_info->bps == 1) {
    k = 0;
    for (i=0; i< (long) pixf_get_size(im_info, NULL); i++) {
      buffer.byte = data[i]; // Read next byte from stream and jump to next byte if we exceed row.
      c1[k] = (double) buffer.bits.b8;
      k++;
      if (k > 0 && k % (*nc) == 0) {
	continue;
      }
      c1[k] = (double) buffer.bits.b7;
      k++;
      if (k > 0 && k % (*nc) == 0) {
	continue;
      }
      c1[k] = (double) buffer.bits.b6;
      k++;
      if (k > 0 && k % (*nc) == 0) {
	continue;
      }
      c1[k] = (double) buffer.bits.b5;
      k++;
      if (k > 0 && k % (*nc) == 0) {
	continue;
      }
      c1[k] = (double) buffer.bits.b4;
      k++;
      if (k > 0 && k % (*nc) == 0) {
	continue;
      }
      c1[k] = (double) buffer.bits.b3;
      k++;
      if (k > 0 && k % (*nc) == 0) {
	continue;
      }
      c1[k] = (double) buffer.bits.b2;
      k++;
      if (k > 0 && k % (*nc) == 0) {
	continue;
      }
      c1[k] = (double) buffer.bits.b1;
      k++;
      if (k > 0 && k % (*nc) == 0) {
	continue;
      }
    }
  } else if(im_info->bps == im_info->bpp) {
    // Single channel
    for (i=0; i<(*nr)*(*nc); i++) {
      if (im_info->maxval <= 255) {
	c1[i] = ((double) data[i])/((double) im_info->maxval);
      } else {
	// We need to read 2 bytes at once when the maximal grey value exceeds 255.
	c1[i] = ((double) (data[2*i] | data[2*i+1] << 8))/((double) im_info->maxval);
      }
    }
  } else {
    // Multi channel
    for (i=0; i<(*nr)*(*nc); i++) {
      if (im_info->maxval <= 255) {
	c1[i] = ((double) data[3*i+0])/((double) im_info->maxval);
	c2[i] = ((double) data[3*i+1])/((double) im_info->maxval);
	c3[i] = ((double) data[3*i+2])/((double) im_info->maxval);
      } else {
	// We need to read 2 bytes at once when the maximal grey value exceeds 255.
	c1[i] = ((double) (data[6*i+0] | data[6*i+1] << 8))/((double) im_info->maxval);
	c2[i] = ((double) (data[6*i+2] | data[6*i+3] << 8))/((double) im_info->maxval);
	c3[i] = ((double) (data[6*i+4] | data[6*i+5] << 8))/((double) im_info->maxval);
      }
    }
  }

  fclose(im_info->f);
  free(im_info);
  free(data);

  return 0;
}

/**
 * Writes an image file.
 * @param im The image structure where the data is being stored.
 * @param filename The filename of the image.
 * @return Error code according to #improc_return.
 * @warning At the moment only pgm and ppm files are supported, although
 * the pixfiles library also supports pbm files. See pixfiles.c and pixfiles.h. 16bit writing is not supported too.
 */
long pixfiles_write_image (long *nr, long *nc, long *nd, double *c1, double *c2, double *c3, const char *filename)
{
  PIXF_Info *im_info;
  unsigned char *data;
  long i;
  int err;

  im_info = calloc(1, sizeof(PIXF_Info));
  im_info->width  = (size_t) *nc;
  im_info->height = (size_t) *nr;
  im_info->maxval = (size_t) 255;
  if( *nd == 1) {
    im_info->bps    = 8;
    im_info->bpp    = 8;
  }
  if( *nd == 3) {
    im_info->bps    = 8;
    im_info->bpp    = 24;
  }
  im_info->ascii  = 0;

  data = calloc( (*nr)*(*nc)*(*nd), sizeof(unsigned char) );
  if( data == NULL ) {
    return -1;
  }

  if( *nd == 1) {
    for(i=0; i<(*nr)*(*nc); i++) {
      if( c1[i] < 0 ) {
	data[i] = 0;
      } else if( c1[i] > 1.0 ) {
	data[i] = 255;
      } else {
	data[i] = (unsigned char) 255 * c1[i];
      }
    }
  }
  if( *nd == 3) {
    for(i=0; i<(*nr)*(*nc); i++) {
      if( c1[i] < 0 ) {
	data[(*nd)*i] = 0;
      } else if( c1[i] > 1.0 ) {
	data[(*nd)*i] = 255;
      } else {
	data[(*nd)*i] = (unsigned char) 255 * c1[i];
      }

      if( c2[i] < 0 ) {
	data[(*nd)*i+1] = 0;
      } else if( c2[i] > 1.0 ) {
	data[(*nd)*i+1] = 255;
      } else {
	data[(*nd)*i+1] = (unsigned char) 255 * c2[i];
      }

      if( c3[i] < 0 ) {
	data[(*nd)*i+2] = 0;
      } else if( c3[i] > 1.0 ) {
	data[(*nd)*i+2] = 255;
      } else {
	data[(*nd)*i+2] = (unsigned char) 255 * c3[i];
      }
    }
  }

  err = pixf_write(filename, im_info, data);
  if( err != PIXF_ERR_OK ) {
    return -1;
  }

  fclose(im_info->f);
  free(data);
  free(im_info);

  return 0;
}
