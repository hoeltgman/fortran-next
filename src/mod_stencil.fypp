! Copyright (C) 2015-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with
! this program. If not, see <http://www.gnu.org/licenses/>.

#:set ikinds = [ 'INT32',  'INT64' ]
#:set rkinds = [ 'REAL32', 'REAL64' ]
module stencil
    !! Routines related to stencil operations and their (sparse) matrix representations. The routines provided in this module
    !! are agnostic towards the dimension of the underlying problem.
    use :: iso_fortran_env, only : INT32, INT64, REAL32, REAL64
    implicit none
    private

    public :: stencillocs
    interface stencillocs
        !! Returns an array containing the linearly labeled relative positions in a stencil of size siz on a grid of size dims.
        !!
        !! Assume we have an 2D grid of size 10 x 12 and a stencil of size 3 x 5. For any position ii on the grid, we
        !! get the corresponding linear and relative offsets of grid points under the stencil by computing
        !!     stencillocs([3, 5], [10, 12])
        !! which returns [-11, -10, -9, -6, -5, -4, -1, 0, 1, 4, 5, 6, 9, 10, 11]. The absolute positions are then obtained by
        !! computing
        !!     ii + stencillocs([3, 5], [10, 12])
        !! the positions are sorted in increasing order. No error checking is done to verify that the obtained positions are
        !! inside the grid.
        !! # NOTES #
        !! The size siz of the stencil may only contain odd integer entries whereas the entries in dims can be arbitrary
        !! integers.
        !! The algorithm works by splitting the result recursively for each considered dimension and finally applying the
        !! trivial 1D case.
#:for itype in ikinds
        module procedure stencillocs_${itype}$
#:endfor
    end interface stencillocs

    public :: stencilmask
    interface stencilmask
        !! Indicates out-of-bounds references for a given stencil on a specified position in an arbitrary grid. Useful in
        !! combination with [[stencillocs(interface)]].
        !!
        !! EXAMPLE
        !!
        !! Assume we have an 2D grid of size 10 x 12 and a stencil of size 3 x 5. For the position ii=2 on the grid, we
        !! get the corresponding mask by computing
        !!
        !! stencilmask([3, 5], [10, 12], 2)
        !!
        !! which returns [f, f, f, f, f, f, t, t, t, t, t, t, t, t, t].
#:for itype in ikinds
        module procedure stencilmask_${itype}$
#:endfor
    end interface stencilmask

    public :: create_5p_stencil
    interface create_5p_stencil
        !! Create a "5-point" stencil for arbitrary dimensions. Does not check for bounds!
        !! Also see [[stencillocs(interface)]] and [[stencilmask(interface)]]
        !!
        !! EXAMPLE
        !!
        !! create_5p_stencil(1) yields [t, t, t]
        !! create_5p_stencil(2) yields [f, t, f, t, t, t, f, t, f]
        !!
        !! NOTES
        !!
        !! No out-of bounds check is done.
#:for itype in ikinds
        module procedure create_5p_stencil_${itype}$
#:endfor
    end interface create_5p_stencil

    public :: create_xp_stencil
    interface create_xp_stencil
        !! Create a cross stencil for arbitrary dimensions. Does not check for bounds+
        !! Also see [[stencillocs(interface)]] and [[stencilmask(interface)]!
        !!
        !! EXAMPLE
        !!
        !! create_xp_stencil(1) yields [t, t, t]
        !! create_xp_stencil(2) yields [f, t, f, t, t, t, f, t, f]
        !!
        !! NOTES
        !!
        !! No out-of bounds check is done.
#:for itype in ikinds
        module procedure create_xp_stencil_${itype}$
#:endfor
    end interface create_xp_stencil

    public :: create_hom_iso_stencil
    interface create_hom_iso_stencil
        !! Create a  stencil for arbitrary dimensions with same weighting along every direction
#:for itype in ikinds
        module procedure create_hom_iso_stencil_${itype}$
#:for rtype in rkinds
        module procedure create_hom_iso_stencil_${itype}$_${rtype}$
#:endfor
#:endfor
    end interface create_hom_iso_stencil

    public :: stencil2sparse_size
    interface stencil2sparse_size
        !! Compute size of the coo sparse matrix arrays required to store the stencil in matrix form
#:for itype in ikinds
        module procedure stencil2sparse_size_${itype}$
#:endfor
    end interface stencil2sparse_size

    public :: const_stencil2sparse
    interface const_stencil2sparse
        !! Convert a constant stencil into a sparse matrix representation in coo format.
#:for itype in ikinds
#:for rtype in rkinds
        module procedure const_stencil2sparse_${itype}$_${rtype}$
#:endfor
#:endfor
    end interface const_stencil2sparse

    public :: stencil2sparse
    interface stencil2sparse
        !! Convert an locally varying stencil into a sparse matrix representation in coo format.
        !! The argument sten should contain the entries of the stencil for each grid point. The first index runs over the
        !! linearly indexed data points and the second index on the linearly indexed entries of the stencil.
#:for itype in ikinds
#:for rtype in rkinds
        module procedure stencil2sparse_${itype}$_${rtype}$
#:endfor
#:endfor
    end interface stencil2sparse

    public :: convolve
    interface convolve
        !! Discrete convolution with a stencil. Signal is padded with 0 if necessary. Length of output signal is identical to
        !! length of input signal
#:for itype in ikinds
#:for rtype in rkinds
        module procedure convolve_${itype}$_${rtype}$
        module procedure nonconst_convolve_${itype}$_${rtype}$
#:endfor
#:endfor
    end interface convolve

    public :: xcorr
    interface xcorr
        !! Discrete cross correlation with a stencil. Signal is padded with 0 if necessary. Length of output signal is identical
        !! to length of input signal
#:for itype in ikinds
#:for rtype in rkinds
        module procedure xcorr_${itype}$_${rtype}$
#:endfor
#:endfor
    end interface xcorr

contains

    ! NAME
    !
    ! stencillocs
    !
    ! DESCRIPTION
    !
    ! Returns an array containing the linearly labeled positions in a stencil of size siz on a grid of size dims.
    !
    ! EXAMPLE
    !
    ! stencillocs([3, 5], [10, 12]) returns [-11, -10, -9, -6, -5, -4, -1, 0, 1, 4, 5, 6, 9, 10, 11]
    !
    ! ARGUMENTS
    !
    ! siz:  size of the stencil
    ! dims: size of the grid
    !
    ! RESULT
    !
    ! y: sorted linearly indexed positions
    !
    ! NOTES
    !
    ! siz may only contain odd integer entries whereas the entries in dims can be arbitrary integers. No error checking is done
    ! to verify that the obtained positions are not out of bounds.
    ! The algorithm works by splitting the result recursively for each considered dimension and finally applying the trivial 1D
    ! case.

#:for itype in ikinds
#ifndef DEBUG
    pure  recursive function stencillocs_${itype}$ (siz, dims) result(y)
#else
        recursive function stencillocs_${itype}$ (siz, dims) result(y)
#endif
            !! This version uses ${itype}$ as integer type.
        use :: miscfun, only : cumprod
        implicit none

        integer(${itype}$), dimension(:),         intent(in) :: siz  !! size of the stencil (must be odd integers)
        integer(${itype}$), dimension(size(siz)), intent(in) :: dims !! size of the grid

        integer(${itype}$), dimension(product(siz)) :: y !! stencil locations

        integer(${itype}$)                       :: ii
        integer(${itype}$)                       :: tmp
        integer(${itype}$), dimension(size(siz)) :: cumdim
        integer(${itype}$), dimension(size(siz)) :: cumsiz

        integer(${itype}$)                       :: bound

        cumsiz = cumprod (siz)
        cumdim = cumprod (dims)

        if (size(dims) == 1) then
            bound  = floor(real(siz(1))/2.0, ${itype}$)
            y = [(ii, ii = -bound, bound)]
        else
            tmp = ubound(cumsiz, 1, ${itype}$)
            do ii = 1_${itype}$, siz(tmp)
                y( ((ii-1)*cumsiz(tmp-1) + 1):((ii-1)*cumsiz(tmp-1) + cumsiz(tmp-1)) ) = &
                        cumdim(tmp-1) * ( (ii-1_${itype}$) - floor(real(siz(tmp))/2.0, ${itype}$) ) + &
                        stencillocs_${itype}$(siz(1:(tmp-1)), dims(1:(tmp-1)))
            end do
        end if

    end function stencillocs_${itype}$

#:endfor

#:for itype in ikinds
#ifndef DEBUG
    pure  function stencilmask_${itype}$ (siz, dims, x) result(y)
#else
        function stencilmask_${itype}$ (siz, dims, x) result(y)
#endif
            !! This version uses ${itype}$ as integer type.
        use :: array,   only : ind2sub
        use :: miscfun, only : cumprod, repelem
        implicit none

        integer(${itype}$), dimension(:),         intent(in) :: siz  !! size of the stencil (must be odd integers)
        integer(${itype}$), dimension(size(siz)), intent(in) :: dims !! size of the grid
        integer(${itype}$),                       intent(in) :: x    !! current position (linear index)

        logical, dimension(product(siz)) :: y !! logical array indicating valid positions

        integer(${itype}$), dimension(product(siz), size(siz)) :: tmp
        integer(${itype}$), dimension(size(siz)+2)             :: csiz
        integer(${itype}$), dimension(size(siz)+2)             :: cumsiz
        integer(${itype}$), dimension(size(siz))               :: sub
        integer(${itype}$)                                     :: ii, jj

        csiz = [1_${itype}$, siz, 1_${itype}$]
        cumsiz = cumprod(csiz)

        do ii = 1_${itype}$, int(size(siz), ${itype}$)
            tmp(1:product(siz), ii) = reshape(spread( &
                    repelem([(jj, jj = -floor(real(siz(ii))/2.0, ${itype}$), floor(real(siz(ii))/2.0, ${itype}$))], &
                    cumsiz(ii)), 2, product(csiz((ii+2):ubound(csiz, 1))) ), [product(siz)] )
        end do

        sub = ind2sub(dims, 1_${itype}$, 1_${itype}$, x)

        do ii = 1_${itype}$, product(siz)
            y(ii) = (.not. (any( tmp(ii, 1:size(siz)) + sub < 1 ) .or. any( tmp(ii, 1:size(siz)) + sub > dims )))
        end do

    end function stencilmask_${itype}$

#:endfor

#:for itype in ikinds
#ifndef DEBUG
    pure  function create_5p_stencil_${itype}$ (dims) result(sten)
#else
        function create_5p_stencil_${itype}$ (dims) result(sten)
#endif
            !! This version uses ${itype}$ as integer type.
        use :: array,   only : ind2sub
        use :: miscfun, only : cumprod, repelem
        implicit none

        integer(${itype}$), intent(in) :: dims !! size of the grid

        logical, dimension(3**dims) :: sten !! logical array indicating stencil positions.

        integer(${itype}$), dimension(3**dims, dims) :: tmp
        integer(${itype}$)                           :: ii

        do ii=1_${itype}$, dims
            tmp(1:(3**dims), ii) = reshape (spread ( &
                    repelem([-1_${itype}$, 0_${itype}$, 1_${itype}$], 3**(ii-1)), 2_${itype}$, 3**(dims-ii) ), [3**dims] )
        end do

        sten = .false.
        where (sum(abs(tmp), 2) <= 1) sten = .true.
    end function create_5p_stencil_${itype}$

#:endfor
#:for itype in ikinds
#ifndef DEBUG
    pure  function create_xp_stencil_${itype}$ (dims, num) result(sten)
#else
        function create_xp_stencil_${itype}$ (dims, num) result(sten)
#endif
            !! This version uses ${itype}$ as integer type.
        use :: array,   only : ind2sub
        use :: miscfun, only : cumprod, repelem
        implicit none

        integer(${itype}$), intent(in) :: dims !! size of the grid
        integer(${itype}$), intent(in) :: num  !! number of stencil entries (odd)

        logical, dimension(num**dims) :: sten !! logical array indicating stencil positions.

        integer(${itype}$), dimension(num**dims, dims) :: tmp
        integer(${itype}$)                             :: ii
        integer(${itype}$), dimension(num) :: dummy

        dummy = [(ii, ii=-floor(real(num)/2.0, ${itype}$), floor(real(num)/2.0, ${itype}$))]
        where (dummy > 1_${itype}$) dummy = 1_${itype}$
        where (dummy < -1_${itype}$) dummy = -1_${itype}$

        do ii=1_${itype}$, dims
            tmp(1:(num**dims), ii) = reshape (spread ( &
                    repelem(dummy, &
                    num**(ii-1)), 2_${itype}$, num**(dims-ii) ), [num**dims] )
        end do

        sten = .false.
        where (sum(abs(tmp), 2) <= 1) sten = .true.
    end function create_xp_stencil_${itype}$

#:endfor
#:for itype in ikinds
#ifndef DEBUG
    pure function create_hom_iso_stencil_${itype}$ (dims, num, weights) result(sten)
#else
    function create_hom_iso_stencil_${itype}$ (dims, num, weights) result(sten)
#endif
        !! Create stencil with same weights for every direction
        use :: array,   only : ind2sub
        use :: miscfun, only : cumprod, repelem
        implicit none

        integer(${itype}$),               intent(in) :: dims    !! dimension of the grid
        integer(${itype}$),               intent(in) :: num
        integer(${itype}$), dimension(:), intent(in) :: weights

        integer(${itype}$), dimension(num**dims) :: sten        !! weights for cross correlation

        integer(${itype}$), dimension(num**dims, dims) :: tmp
        integer(${itype}$)                             :: ii
        integer(${itype}$), dimension(:),   allocatable :: dummy
        integer(${itype}$), dimension(:,:), allocatable :: dummy2

        do ii= 1_${itype}$, dims
            allocate(dummy(num**(ii-1)))
            allocate(dummy2(num**(ii-1),num**(ii-1)))
            dummy  = repelem(weights, num**(ii-1))
            dummy2 = spread ( dummy, 2_${itype}$, num**(dims-ii) )
            tmp(1:(num**dims), ii) = reshape (dummy2, [num**dims] )
            deallocate(dummy, dummy2)
        end do

        sten = product(abs(tmp), 2)
    end function create_hom_iso_stencil_${itype}$

#:for rtype in rkinds
#ifndef DEBUG
    pure function create_hom_iso_stencil_${itype}$_${rtype}$ (dims, num, weights) result(sten)
#else
    function create_hom_iso_stencil_${itype}$_${rtype}$ (dims, num, weights) result(sten)
#endif
        !! Create stencil with same weights for every direction
        use :: array,   only : ind2sub
        use :: miscfun, only : cumprod, repelem
        implicit none

        integer(${itype}$),               intent(in) :: dims    !! dimension of the grid
        integer(${itype}$),               intent(in) :: num
        real(${rtype}$),    dimension(:), intent(in) :: weights

        real(${rtype}$),    dimension(num**dims) :: sten        !! weights for cross correlation

        real(${rtype}$),    dimension(num**dims, dims) :: tmp
        integer(${itype}$)                             :: ii
        real(${rtype}$),   dimension(:),   allocatable :: dummy
        real(${rtype}$),   dimension(:,:), allocatable :: dummy2

        do ii= 1_${itype}$, dims
            allocate(dummy(num**(ii-1)))
            allocate(dummy2(num**(ii-1), num**(ii-1)))
            dummy  = repelem(weights, num**(ii-1))
            dummy2 = spread ( dummy, 2_${itype}$, num**(dims-ii) )
            tmp(1:(num**dims), ii) = reshape (dummy2, [num**dims] )
            deallocate(dummy, dummy2)
        end do

        sten = product(abs(tmp), 2)
    end function create_hom_iso_stencil_${itype}$_${rtype}$

#:endfor
#:endfor
#:for itype in ikinds
#ifndef DEBUG
    pure  function stencil2sparse_size_${itype}$ (siz, dims, mask) result (numel)
#else
        function stencil2sparse_size_${itype}$ (siz, dims, mask) result (numel)
#endif
            !! This version uses ${itype}$ as integer type.
        implicit none

        integer(${itype}$), dimension(:),            intent(in) :: siz  !! size of the stencil
        integer(${itype}$), dimension(size(siz)),    intent(in) :: dims !! size of the grid
        logical,            dimension(product(siz)), intent(in) :: mask !! indicator for the relevant stencil entries

        integer(${itype}$) :: numel !! number of COO tuples

        integer(${itype}$) :: ii

        numel = 0
        do ii = 1_${itype}$, product(dims)
            numel = numel + int(count(stencilmask(siz, dims, ii) .and. mask), ${itype}$)
        end do
    end function stencil2sparse_size_${itype}$

#:endfor

#:for itype in ikinds
#:for rtype in rkinds
#ifndef DEBUG
    pure  subroutine const_stencil2sparse_${itype}$_${rtype}$ (siz, dims, mask, sten, ir, jc, a)
#else
        subroutine const_stencil2sparse_${itype}$_${rtype}$ (siz, dims, mask, sten, ir, jc, a)
#endif
            !! This version uses ${itype}$ as integer type and ${rtype}$ for the stencil entries
        implicit none

        integer(${itype}$), dimension(:),            intent(in) :: siz  !! size of the stencil
        integer(${itype}$), dimension(size(siz)),    intent(in) :: dims !! size of the grid
        logical,            dimension(product(siz)), intent(in) :: mask !! indicator for the relevant stencil entries
        real(${rtype}$),    dimension(product(siz)), intent(in) :: sten !! stencil entries

        integer(${itype}$), dimension(count(mask)*int(product(dims))), intent(out) :: ir !! row indices
        integer(${itype}$), dimension(count(mask)*int(product(dims))), intent(out) :: jc !! column indices
        real(${rtype}$),    dimension(count(mask)*int(product(dims))), intent(out) :: a  !! corresponding entries

        ! integer(${itype}$), dimension(product(siz)) :: unused
        logical,            dimension(product(siz)) :: tmp_mask
        integer(${itype}$)                          :: tmp_cnt
        integer(${itype}$)                          :: counter
        integer(${itype}$), dimension(product(siz)) :: const_stl
        integer(${itype}$)                          :: ii, jj

        ! unused = -1
        counter = 1_${itype}$
        const_stl = stencillocs(siz, dims)

        do ii = 1_${itype}$, product(dims)
            tmp_mask                        = stencilmask(siz, dims, ii) .and. mask
            tmp_cnt                         = int(count(tmp_mask), ${itype}$)
            ir(counter:(counter+tmp_cnt-1)) = [(ii, jj=1_${itype}$, tmp_cnt)]
            jc(counter:(counter+tmp_cnt-1)) = pack(ii + const_stl, tmp_mask)
            a(counter:(counter+tmp_cnt-1))  = pack(sten, tmp_mask)
            counter                         = counter + tmp_cnt
        end do
    end subroutine const_stencil2sparse_${itype}$_${rtype}$

#:endfor
#:endfor
#:for itype in ikinds
#:for rtype in rkinds
#ifndef DEBUG
    pure subroutine stencil2sparse_${itype}$_${rtype}$ (siz, dims, mask, sten, ir, jc, a)
#else
        subroutine stencil2sparse_${itype}$_${rtype}$ (siz, dims, mask, sten, ir, jc, a)
#endif
            !! This version uses ${itype}$ as integer type and ${rtype}$ for the stencil entries
        implicit none

        integer(${itype}$), dimension(:),                           intent(in) :: siz  !! size of the stencil
        integer(${itype}$), dimension(size(siz)),                   intent(in) :: dims !! size of the grid
        logical,            dimension(product(siz)),                intent(in) :: mask !! indicator for the stencil entries
        real(${rtype}$),    dimension(product(dims), product(siz)), intent(in) :: sten !! stencil entries

        integer(${itype}$), dimension(count(mask)*int(product(dims))), intent(out) :: ir !! row indices
        integer(${itype}$), dimension(count(mask)*int(product(dims))), intent(out) :: jc !! column indices
        real(${rtype}$),    dimension(count(mask)*int(product(dims))), intent(out) :: a  !! corresponding entries

        logical,            dimension(product(siz)) :: tmp_mask
        integer(${itype}$)                          :: tmp_cnt
        integer(${itype}$)                          :: counter
        integer(${itype}$), dimension(product(siz)) :: const_stl
        integer(${itype}$)                          :: ii, jj

        counter = 1_${itype}$
        const_stl = stencillocs(siz, dims)

        do ii = 1_${itype}$, product(dims)
            tmp_mask                        = stencilmask(siz, dims, ii) .and. mask
            tmp_cnt                         = int(count(tmp_mask), ${itype}$)
            ir(counter:(counter+tmp_cnt-1)) = [(ii, jj=1_${itype}$, tmp_cnt)]
            jc(counter:(counter+tmp_cnt-1)) = pack(ii + const_stl, tmp_mask)
            a(counter:(counter+tmp_cnt-1))  = pack(sten(ii,:), tmp_mask)
            counter                         = counter + tmp_cnt
        end do
    end subroutine stencil2sparse_${itype}$_${rtype}$

#:endfor
#:endfor
#:for itype in ikinds
#:for rtype in rkinds
#ifndef DEBUG
    pure function convolve_${itype}$_${rtype}$ (dims, siz, arr, ker, mask) result(res)
#else
        function convolve_${itype}$_${rtype}$ (dims, siz, arr, ker, mask) result(res)
#endif
            !! This version uses ${itype}$ as integer type and ${rtype}$ for the signal and kernel entries
            implicit none

            integer(${itype}$), intent(in), dimension(:)             :: dims !! size of the signal
            integer(${itype}$), intent(in), dimension(size(dims))    :: siz  !! size of the kernel
            real(${rtype}$),    intent(in), dimension(product(dims)) :: arr  !! input signal
            real(${rtype}$),    intent(in), dimension(product(siz))  :: ker  !! convolution kernel
            logical,            intent(in), dimension(product(siz))  :: mask !! stencil mask

            real(${rtype}$), dimension(product(dims)) :: res

            integer(${itype}$) :: ii

            res = 0.0_${rtype}$

            do ii = lbound(arr, 1, ${itype}$), ubound(arr, 1, ${itype}$)
                res(ii) = sum( &
                        arr(ii + stencillocs (siz, dims)) * &
                        ker(ubound(ker,1):lbound(ker,1):-1), &
                        1, mask .and. stencilmask(siz, dims, ii) )
            end do
        end function convolve_${itype}$_${rtype}$

#:endfor
#:endfor
#:for itype in ikinds
#:for rtype in rkinds
#ifndef DEBUG
    pure function nonconst_convolve_${itype}$_${rtype}$ (dims, siz, arr, ker, mask) result(res)
#else
        function nonconst_convolve_${itype}$_${rtype}$ (dims, siz, arr, ker, mask) result(res)
#endif
            !! This version uses ${itype}$ as integer type and ${rtype}$ for the signal and kernel entries
            implicit none

            integer(${itype}$), intent(in), dimension(:)                            :: dims !! size of the signal
            integer(${itype}$), intent(in), dimension(size(dims))                   :: siz  !! size of the kernel
            real(${rtype}$),    intent(in), dimension(product(dims))                :: arr  !! input signal
            real(${rtype}$),    intent(in), dimension(product(dims), product(siz))  :: ker  !! convolution kernel
            logical,            intent(in), dimension(product(siz))                 :: mask !! stencil mask

            real(${rtype}$), dimension(product(dims)) :: res

            integer(${itype}$) :: ii

            res = 0.0_${rtype}$

            do ii = lbound(arr, 1, ${itype}$), ubound(arr, 1, ${itype}$)
                res(ii) = sum( &
                        arr(ii + stencillocs (siz, dims)) * &
                        ker(ii, ubound(ker,1):lbound(ker,1):-1), &
                        1, mask .and. stencilmask(siz, dims, ii) )
            end do
        end function nonconst_convolve_${itype}$_${rtype}$

#:endfor
#:endfor
#:for itype in ikinds
#:for rtype in rkinds
#ifndef DEBUG
    pure function xcorr_${itype}$_${rtype}$ (dims, siz, arr, ker, mask) result(res)
#else
        function xcorr_${itype}$_${rtype}$ (dims, siz, arr, ker, mask) result(res)
#endif
            !! This version uses ${itype}$ as integer type and ${rtype}$ for the signal and kernel entries
            implicit none

            integer(${itype}$), intent(in), dimension(:)             :: dims !! size of the signal
            integer(${itype}$), intent(in), dimension(size(dims))    :: siz  !! size of the kernel
            real(${rtype}$),    intent(in), dimension(product(dims)) :: arr  !! input signal
            real(${rtype}$),    intent(in), dimension(product(siz))  :: ker  !! convolution kernel
            logical,            intent(in), dimension(product(siz))  :: mask !! stencil mask

            real(${rtype}$), dimension(product(dims)) :: res

            integer(${itype}$) :: ii

            res = 0.0_${rtype}$

            do ii = lbound(arr, 1, ${itype}$), ubound(arr, 1, ${itype}$)
                res(ii) = sum( arr(ii + stencillocs (siz, dims)) * ker, 1, mask .and. stencilmask(siz, dims, ii) )
            end do
        end function xcorr_${itype}$_${rtype}$

#:endfor
#:endfor

end module stencil
