#include <stdio.h>
#include <stdlib.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cusparse_v2.h>

// Set to 0
__global__ void set0 (double* x, int n) {
  int ii = blockDim.x * blockIdx.x + threadIdx.x;
  if (ii < n) {x[ii] = 0.0;}
  return;
}

__global__ void set0i (int* x, int n) {
  int ii = blockDim.x * blockIdx.x + threadIdx.x;
  if (ii < n) {x[ii] = 0.0;}
  return;
}

// Compute out = a * x * y + b * z
__global__ void axypbz (
			const double* x,
			const double* y,
			const double* z,
			double        a,
			double        b,
			int           n,
			double     *out
			) {
  int ii = blockDim.x * blockIdx.x + threadIdx.x;
  if (ii < n) {out[ii] = a*x[ii]*y[ii] + b*z[ii];}
  return;
}

// Compute out = a * x + b * y
__global__ void axpby (
		       const double* x,
		       const double* y,
		       double        a,
		       double        b,
		       int           n,
		       double     *out
		       ) {
  int ii = blockDim.x * blockIdx.x + threadIdx.x;
  if (ii < n) {out[ii] = a*x[ii] + b*y[ii];}
  return;
}

// Compute softshrinkage
__global__ void shrinkage (
			   const double* x,
			   double        t,
			   int           n,
			   double     *out
			   ) {
  int ii = blockDim.x * blockIdx.x + threadIdx.x;
  if (ii < n) {
    if      (x[ii] < -t) {out[ii] = x[ii] + t;}
    else if (x[ii] >  t) {out[ii] = x[ii] - t;}
    else                 {out[ii] = 0.0;}
  }
  return;
}

extern "C" void opt_linearised_pd (
				   int     n,      // [in]     size of all the data
				   int     nnz,    // [in]     size of the sparse matrix (COO format)
				   double *u,      // [in,out] solution
				   double *c,      // [in,out] mask
				   double *f,      // [in]     image
				   int    *Air,    // [in]     Gateaux derivative w.r.t. c (row index)
				   int    *Ajc,    // [in]     Gateaux derivative w.r.t. c (col index)
				   double *A,      // [in]     Gateaux derivative w.r.t. c (val)
				   double *B,      // [in]     Gateaux derivative w.r.t. u
				   double *g,      // [in]     constant terms of linearisation
				   double *ubar,   // [in]     linearisation point u
				   double *cbar,   // [in]     linearisation point c
				   double lambda,  // [in]     Sparsity weight
				   double mu,      // [in]     Proximal weight
				   double epsilon, // [in]     Regularisation weight
				   double tau,     // [in]     primal dual solver parameter
				   double sigma,   // [in]     primal dual solver parameter
				   double L,       // [in]     estimate for operator norm
				   int    maxIt    // [in]     number of iterations
				   ) {

  // ** LOCAL VARIABLES ** //

  cublasStatus_t     blas_status;
  cusparseStatus_t sparse_status;
  cudaError_t        cuda_status;

  cublasHandle_t     blas_handle;
  cusparseHandle_t sparse_handle;

  cusparseMatDescr_t descr_A; // Matrix descriptor for A

  /* int *h_csr_A, *h_Ajc; */
  /* double *h_A, *h; */

  // Corresponding vars on device have the prefix cu_
  double *cu_u;    // Corresponding Solution
  double *cu_c;    // Mask
  double *cu_f;    // Image Data
  int    *cu_Air;  // Gateaux derivative w.r.t. c (row index)
  int    *cu_Ajc;  // Gateaux derivative w.r.t. c (col index)
  double *cu_A;    // Gateaux derivative w.r.t. c (val)
  double *cu_B;    // Gateaux derivative w.r.t. u
  double *cu_g;    // constant terms of linearisation
  double *cu_ubar; // linearisation point u
  double *cu_cbar; // linearisation point c

  // Additional variables required by the algorithm
  double *cu_y;    // dual var y
  double *cu_uold; // previous iterate u
  double *cu_cold; // previous iterate c
  double *cu_uhat; // dummy variable uhat
  double *cu_chat; // dummy variable chat

  // dummy storage vars
  double *cu_Au;   // stores A*u
  double *cu_ATy;  // stores A^T*y
  double *cu_diff; // dummy var to store differences between vectors

  // helper vars
  int    *cu_csr_A; // Gateaux derivative w.r.t. c (row pointer in csr format)

  const double   one =  1.0;
  const double m_one = -1.0;
  const double f_u   =  1.0/(1.0 + tau*mu + tau);
  const double g_u   = -1.0 * tau;
  const double h_u   = sigma;
  const double f_c   = 1.0/(1.0 + tau*mu + tau*epsilon);
  const double g_c   = tau*lambda*f_c;
  const double theta = 1.0;
  const double m_mu  = -1.0*mu;

  int ii;
  double dist;

  const int threadsPerBlock = 256;
  const int blocksPerGrid = (n + threadsPerBlock - 1) / threadsPerBlock;

  // *** Initialise environment ********************************************* //

  blas_status = cublasCreate(&blas_handle);
  if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("cuBLAS Library initialization failed"); return;}

  sparse_status = cusparseCreate (&sparse_handle);
  if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("cuSparse Library initialization failed"); return;}

  sparse_status = cusparseCreateMatDescr (&descr_A);
  if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("Matrix descriptor initialization failed"); return;}

  sparse_status = cusparseSetMatType (descr_A, CUSPARSE_MATRIX_TYPE_GENERAL);
  if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("Matrix type setting failed"); return;}

  sparse_status = cusparseSetMatIndexBase (descr_A, CUSPARSE_INDEX_BASE_ONE);
  if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("Matrix index base setting failed"); return;}

  // Allocate space for variables on device

  cuda_status = cudaMalloc ((void**) &cu_u,     n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_c,     n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_f,     n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_Air, nnz*sizeof(int));    if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_Ajc, nnz*sizeof(int));    if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_A,   nnz*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_B,     n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_g,     n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_ubar,  n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_cbar,  n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}

  cuda_status = cudaMalloc ((void**) &cu_y,     n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_uold,  n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_cold,  n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_uhat,  n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_chat,  n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}

  cuda_status = cudaMalloc ((void**) &cu_Au,    n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_ATy,   n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}
  cuda_status = cudaMalloc ((void**) &cu_diff,  n*sizeof(double)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}

  cuda_status = cudaMalloc ((void**) &cu_csr_A, (n+1)*sizeof(int)); if (cuda_status != cudaSuccess) {printf("cudaMalloc failed in %d\n",__LINE__); return;}

  // Copy data to device

  cuda_status = cudaMemcpy (   cu_u,    u, (size_t)   (n*sizeof(double)), cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy (   cu_c,    c, (size_t)   (n*sizeof(double)), cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy (   cu_f,    f, (size_t)   (n*sizeof(double)), cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy ( cu_Air,  Air, (size_t) (nnz*sizeof(int)),    cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy ( cu_Ajc,  Ajc, (size_t) (nnz*sizeof(int)),    cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy (   cu_A,    A, (size_t) (nnz*sizeof(double)), cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy (   cu_B,    B, (size_t) (n*sizeof(double)),   cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy (   cu_g,    g, (size_t) (n*sizeof(double)),   cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy (cu_ubar, ubar, (size_t) (n*sizeof(double)),   cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy (cu_cbar, cbar, (size_t) (n*sizeof(double)),   cudaMemcpyHostToDevice); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}

  // Init remaining arrays with 0

  set0<<<blocksPerGrid, threadsPerBlock>>>(cu_y,    n);
  set0<<<blocksPerGrid, threadsPerBlock>>>(cu_uold, n);
  set0<<<blocksPerGrid, threadsPerBlock>>>(cu_cold, n);
  set0<<<blocksPerGrid, threadsPerBlock>>>(cu_uhat, n);
  set0<<<blocksPerGrid, threadsPerBlock>>>(cu_chat, n);
  set0<<<blocksPerGrid, threadsPerBlock>>>(cu_Au,   n);
  set0<<<blocksPerGrid, threadsPerBlock>>>(cu_ATy,  n);
  set0<<<blocksPerGrid, threadsPerBlock>>>(cu_diff, n);

  set0i<<<blocksPerGrid, threadsPerBlock>>>(cu_csr_A, n+1);

  // cudaDeviceSynchronize();

  // *** Initialise environment ********************************************* //

  // Convert A to CSR format
  sparse_status = cusparseXcoo2csr (sparse_handle, cu_Air, nnz, n, cu_csr_A, CUSPARSE_INDEX_BASE_ONE); if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("Failure in %d with CODE %d\n",__LINE__,sparse_status); return;}

  // cudaDeviceSynchronize();

  /* blas_status = cublasSetVector(n, sizeof(double), ubar, 1, cu_u, 1); */
  /* if (blas_status !=  CUBLAS_STATUS_SUCCESS) { */
  /*     printf("Failure in %d",__LINE__); */
  /*     return; */
  /*   } */
  /* blas_status = cublasSetVector(n, sizeof(double), cbar, 1, cu_c, 1); */
  /* if (blas_status !=  CUBLAS_STATUS_SUCCESS) { */
  /*     printf("Failure in %d",__LINE__); */
  /*     return; */
  /*   } */

  for (ii = 0; ii < maxIt; ii++) {

    // --- Solve linearised problem with primal dual method --------------------------------------- //

    // --- Proximal Step on y with F^* -----------------------------------------------------  //

    // Copy iterates into cu_*old variables
    blas_status = cublasDcopy(blas_handle, n, cu_c, 1, cu_cold, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}
    blas_status = cublasDcopy(blas_handle, n, cu_u, 1, cu_uold, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    // cudaDeviceSynchronize();

    // Compute cu_Au = B*chat - g
    axypbz<<<blocksPerGrid, threadsPerBlock>>> (cu_B, cu_chat, cu_g, one, m_one, n, cu_Au);

    // cudaDeviceSynchronize();

    // Compute cu_Au = A*uhat + cu_Au = A*uhat + B*chat - g
    sparse_status = cusparseDcsrmv(sparse_handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
				   n, n, nnz,
				   &one,
				   descr_A, cu_A, cu_csr_A, cu_Ajc,
				   cu_uhat,
				   &one,
				   cu_Au);
    if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("Failure in %d with CODE %d\n",__LINE__,sparse_status); return;}

    // cudaDeviceSynchronize();

    // Compute y = y + sigma * cu_Au (i.e. y = y + sigma ( A*uhat + B*chat - g)
    blas_status = cublasDaxpy(blas_handle, n, &h_u, cu_Au, 1, cu_y, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    // --- Proximal Step on (u,c) with G^* -------------------------------------------------  //

    // --- update u -----------------------------------------------------------------  //

    // Compute cu_ATy = - f - mu * ubar
    axpby<<<blocksPerGrid, threadsPerBlock>>> (cu_f, cu_ubar, m_one, m_mu, n, cu_ATy);

    // cudaDeviceSynchronize();

    // Compute cu_ATy = A^T*y + cu_ATy = A^T*y - f - mu * ubar
    sparse_status = cusparseDcsrmv(sparse_handle, CUSPARSE_OPERATION_TRANSPOSE,
				   n, n, nnz,
				   &one,
				   descr_A, cu_A, cu_csr_A, cu_Ajc,
				   cu_y,
				   &one,
				   cu_ATy);
    if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("Failure in %d with CODE %d\n",__LINE__,sparse_status); return;}

    // cudaDeviceSynchronize();

    // Compute u = u - tau * cu_ATy = u - tau * (A^T*y - f - mu * ubar)
    blas_status = cublasDaxpy(blas_handle, n, &g_u, cu_ATy, 1, cu_u, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    // cudaDeviceSynchronize();

    // Compute u = u/(1+tau+mu*tau) (i.e. u = (u - tau * (A^T*y - f - mu * ubar))/(1+tau+mu*tau)
    blas_status = cublasDscal(blas_handle, n, &f_u, cu_u, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    // cudaDeviceSynchronize();

    // --- update c -----------------------------------------------------------------  //

    // chat = -tau * B * y + tau*mu*cbar
    // chat is used as dummy storage !
    axypbz<<<blocksPerGrid, threadsPerBlock>>> (cu_B, cu_y, cu_cbar, g_u, tau*mu, n, cu_chat);

    // cudaDeviceSynchronize();

    // c = cu_hat + c = c -tau * B * y + tau*mu*cbar
    blas_status = cublasDaxpy(blas_handle, n, &one, cu_chat, 1, cu_c, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    // cudaDeviceSynchronize();

    // c = c / (1 + tau*mu + tau*epsi) (i.e. c = (c -tau * B * y + tau*mu*cbar)/(1 + tau*mu + tau*epsi)
    blas_status = cublasDscal(blas_handle, n, &f_c, cu_c, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    // cudaDeviceSynchronize();

    // copy c into chat
    blas_status = cublasDcopy(blas_handle, n, cu_c, 1, cu_chat, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    // cudaDeviceSynchronize();

    // perform shrinkage on c
    shrinkage<<<blocksPerGrid, threadsPerBlock>>> (cu_chat, g_c, n, cu_c);

    // cudaDeviceSynchronize();

    // --- Extrapolation step with theta = 1 -------------------------------------------------- //

    // uhat = u + theta * (u - u_old)
    axpby<<<blocksPerGrid, threadsPerBlock>>> (cu_u, cu_uold, 1+theta, -theta, n, cu_uhat);

    // chat = c + theta * (c - c_old)
    axpby<<<blocksPerGrid, threadsPerBlock>>> (cu_c, cu_cold, 1+theta, -theta, n, cu_chat);

    // --- Solve linearised problem with primal dual method (finished) ---------------------------- //
    // cudaDeviceSynchronize();

    // --- Check whether fixed point has been reached -- //

    // Check on c
    // diff = c
    // diff = diff - cold = c - cold
    // dist = ||diff||
    blas_status = cublasDcopy(blas_handle, n,            cu_c, 1, cu_diff, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}
    blas_status = cublasDaxpy(blas_handle, n, &m_one, cu_cold, 1, cu_diff, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}
    blas_status = cublasDnrm2(blas_handle, n,         cu_diff, 1,   &dist   ); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    if (dist < 1.0e-8) {break;}

    // Check on u
    // diff = u
    // diff = diff - cold = u - uold
    // dist = ||diff||
    blas_status = cublasDcopy(blas_handle, n,            cu_u, 1, cu_diff, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}
    blas_status = cublasDaxpy(blas_handle, n, &m_one, cu_uold, 1, cu_diff, 1); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}
    blas_status = cublasDnrm2(blas_handle, n,         cu_diff, 1,   &dist   ); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

    if (dist < 1.0e-8) {break;}

  }

  printf("Stopped at iteration       %d/%d\n",ii,maxIt);

  // -- Cleanup environment -- //

  sparse_status = cusparseDestroyMatDescr(descr_A); if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("Failure in %d with CODE %d\n",__LINE__,sparse_status); return;}
  sparse_status = cusparseDestroy(sparse_handle);   if (sparse_status != CUSPARSE_STATUS_SUCCESS) {printf("Failure in %d with CODE %d\n",__LINE__,sparse_status); return;}

  blas_status = cublasDestroy(blas_handle); if (blas_status != CUBLAS_STATUS_SUCCESS) {printf("Failure in %d\n",__LINE__); return;}

  // Copy to host

  cuda_status = cudaMemcpy (u, cu_u, (size_t) (n*sizeof(double)), cudaMemcpyDeviceToHost); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}
  cuda_status = cudaMemcpy (c, cu_c, (size_t) (n*sizeof(double)), cudaMemcpyDeviceToHost); if (cuda_status != cudaSuccess) {printf("cudaMemcpy failed in %d\n",__LINE__); return;}

  // Free Memory

  cuda_status = cudaFree(cu_u);     if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_c);     if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_f);     if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_Air);   if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_Ajc);   if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_A);     if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_B);     if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_g);     if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_ubar);  if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_cbar);  if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_y);     if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_uold);  if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_cold);  if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_uhat);  if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_chat);  if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_Au);    if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_ATy);   if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_diff);  if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}
  cuda_status = cudaFree(cu_csr_A); if (cuda_status != cudaSuccess) {printf("cudaFree failed in %d\n",__LINE__); return;}

  return;
}
