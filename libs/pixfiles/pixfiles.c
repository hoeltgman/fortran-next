/* This file is part of libpixfiles.

libpixfiles is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libpixfiles is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libpixfiles.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#ifdef __APPLE__
#include <machine/endian.h>
#else
#include <endian.h>
#endif
#include <byteswap.h>
#include <math.h>

#include "pixfiles.h"

#ifdef PIXF_ENABLE_VERBOSE
	int pixf_verbose = 0;
#endif

#define PPM_MAXVAL 65535

#define REMARK_CHAR '#'
#define IS_ENDLINE(x) (((x) == '\n') || ((x) == '\r'))
#define IS_PNM_SPACE(x) (((x) == ' ') || IS_ENDLINE(x))

#define	N_FORMATS 6

typedef enum {
	PF_ABITS = 0, PF_AGRAY, PF_ARGB, PF_BITS, PF_GRAY, PF_RGB
} Format;

static char FORMAT_MAGICS[N_FORMATS][3] = {
	"P1", "P2", "P3", "P4", "P5", "P6"
};

/* number of sub-pixel values per line */
#define N_PER_LINE_1 69
#define N_PER_LINE_8 16
#define N_PER_LINE_16 10

/* finds end of line (end of comment) */
static int skip_comment(FILE *f)
{
	int err;

	clearerr(f);

	do {
		err = fgetc(f);
	} while (err != EOF && !IS_ENDLINE(err));

	if (err == EOF && ferror(f)) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: Error reading file: %s\n", __func__,
					strerror(errno));
		#endif
		return PIXF_ERR_READ;
	}

	return PIXF_ERR_OK;
}

/* skips whitespaces (and comment lines). parameter "single" determines
whether only one whitespace character (besides comments) should be skipped */
static int skip_whitespace(FILE *f, int single)
{
	size_t n = 0;
	int    err;

	clearerr(f);

	for (;;) {
		err = fgetc(f);

		if (err == REMARK_CHAR) {
			if ((err = skip_comment(f)) != PIXF_ERR_OK)
				return err;
			else continue;
		}

		if (err == EOF || !IS_PNM_SPACE(err)) break;

		if (single && n > 0) break;

		n++;
	}

	if (err == EOF && ferror(f)) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: Error reading from file: %s\n", __func__,
					strerror(errno));
		#endif
		return PIXF_ERR_READ;
	}

	ungetc(err, f); /* return first non-whitespace char back to the stream */

	return PIXF_ERR_OK;
}

/* Get integer from stream. Skip whitespaces and comments first.*/
static int get_number(FILE *f, int *number)
{
	char num[33] = {0};
	int  err, npos = 0;

	clearerr(f);

	err = skip_whitespace(f, 0);

	if (err != PIXF_ERR_OK) return err;

	do {
		err = fgetc(f);
		num[npos++] = (char)err;
	} while	(err != EOF && isdigit(err) && npos <= 32);

	if (err == EOF && ferror(f)) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: Error reading from file: %s\n", __func__,
					strerror(errno));
		#endif
		return PIXF_ERR_READ;
	}

	/* return non-digit char back to the stream */
	ungetc(err, f);

	err = sscanf(num, "%d", number);

	if (err == 0 || err == EOF) return PIXF_ERR_PPM;

	return PIXF_ERR_OK;
}

/*
   Determine format of PPM file, e.g. "P4"-bitmap, "P5"-grayscale, "P6" - RGB.

   Returns PIXF_ERR_OK on success and valid format, PIXF_ERR_READ on read error
   and PIXF_ERR_PPM on invalid format or EOF.

   Actual format of the file is returned in fmt parameter.

*/
static int format(FILE *f, Format *fmt)
{
	size_t i;
	char   str[3];
	int    err;

	clearerr(f);

	err = fgetc(f);

	if (err == EOF) {
		if (ferror(f)) {
			#ifdef PIXF_ENABLE_VERBOSE
				if (pixf_verbose)
					fprintf(stderr, "%s: Error reading file: %s\n", __func__,
						strerror(errno));
			#endif
			return PIXF_ERR_READ;
		}	else return PIXF_ERR_PPM;
	}

	str[0] = (char)err;

	err = fgetc(f);

	if (err == EOF) {
		if (ferror(f)) {
			#ifdef PIXF_ENABLE_VERBOSE
				if (pixf_verbose)
					fprintf(stderr, "%s: Error reading file: %s\n", __func__,
						strerror(errno));
			#endif
			return PIXF_ERR_READ;
		} else return PIXF_ERR_PPM;
	}

	str[1] = (char)err;
	str[2] = 0;

	for (i = 0; i < N_FORMATS; i++) {
		if (strcmp(str, FORMAT_MAGICS[i]) == 0) {
			*fmt = (Format)i;
			return PIXF_ERR_OK;
		}
	}

	return PIXF_ERR_PPM;
}

static int check_header(FILE *f, Format *fmt, size_t *width, size_t *height,
	size_t *maxval)
{
	int err,num;

	err=format(f,fmt);

	if (err!=PIXF_ERR_OK) return err;

	#ifdef DEBUG
		fprintf(stderr,"%s: format: %s\n", __func__, FORMAT_MAGICS[*fmt]);
	#endif

	err=get_number(f,&num);

	if (err==PIXF_ERR_PPM) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: image has an invalid width specified!\n",
					__func__);
		#endif
		return PIXF_ERR_PPM;
	}

	if (err!=PIXF_ERR_OK) return err;

	*width=(size_t)num;

	#ifdef DEBUG
		fprintf(stderr, "%s: width: %u px\n", __func__, (unsigned) *width);
	#endif

	err = get_number(f, &num);

	if (err == PIXF_ERR_PPM) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: image has an invalid height specified!\n",
					__func__);
		#endif
		return PIXF_ERR_PPM;
	}

	if (err != PIXF_ERR_OK) return err;

	*height = (size_t)num;

	#ifdef DEBUG
		fprintf(stderr, "%s: height: %u px\n", __func__, (unsigned) *height);
	#endif

	if (*fmt!=PF_BITS && *fmt!=PF_ABITS) {
		err=get_number(f,&num);

		if (err==PIXF_ERR_PPM) {
			#ifdef PIXF_ENABLE_VERBOSE
				if (pixf_verbose)
					fprintf(stderr, "%s: image has invalid sample size!\n",
						__func__);
			#endif
			return PIXF_ERR_PPM;
		}

		if (err!=PIXF_ERR_OK) return err;

		*maxval=(size_t)num;
	} else *maxval=1;

	#ifdef DEBUG
		fprintf(stderr,"%s: maxval: %u\n", __func__, (unsigned) *maxval);
	#endif

	err=skip_whitespace(f, 1);

	if (err!=PIXF_ERR_OK) return err;

	return PIXF_ERR_OK;
}

int pixf_read_info(PIXF_Info *info)
{
	int    err;
	Format fmt;

	err=check_header(info->f,&fmt,&info->width,&info->height,&info->maxval);

	if (err!=PIXF_ERR_OK) return err;

	if (info->maxval>PPM_MAXVAL) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: invalid sample size (maxval) specified in file!\n",
					__func__);
		#endif
		return PIXF_ERR_PPM;
	}

	info->ascii=0;
	switch (fmt) {
		case PF_ABITS:
			info->ascii=1;
		case PF_BITS:
			info->bps=1;
			info->bpp=1;
			break;
		case PF_AGRAY:
			info->ascii=1;
		case PF_GRAY:
			if (info->maxval<=255) {
				info->bps=8;
				info->bpp=8;
			} else {
				info->bps=16;
				info->bpp=16;
			}
			break;
		case PF_ARGB:
			info->ascii=1;
		case PF_RGB:
			if (info->maxval<=255) {
				info->bps=8;
				info->bpp=24;
			} else {
				info->bps=16;
				info->bpp=48;
			}
			break;
	}

	return PIXF_ERR_OK;
}

int pixf_open(const char *fname,PIXF_Info *info)
{
	info->f=fopen(fname, "rb");

	if (info->f==NULL) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: error opening file: %s", __func__,
					strerror(errno));
		#endif
		return PIXF_ERR_OPEN;
	}

	return pixf_read_info(info);
}

static void swap_b(void *buf, size_t size)
{
	unsigned char *bptr = (unsigned char *)buf;
	size_t        n = size;

	while (n >= 2) {
		uint16_t x = *(uint16_t *)bptr;
		*(uint16_t *)bptr = bswap_16(x);
		bptr += 2;
		n -= 2;
	}
}

static void neg(void *bytes, size_t size)
{
	unsigned char *bptr = (unsigned char *)bytes;
	size_t        i;

	for (i = 0; i < size; i++, bptr++)
		*bptr = ~(*bptr);
}

static int read_raw(const PIXF_Info *info, void *imgbytes, size_t hsize)
{
	unsigned char *iptr = (unsigned char *)imgbytes;
	size_t        i, n;

	for (i = 0; i < info->height; i++) {
		n = fread(iptr, 1, hsize, info->f);

		if (n < hsize) {
			#ifdef PIXF_ENABLE_VERBOSE
				if (pixf_verbose)
					fprintf(stderr, "%s: unexpected EOF during reading of image data (expected %lu bytes, read %lu)\n",
						__func__, (unsigned long)hsize, (unsigned long)n);
			#endif
			return PIXF_ERR_READ;
		}

		/* if we are running on little-endian machine and sample size is
		more than 8 bits (aligned to 16 bits), swap bytes */
		#if __BYTE_ORDER==__LITTLE_ENDIAN
			if (info->bps>8) swap_b(iptr,hsize);
		#endif

		if (info->bpp == 1) neg((void *)iptr,hsize);

		iptr += hsize;
	}

	return PIXF_ERR_OK;
}

static int read_ascii(const PIXF_Info *info, void *imgbytes)
{
	unsigned char *iptr = (unsigned char *)imgbytes;
	size_t        i, j;
	int           err;

	if (info->bpp==1) {
		for (i=0;i<info->height;i++) {
			unsigned char bit=0x80;
			for (j=0;j<info->width;j++) {
				do {
					err=fgetc(info->f);

					if (err==EOF) {
						#ifdef PIXF_ENABLE_VERBOSE
							if (pixf_verbose)
								fprintf(stderr, "%s: error reading image data!\n",
									__func__);
						#endif
						return PIXF_ERR_READ;
					}
				} while (IS_PNM_SPACE(err));

				if ((char)err=='0')	*((unsigned char *)iptr)|=bit;

				if (bit==1) {
					bit=0x80;
					iptr++;
				} else bit>>=1;
			}
		}
	} else {
		size_t hsize=info->width;

		if (info->bpp>info->bps) hsize*=3; /* RGB image */

		for (i=0;i<info->height;i++) {
			for (j=0;j<hsize;j++) {
				unsigned x=0;

				do {
					err=fgetc(info->f);

					if (err==EOF) {
						#ifdef PIXF_ENABLE_VERBOSE
							if (pixf_verbose)
								fprintf(stderr, "%s: error reading image data!\n",
									__func__);
						#endif
						return PIXF_ERR_READ;
					}
				} while (IS_PNM_SPACE(err));

				ungetc(err,info->f);

				err=fscanf(info->f,"%u",&x);

				if (err!=1) {
					#ifdef PIXF_ENABLE_VERBOSE
						if (pixf_verbose)
							fprintf(stderr, "%s: error reading image data: %s\n",
								__func__, strerror(errno));
					#endif
					return PIXF_ERR_READ;
				}

				if (info->bps<=8) {
					*((unsigned char *)iptr)=(unsigned char)x;
					iptr++;
				} else {
					*((uint16_t *)iptr)=x;
					iptr+=2;
				}
			}
		}
	} /* info->bpp!=1 */

	return PIXF_ERR_OK;
}

size_t pixf_get_size(const PIXF_Info *info,size_t *pitch)
{
	size_t hsize,size;

	if (info->bpp==1) {
		hsize=(info->width+7)>>3;
		size=info->height*hsize;
	} else {
		/* grayscale image */
		if (info->bpp==info->bps) {
			hsize=info->width*((info->bps+7)>>3);
		} else { /* RGB */
			hsize=info->width*((info->bps+7)>>3)*3;
		}
		size=hsize*info->height;
	}

	if (pitch!=NULL) *pitch=hsize;

	return size;
}

int pixf_read(const PIXF_Info *info, void **imgbytes)
{
	size_t hsize, size;
	int    err;

	size = pixf_get_size(info,&hsize);

	#ifdef DEBUG
		fprintf(stderr,"%s: hsize = %lu bytes\n", __func__, (unsigned long)hsize);
		fprintf(stderr,"%s: size = %lu bytes\n", __func__, (unsigned long)size);
		fprintf(stderr,"%s: %lu bps\n", __func__, (unsigned long)info->bps);
		fprintf(stderr,"%s: %lu bpp\n", __func__, (unsigned long)info->bpp);
	#endif

	*imgbytes = malloc(size);

	if (*imgbytes == NULL) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose) fprintf(stderr, "%s: error allocating memory!\n", __func__);
		#endif
		return PIXF_ERR_MEM;
	}

	if (!info->ascii)
		err = read_raw(info, *imgbytes, hsize);
	else {
		#ifdef DEBUG
			fprintf(stderr, "%s: reading ascii file\n", __func__);
		#endif

		err = read_ascii(info, *imgbytes);
	}

	if (err != PIXF_ERR_OK) return err;

	return PIXF_ERR_OK;
}

static int write_ascii(PIXF_Info *info,const void *imgbytes)
{
	int    err;
	size_t i,j,n=0;
	size_t size;

	/* 1 bit per pixel */
	if (info->bps==1 && info->bpp==1) {
		const uint8_t *bptr=(const uint8_t *)imgbytes;

		size=info->width*info->height;

		for (j=0;j<info->height;j++) {
			uint8_t bit=0x80;
			for (i=0;i<info->width;i++) {
				if (((*bptr)&bit)!=0)
					err=putc('0',info->f);
				else
					err=putc('1',info->f);

				if (err==EOF) {
					#ifdef PIXF_ENABLE_VERBOSE
						if (pixf_verbose)
							fprintf(stderr, "%s: error writing image data to file\n",
								__func__);
					#endif
					return PIXF_ERR_WRITE;
				}

				if (bit==1) {
					bit=0x80;
					bptr++;
				} else bit>>=1;

				if (n!=0 && ((n+1)%N_PER_LINE_1)==0) {
					err=putc('\n',info->f);

					if (err==EOF) {
						#ifdef PIXF_ENABLE_VERBOSE
							if (pixf_verbose)
								fprintf(stderr, "%s: error writing image data to file\n",
									__func__);
						#endif
						return PIXF_ERR_WRITE;
					}
				}
				n++;
			}
			if (bit!=0x80) bptr++;
		}

		return PIXF_ERR_OK;
	}

	/* 16 bits per sample */
	if (info->maxval>255) {
		const uint16_t *bptr=(const uint16_t *)imgbytes;

		size=info->width*info->height;

		/* RGB image */
		if (info->bps!=info->bpp) {
			size*=3;
		}

		for (i=0;i<size;i++) {
			err=fprintf(info->f,"%u ",(unsigned)*bptr);

			if (err<0) {
				#ifdef PIXF_ENABLE_VERBOSE
					if (pixf_verbose)
						fprintf(stderr, "%s: error writing image data to file\n",
							__func__);
				#endif
				return PIXF_ERR_WRITE;
			}

			if (n!=0 && ((n+1)%N_PER_LINE_16)==0) {
				err=putc('\n',info->f);
				if (err==EOF) {
					#ifdef PIXF_ENABLE_VERBOSE
						if (pixf_verbose)
							fprintf(stderr, "%s: error writing image data to file\n",
								__func__);
					#endif
					return PIXF_ERR_WRITE;
				}
			}

			n++;bptr++;
		}

		return PIXF_ERR_OK;
	}

	/* 8 bits per sample */

	{
		const uint8_t *bptr=(const uint8_t *)imgbytes;

		size=info->width*info->height;

		/* RGB image */
		if (info->bps!=info->bpp) {
			size*=3;
		}

		for (i=0;i<size;i++) {
			err=fprintf(info->f,"%u ",(unsigned)*bptr);

			if (err<0) {
				#ifdef PIXF_ENABLE_VERBOSE
					if (pixf_verbose)
						fprintf(stderr, "%s: error writing image data to file\n",
							__func__);
				#endif
				return PIXF_ERR_WRITE;
			}

			if (n!=0 && ((n+1)%N_PER_LINE_8)==0) {
				err=putc('\n',info->f);
				if (err==EOF) {
					#ifdef PIXF_ENABLE_VERBOSE
						if (pixf_verbose)
							fprintf(stderr, "%s: error writing image data to file\n",
								__func__);
					#endif
					return PIXF_ERR_WRITE;
				}
			}

			n++;bptr++;
		}
	}

	return PIXF_ERR_OK;
}

int pixf_write_file(PIXF_Info *info,const void *imgbytes)
{
	int        err,swap=0;
	size_t     hsize,i,n;
	const char *bptr=(const char *)imgbytes;

	/* write MAGIC - image type */
	if (info->bps==1) {
		/* 1-bit image */
		if (info->bpp!=1) return PIXF_ERR_PPM;
		#ifdef DEBUG
			fprintf(stderr, "%s: writing as bitmap (1-bpp)\n", __func__);
		#endif
		if (info->ascii)
			err=fprintf(info->f,"%s\n",FORMAT_MAGICS[PF_ABITS]);
		else
			err=fprintf(info->f,"%s\n",FORMAT_MAGICS[PF_BITS]);

		if (err<0) {
			#ifdef PIXF_ENABLE_VERBOSE
				if (pixf_verbose)
					fprintf(stderr, "%s: error writing to PBM file: %s\n",
						__func__, strerror(errno));
			#endif
			return PIXF_ERR_WRITE;
		}

		hsize=(info->width+7)/8;
	}
	/* grayscale or RGB image */
	else {
		/* grayscale image */
		if (info->bpp==info->bps) {
			#ifdef DEBUG
				fprintf(stderr, "%s: writing as grayscale image\n", __func__);
			#endif
			if (info->ascii)
				err=fprintf(info->f,"%s\n",FORMAT_MAGICS[PF_AGRAY]);
			else
				err=fprintf(info->f,"%s\n",FORMAT_MAGICS[PF_GRAY]);

			if (err<0) {
				#ifdef PIXF_ENABLE_VERBOSE
					if (pixf_verbose)
						fprintf(stderr, "%s: error writing to PGM file: %s",
							__func__, strerror(errno));
				#endif
				return PIXF_ERR_WRITE;
			}

			if (info->bps<=8)
				hsize=info->width;
			else {
				hsize=info->width*2;
				#if __BYTE_ORDER==__LITTLE_ENDIAN
					swap=1;
				#endif
				#ifdef DEBUG
					fprintf(stderr,"%s: image has 16 bits per sample\n", __func__);
				#endif
			}
		}
		/* RGB image */
		else {
			/* RGB image */
			#ifdef DEBUG
				fprintf(stderr, "%s: writing as RGB image\n", __func__);
			#endif
			if (info->ascii)
				err=fprintf(info->f,"%s\n",FORMAT_MAGICS[PF_ARGB]);
			else
				err=fprintf(info->f,"%s\n",FORMAT_MAGICS[PF_RGB]);

			if (err<0) {
				#ifdef PIXF_ENABLE_VERBOSE
					if (pixf_verbose)
						fprintf(stderr, "%s: error writing to PPM file: %s",
							__func__, strerror(errno));
				#endif
				return PIXF_ERR_WRITE;
			}

			if (info->bps<=8)
				hsize=info->width*3;
			else {
				hsize=info->width*6;
				#if __BYTE_ORDER==__LITTLE_ENDIAN
					swap=1;
				#endif

				#ifdef DEBUG
					fprintf(stderr,"%s: image has 16 bits per sample\n", __func__);
				#endif
			}	/* bps>8 */
		} /* RGB image */
	} /* grayscale or RGB */

	#ifdef DEBUG
		fprintf(stderr,"%s: width: %lu px\n", __func__, (unsigned long)info->width);
		fprintf(stderr,"%s: height: %lu px\n", __func__, (unsigned long)info->height);
		fprintf(stderr,"%s: maxval: %u\n", __func__, (unsigned)info->maxval);
		fprintf(stderr,"%s: hsize: %lu bytes\n", __func__, (unsigned long)hsize);
	#endif

	/* write width and height */
	err = fprintf(info->f,"%lu %lu\n", (unsigned long)info->width,
		(unsigned long)info->height);

	if (err<0) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: error writing image dimensions to the file: %s\n",
					__func__, strerror(errno));
		#endif
		return PIXF_ERR_WRITE;
	}

	/* write Maxval, but only for grayscale and RGB images */
	if (info->bps>1) {
		err = fprintf(info->f, "%u\n", (unsigned)info->maxval);
		if (err<0) {
			#ifdef PIXF_ENABLE_VERBOSE
				if (pixf_verbose)
					fprintf(stderr, "%s: error writing maxval to the file: %s\n",
						__func__, strerror(errno));
			#endif
			return PIXF_ERR_WRITE;
		}
	}

	if (info->ascii) {
		write_ascii(info,imgbytes);
	} else {
		for (i=0;i<info->height;i++) {

			if (swap) swap_b((void *)bptr,hsize);

			if (info->bpp==1) neg((void *)bptr,hsize);

			n=fwrite(bptr,1,hsize,info->f);

			/* on 1bpp image, negate data back, so maintain buffer "unchanged" */
			if (info->bpp==1) neg((void *)bptr,hsize);

			if (n!=hsize) {
				#ifdef PIXF_ENABLE_VERBOSE
					if (pixf_verbose)
						fprintf(stderr, "%s: error writing image data to the file: %s\n",
							__func__, strerror(errno));
				#endif
				return PIXF_ERR_WRITE;
			}

			/* maintain buffer "unchanged" - swap data back to little endian */
			if (swap) swap_b((void *)bptr,hsize);

			bptr+=hsize;
		}
	}

	return PIXF_ERR_OK;
}

int pixf_write(const char *filename, PIXF_Info *info, const void *imgbytes)
{
	info->f=fopen(filename,"wb");

	if (info->f==NULL) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr, "%s: error opening file \"%s\" for writing: %s\n",
					__func__, filename, strerror(errno));
		#endif
		return PIXF_ERR_OPEN;
	}

	return pixf_write_file(info,imgbytes);

}

static int gamma_table(uint16_t maxval,double gamma,uint16_t **table)
{
	size_t i;

	(*table)=(uint16_t *)malloc((maxval+1)*sizeof(uint16_t));

	if (table==NULL) return PIXF_ERR_MEM;

	for (i=0;i<=maxval;i++) {
		double d=pow((double)i/(double)maxval,gamma)*(double)maxval;
		(*table)[i]=(uint16_t)d;
	}

	return PIXF_ERR_OK;
}

int pixf_gamma(void *imgbytes,uint16_t maxval,double gamma,size_t size)
{
	uint16_t *table;
	size_t   i;
	int      err;

	#ifdef DEBUG
		fprintf(stderr,"%s: creating gamma table\n", __func__);
	#endif

	if ((err=gamma_table(maxval,gamma,&table))!=PIXF_ERR_OK) return err;

	/* 1bpp image is not supported */
	if (maxval<=1) {
		#ifdef PIXF_ENABLE_VERBOSE
			if (pixf_verbose)
				fprintf(stderr,"%s: cannot process 1 bit per pixel image!\n", __func__);
		#endif
		return PIXF_ERR_PPM;
	}

	if (maxval<=255) {
		#ifdef DEBUG
			fprintf(stderr,"%s: processing 8-bit image\n", __func__);
		#endif

		uint8_t *dptr=(uint8_t *)imgbytes;

		for (i=0;i<size;i++) {
			*dptr=table[*dptr];
			dptr++;
		}
	} else {
		#ifdef DEBUG
			fprintf(stderr,"%s: processing 16-bit image\n", __func__);
		#endif

		uint16_t *dptr=(uint16_t *)imgbytes;
		for (i=0;i<size;i++) {
			*dptr=table[*dptr];
			dptr++;
		}
	}

	free(table);

	return PIXF_ERR_OK;
}
