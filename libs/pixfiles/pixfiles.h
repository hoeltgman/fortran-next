/* This file is part of libpixfiles.

libpixfiles is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

libpixfiles is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libpixfiles.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __PIXFILES_H

#define __PIXFILES_H

#include <stdio.h>
#include <stdint.h>

/** @file
 *  @brief libpixfiles API header file
 */

/** @name Error codes */

/** @{ */

/** @brief No error, everything went ok. */
#define PIXF_ERR_OK 0
/** @brief Error allocating memory. */
#define PIXF_ERR_MEM 1
/** @brief Error opening file. */
#define PIXF_ERR_OPEN 2
/** @brief Error reading from file. */
#define PIXF_ERR_READ 3
/** @brief Error in PNM file (bad format, corrupted file). */
#define PIXF_ERR_PPM 4
/** @brief Error writing to file. */
#define PIXF_ERR_WRITE 5

/** @} */

/** @brief Structure with information about image file. */
typedef struct {
	size_t width;   /**< @brief Width of the image in pixels. */
	size_t height;  /**< @brief Height of the image in pixels. */
	size_t maxval;  /**< @brief Maximal sample value, e.g. 255 for 8 bits per sample. */
	size_t bps;     /**< @brief Bits per sample, 1 to 16. */
	size_t bpp;     /**< @brief Bits per pixel, 1 to 48. */
	int    ascii;   /**< @brief Whether the file was read or should be written in ascii. */

	FILE   *f;      /**< @brief Handle to opened file. */
} PIXF_Info;

/** @name
 *  Constants for gamma adjustments
 */

/** @{ */

/** @brief Gamma value used for encoding image to PNM file with pixf_gamma. */
#define PIXF_GAMMA_ENCODE 1.0/2.2
/** @brief Gamma value used for decoding image from PNM file with pixf_gamma. */
#define PIXF_GAMMA_DECODE 2.2

/** @} */

#ifdef PIXF_ENABLE_VERBOSE
/** @brief Whether to print error messages from this library to stderr.
 *  @details Set this to nonzero, if you want to print error messages from
 * library to stderr
 */
	extern int pixf_verbose;
#endif

/** @brief Opens PNM file for reading.
 *  @param[in] fname Name of the file to open.
 *  @param[in] info Pointer to the (uninitialized) PIXF_Info structure.
 *  @return PIXF_ERR_OK on success, or any other code on fail.
 */
int pixf_open(const char *fname, PIXF_Info *info);

/** @brief Read image data from opened PNM file.
 *  @param[in] info PIXF_Info with filled information about file
 *  (with pixf_open).
 *  @param[out] imgbytes poiter to (void *) - this function will allocate
 *  memory automatically.
 *  @return PIXF_ERR_OK on success, or any other code on fail.
 */
int pixf_read(const PIXF_Info *info, void **imgbytes);

/** @brief Write image to the PNM file.
 *  @param[in] filename Name of the file to write image to.
 *  @param[in] info Pointer to the initialized PIXF_Info structure -
 *  by pixf_open, or manually.
 *  @param[in] imgbytes Poiter to the RAW image data.
 *  @return PIXF_ERR_OK on success, or any other code on fail.
 *  @note In manual initialization of PIXF_Info, you should set width, height,
 *  maxval, bps, bpp and ascii members of PIXF_Info structure to proper values.
 *  @note  Image samples should be padded to 8 bits (for maxval <= 255), or
 *  to 16 bits (maxval > 255 and maxval <= 65535). For RGB images, there are
 *  three samples per pixel - in R, G, B order.
 */
int pixf_write(const char *filename, PIXF_Info *info, const void *imgbytes);

/****************************************************************************/

/** @brief Gamma transfer function.
 *  @param[in,out] imgbytes RAW image data.
 *  @param[in] maxval Maxval, i.e. 2^(bps) - 1.
 *  @param[in] gamma Gamma transfer constant.
 *  @param[in] size Image data size.
 *  @return PIXF_ERR_OK if everything went ok
 *  @return PIXF_ERR_MEM on memory allocation error
 *  @return PIXF_ERR_PPM on 1bpp image (this can't be transferred with gamma
 *  function).
 *  @note Use PIXF_GAMMA_ENCODE for linear-to-BT.709 encoding and
 *  PIXF_GAMMA_DECODE if you want to convert samples from loaded image to
 *  the linear ones.
 *  @note Size is width * height * samples_per_color of the image, e.g.
 *  width * height for grayscale images, width * height * 3 for RGB images.
 *  @note As in pixf_write, if maxval<=255, image has to be 8bps, else
 *  the image has to be 16bps.
 */
int pixf_gamma(void *imgbytes, uint16_t maxval, double gamma, size_t size);

/****************************************************************************/

/** @name
 *  Functions on already opened files.
 */

/** @{ */

/** @brief Read PNM header.
 *  @param[in,out] info PIXF_Info structure which should be filled with
 *  information about image.
 *  @return PIXF_ERR_OK on success, or any other code on fail.
 *  @note You should set info->f to the handle of opened file before calling
 *  this function.
 */
int pixf_read_info(PIXF_Info *info);

/** @brief Write image data to the file.
 *  @param[in] info PIXF_Info structure with all information set.
 *  @param[in] imgbytes RAW image data to write to file.
 *  @return PIXF_ERR_OK on success, or any other code on fail.
 *  @note Image data will be written to the opened file using handle info->f.
 */
int pixf_write_file(PIXF_Info *info, const void *imgbytes);

/** @} */

/****************************************************************************/

/** @brief Compute the size of an image (in bytes).
 * 	@param[in] info Pointer to the initialized PIXF_Info structure.
 *  @param[out] pitch Pointer to the variable where the size of one line of the
 *  image will be stored. Can be NULL.
 *  @note At least width, height, bps and bpp should be set in PIXF_Info
 *  before calling of this function.
 */
size_t pixf_get_size(const PIXF_Info *info, size_t *pitch);

#endif
