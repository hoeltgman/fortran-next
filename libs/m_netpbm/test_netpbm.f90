program test_netpbm
  !!****p* src/utils/test_netpbm
  !!  NAME
  !!    test_netpbm  --  sample output from m_netpbm
  !!  SYNOPSIS
  !!    $./test_netpbm
  !!  DESCRIPTION
  !!    Test writing a binary pbm files.
  !!    Executing it without command line options will create two files in the
  !!    directory where it was invoked, 'out_bw.ppm' and 'out_gs.ppm'.
  !!    Both can be displayed with ImageMagick's 'display' command and in
  !!    addition, using 'animate', all frames inside 'out_gs.ppm' can be viewed
  !!    as a movie.
  !!  OPTIONS
  !!    See ./test_netpbm --help
  !!  USES
  !!    src/utils/m_option_parser
  !!    src/utils/m_netpbm
  !!  MODIFICATION HISTORY
  !!    2008-03-15 -- WvH : removed unused variables
  !!    2008-04-17 -- WvH : rewrote m_netpbm to be more OO
  !!  SEE ALSO
  !!    src/utils/m_netpbm
  !!  COPYRIGHT
  !!    Copyright (c) 2008 Wim Van Hoydonck
  !!  AUTHOR
  !!    Wim Van Hoydonck
  !!  CREATION DATE
  !!    2008-02-08
  !!  SOURCE
  use m_option_parser
  use m_netpbm
  
  implicit none
  
  type(option_t) , allocatable  :: opts(:)
  integer                       :: width , height , minwh
  logical                       :: help , is_movie , w_in_cl , h_in_cl
  
  integer , allocatable         :: gs_data(:,:) , bw_data(:,:) , rgb_data(:,:,:)
  real    , allocatable         :: gs_data_real(:,:) , rgb_data_real(:,:,:)
  integer                       :: ierr , i
  
  
  type(netpbm_t)                :: img_gs , img_rgb , img_bw , mov_gs , mov_rgb
  
  
  !==========================!
  ! command line option part !
  !==========================!
  
  allocate ( opts(4) )
  
  call set_option ( opts , "--width"  , "-W" , 100     , "set the file width in pixels"  )
  call set_option ( opts , "--height" , "-H" , 100     , "set the file heigth in pixels" )
  call set_option ( opts , "--help"   , "-h" , .false. , "print help message to screen and quit" )
  call set_option ( opts , "--movie"  , "-m" , .false. , "write a movie" )
  
  call parse_options ( opts )
  
  call get_option_value ( opts , "-W" , width  , w_in_cl )
  call get_option_value ( opts , "-H" , height , h_in_cl )
  call get_option_value ( opts , "-h" , help  )
  call get_option_value ( opts , "-m" , is_movie )

  if (help) then
    call print_help_message ( opts )
    stop
  end if
  
  print *, w_in_cl , h_in_cl
  ! check that width and height are the same here
  if ( w_in_cl .and. .not. h_in_cl ) height = width
  if ( h_in_cl .and. .not. w_in_cl ) width  = height
  if ( w_in_cl .and. h_in_cl ) then
    minwh = maxval( [width, height] )
    width = minwh
    height = minwh
  end if
  
  ! initializations
  call init_netpbm ( "bw_data_test.ppm"  , img_bw  , ierr , write_bw = .true. , write_movie = is_movie , fix_file_ext = .true. )
  if ( ierr /= 0) stop
  
  call init_netpbm ( "gs_data_test.ppm"  , img_gs  , ierr , write_gs = .true. , write_movie = is_movie , fix_file_ext = .true. )
  if ( ierr /= 0) stop
  
  call init_netpbm ( "rgb_data_test.ppm" , img_rgb , ierr , write_rgb = .true. , write_movie = is_movie , fix_file_ext = .true. )
  if ( ierr /= 0) stop
  
  call init_netpbm ( "gs_movie_test.ppm" , mov_gs  , ierr , write_gs = .true. , write_movie = .true. , fix_file_ext = .true. )
  if ( ierr /= 0) stop
  
  call init_netpbm ( "rgb_movie_test.ppm" , mov_rgb  , ierr , write_rgb = .true. , write_movie = .true. , fix_file_ext = .true. )
  if ( ierr /= 0) stop
  
  
  ! allocate arrays
  allocate ( gs_data(height,width) , gs_data_real(height,width) , bw_data(height,width) , &
            rgb_data_real(height,width,3) , rgb_data(height,width,3) )
  
  ! grayscale random data
  call random_number ( harvest = gs_data_real )
  
  gs_data = int( gs_data_real * 255.0 )
  
  ! monochrome vertical stripes
  bw_data = 0
  forall( i = 1:width:6 ) bw_data(:,i:i+2) = 1
  
  ! colour random data
  call random_number ( harvest = rgb_data_real )
  rgb_data = int( rgb_data_real * 255.0 )
  
  
  ! write data
  call write_netpbm ( img_bw , bw_data )
  call write_netpbm ( img_gs , gs_data )
  !call write_netpbm ( img_rgb , rgb_data )
  call write_netpbm ( img_rgb , rgb_data )
  
  
  ! grayscale movie
  do i = 1 , width
    
    call random_number ( harvest = gs_data_real )
    call random_number ( harvest = rgb_data_real )
    
    gs_data   = int( gs_data_real * 255 )
    rgb_data  = int( rgb_data_real * 255 )
    
    if ( i <= width-3 ) then
      rgb_data(:,i:i+3,1)     = 255
      rgb_data(:,i:i+3,2:3)   = 0
    else
      rgb_data(:,i:width,1)   = 255
      rgb_data(:,i:width,2:3) = 0
    end if
    
    if ( i == width ) then
      call write_netpbm ( mov_gs ,  gs_data ,  close_file = .true. )
      call write_netpbm ( mov_rgb , rgb_data , close_file = .true. )
    else
      call write_netpbm ( mov_gs ,  gs_data )
      call write_netpbm ( mov_rgb , rgb_data )
    end if
    
  end do
  !!******
  
end program test_netpbm

