module m_netpbm
  !!****h* src/utils/m_netpbm
  !!  NAME
  !!    m_netpbm  --  netpbm output
  !!  SYNOPSIS
  !!    use m_netpbm
  !!  DESCRIPTION
  !!    Output netpbm images in binary format.
  !!    Three formats are supported:
  !!      * pbm  (monochrome, b&w)
  !!      * pgm  (grayscale)
  !!      * ppm  (full colour)
  !!    These can write the files with values that fit in a single byte (or
  !!    less). 
  !!    No attempt is done to add the proper extension to the file. The data 
  !!    is written to a file with file_name as supplied by the user.
  !!    
  !!    It contains two public subroutine: init_netpbm and write_netpbm
  !!    
  !!    With the Intel fortran compiler, this file must be compiled with:
  !!    -heap-arrays 0
  !!    otherwise, it might give a segfault when using large arrays.
  !!    Or, the stack size of the shell must be increased (ulimit -s unlimited)
  !!    
  !!  MODIFICATION HISTORY
  !!    2008-02-06 -- WvH : Started messing with pgm and ppm output.
  !!    2008-02-08 -- WvH : Got binary output for pgm working.
  !!    2008-02-09 -- WvH : Complete rewrite. Only output is binary and only for
  !!                        grayscale and full colour.
  !!                        Uncommented all old subroutines (these are candidates
  !!                        for complete removal)
  !!    2008-02-10 -- WvH : Got help on the intel forums for the segfault with
  !!                        large arrays.
  !!                        Some optimizations (less array reshapes)
  !!    2008-02-14 -- WvH : Monochrome output works to
  !!    2008-04-17 -- WvH : Rewrote this module, per-file data is stored in a
  !!                        derived type, which ensures that no data must be
  !!                        saved in the module itself.
  !!  COPYRIGHT
  !!    Copyright (c) 2008 Wim Van Hoydonck
  !!  AUTHOR
  !!    Wim Van Hoydonck
  !!  CREATION DATE
  !!    2008-02-06
  !!  SOURCE
  implicit none
  
  private
  
  type :: netpbm_t
    integer                 :: rows , cols
    character(len=128)      :: file_name
    integer                 :: file_unit
    character(len=4)        :: file_ext
    integer                 :: max_val
    character(len=2)        :: netpbm_id
    logical                 :: write_rgb , write_gs , write_bw
    logical                 :: write_movie
    logical                 :: init
    logical                 :: file_closed
  end type netpbm_t
  !!******
  
  interface write_netpbm
      !!****s* src/utils/m_netpbm/write_netpbm
      !!  NAME
      !!    write_netpbm  --  write monochrome, grayscale or full colour image
      !!  SYNOPSIS
      !!    call write_netpbm ( image , gs_bw_data , close_file )
      !!    call write_netpbm ( image , rgb_data , close_file )
      !!  DESCRIPTION
      !!    This is the public interface procedure to use when writing image
      !!    data to a file.
      !!  INPUTS
      !!    type(netpbm_t)      :: image
      !!    integer             :: gs_bw_data(:,:)
      !!    integer             :: rgb_data(:,:,:)
      !!    logical , optional  :: close_file
      !!  SEE ALSO
      !!    src/utils/m_netpbm/write_netpbm_gs_bw
      !!    src/utils/m_netpbm/write_netpbm_rgb
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-04-17
      !!  SOURCE
    module procedure write_netpbm_gs_bw , &
                     write_netpbm_rgb
      !!******
  end interface write_netpbm
  
  
  public :: init_netpbm , write_netpbm , netpbm_t
  
  contains
    
    subroutine init_netpbm ( file_name , image , ierr , write_bw , write_gs , write_rgb , &
                             write_movie , fix_file_ext )
      !!****s* src/utils/m_netpbm/init_netpbm
      !!  NAME
      !!    init_netpbm  --  fill derived type holding header info
      !!  SYNOPSIS
      !!    One of:
      !!      call init_netpbm ( file_name , image , ierr , write_bw , write_movie , fix_file_ext )
      !!      call init_netpbm ( file_name , image , ierr , write_gs , write_movie , fix_file_ext )
      !!      call init_netpbm ( file_name , image , ierr , write_rgb , write_movie , fix_file_ext )
      !!    where 'write_movie' and 'fix_file_ext' are optional
      !!  DESCRIPTION
      !!    Assign file_name to the respective file_name field of netpbm_t, 
      !!    and get a free file unit to write the image data to.
      !!    Sets some logicals to proper values.
      !!    Fix_file_ext ensures that the output file has the proper filename
      !!    extension for the kind of data to write.
      !!  INPUTS
      !!    character(len=*)    :: file_name
      !!    type(netpbm_t)      :: image
      !!    logical , optional  :: write_bw , write_gs , write_rgb
      !!    logical , optional  :: write_movie
      !!    logical , optional  :: fix_file_ext
      !!  OUTPUT
      !!    type(netpbm_t)      :: image
      !!    integer             :: ierr
      !!  USES
      !!    src/utils/m_sys_fun
      !!  MODIFICATION HISTORY
      !!  SEE ALSO
      !!    src/utils/m_netpbm/write_netpbm
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-04-15
      !!  SOURCE
      use m_sys_fun, only: get_free_file_unit , string_split
      
      implicit none
      
      character(len=*)    , intent(in)    :: file_name
      type(netpbm_t)      , intent(inout) :: image
      integer             , intent(out)   :: ierr
      logical , optional  , intent(in)    :: write_bw , write_gs , write_rgb
      logical , optional  , intent(in)    :: write_movie
      logical , optional  , intent(in)    :: fix_file_ext
      
      integer                             :: ios
      logical                             :: fix_extension
      character(len=len(image % file_name)) , allocatable :: split_file_name(:)
      
      
      ! initialize some fields of the derived type
      image % write_rgb       = .false.
      image % write_gs        = .false.
      image % write_bw        = .false.
      image % init            = .false.
      image % file_closed     = .true.
      
      ! set netpbm_t file_name to supplied file_name
      image % file_name       = trim( file_name )
      ierr                    = 0
      
      
      !=============!
      ! check input !
      !=============!
      
      ! see what kind of file we have to write
      if ( present(write_rgb) .and. .not. present(write_gs)  .and. .not. present(write_bw) )  image % write_rgb = .true.
      if ( present(write_gs)  .and. .not. present(write_rgb) .and. .not. present(write_bw) )  image % write_gs  = .true.
      if ( present(write_bw)  .and. .not. present(write_rgb) .and. .not. present(write_gs) )  image % write_bw  = .true.
      
      ! if two or more of the above three options were given, do nothing
      if ( .not. image % write_rgb .and. .not. image % write_gs .and. .not. image % write_bw ) then
        write(*,*) "write_pbm: incorrect number of arrays supplied, exactly 1 needed"
        ierr = 1
        return
      end if
      
      ! movie-related parameters
      if ( present(write_movie) ) then
        if ( write_movie )      image % write_movie     = .true.
      end if
      
      ! fix extension of supplied filename
      fix_extension = .false.
      if ( present( fix_file_ext ) ) then
        if ( fix_file_ext ) fix_extension = .true.    
      end if
      
      
      ! set netpbm_id, filename extension and maximum value of pixels
      if ( image % write_rgb ) then     ! full colour
        image % netpbm_id = "P6"
        image % max_val   = 255
        image % file_ext  = ".ppm"
      elseif ( image % write_gs  ) then ! grayscale
        image % netpbm_id = "P5"
        image % max_val   = 255
        image % file_ext  = ".pgm"
      else                              ! monochrome
        image % netpbm_id = "P4"
        image % max_val   = 1
        image % file_ext  = ".pbm"
      end if
      
      ! fix file extension if asked for
      if ( fix_extension ) then
        
        ! split filename in two parts, if two parts are returned, last part contains extension
        call string_split( image % file_name , split_file_name , "." , max_split = 1 , back = .true. )
        
        if ( size( split_file_name ) > 1 ) then
          image % file_name = trim( split_file_name(2)(:) ) // image % file_ext
        else
          ! no separator found
          image % file_name = trim( split_file_name(1)(:) ) // image % file_ext
        end if
        
      end if
      
      
      !====================================================!
      ! get a free file unit and open the file for writing !
      !====================================================!
      
      ! get a file unit
      call get_free_file_unit ( image % file_unit )
      if ( image % file_unit == 0 ) then
        write(*,*) "write_pbm: unable to get a free file unit"
        ierr = 2
        return
      end if
      
      
      ! open file in unformatted stream mode
      open ( unit = image % file_unit , file = trim(image % file_name) , status = "replace" , & 
             form = "unformatted" , access = "stream" , iostat = ios )
      
      ! is file open?
      if ( ios /= 0 ) then
        write(*,*) "write_pbm: could not open file"
        ierr = 3
        return
      end if
      
      image % init        = .true.
      image % file_closed = .false.
      !!******
      
    end subroutine init_netpbm
    
    
    subroutine write_netpbm_gs_bw ( image , gs_bw_data , close_file )
      !!***is* src/utils/m_netpbm/write_netpbm_gs_bw
      !!  NAME
      !!    write_netpbm_gs_bw  --  write grayscale or monochrome data to a file
      !!  SYNOPSIS
      !!    call write_netpbm_gs_bw ( image , gs_bw_data , close_file )
      !!  DESCRIPTION
      !!    This is an internal subroutine that should not be used from outside
      !!    this module. Use the generic interface 'write_netpbm' instead.
      !!    It writes the data in array gs_bw_data to the file named 
      !!    image % file_name.
      !!  INPUTS
      !!    type(netpbm_t)      :: image
      !!    integer             :: gs_bw_data(:,:)
      !!    logical , optional  :: close_file
      !!  SEE ALSO
      !!    src/utils/m_netpbm/write_netpbm
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-04-17
      !!  SOURCE
      implicit none
      
      type(netpbm_t)     , intent(inout)  :: image
      integer            , intent(in)     :: gs_bw_data(:,:)
      logical , optional , intent(in)     :: close_file
      
      integer(1) , allocatable            :: gs_bw_data_out(:,:)
      logical                             :: close_file_unit
      
      
      close_file_unit = .false.
      if ( present( close_file ) ) then
        if ( close_file ) close_file_unit = .true.
      end if
      
      ! check that file was not already closed
      if ( image % file_closed ) then
        write(*,*) "ERROR : could not write image to file as it was closed "
        write(*,*) "        in a previous call to write_netpbm"
        return
      end if
      
      ! image put into init_netpbm and we're not writing a full colour image
      if ( image % init .and. .not. image % write_rgb ) then
        
        ! get image size
        image % rows  = size( gs_bw_data , dim=1 )
        image % cols  = size( gs_bw_data , dim=2 )
        
        allocate ( gs_bw_data_out( image % cols , image % rows ) )
        
        
        ! write file header
        call write_netpbm_header ( image )
        
        
        ! check data
        if ( image % write_gs ) then
          
          call check_gs_data ( gs_bw_data , image , gs_bw_data_out )
          
        elseif ( image % write_bw ) then
          
          call check_bw_data ( gs_bw_data , image , gs_bw_data_out )
          
        end if
        
        ! write file body
        write( image % file_unit ) gs_bw_data_out
        deallocate ( gs_bw_data_out )
        
        
        ! close file if necessary
        if ( ( .not. image % write_movie ) .or. ( image % write_movie .and.  close_file_unit ) ) then
          close( image % file_unit )
          image % file_closed = .true.
        end if
      end if
      !!******
    
    end subroutine write_netpbm_gs_bw
    
    
    subroutine write_netpbm_rgb ( image , rgb_data , close_file )
      !!***is* src/utils/m_netpbm/write_netpbm_rgb
      !!  NAME
      !!    write_netpbm_rgb  --  write full colour image data to a file
      !!  SYNOPSIS
      !!    call write_netpbm_rgb ( image , rgb_data , close_file )
      !!  DESCRIPTION
      !!    This is an internal subroutine that should not be used from outside
      !!    this module. Use the generic interface 'write_netpbm' instead.
      !!    It writes the data in array rgb_data to the file named 
      !!    image % file_name.
      !!  INPUTS
      !!    type(netpbm_t)      :: image
      !!    integer             :: rgb_data(:,:,:)
      !!    logical , optional  :: close_file
      !!  SEE ALSO
      !!    src/utils/m_netpbm/write_netpbm
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-04-17
      !!  SOURCE
      implicit none
      
      type(netpbm_t)     , intent(inout)  :: image
      integer            , intent(in)     :: rgb_data(:,:,:)
      logical , optional , intent(in)     :: close_file
      
      integer(1) , allocatable            :: rgb_data_out(:,:,:)
      logical                             :: close_file_unit
      
      
      close_file_unit = .false.
      if ( present( close_file ) ) then
        if ( close_file ) close_file_unit = .true.
      end if
      
      ! check that file was not already closed
      if ( image % file_closed ) then
        write(*,*) "ERROR : could not write image data to file '", trim(image % file_name) , "'"
        write(*,*) "        as it was closed in a previous call to write_netpbm"
        return
      end if
      
      ! image put into init_netpbm and we're not writing a grayscale or monochrome image
      if ( image % init .and. .not. image % write_gs .and. .not. image % write_bw ) then
        
        ! get image size
        image % rows  = size( rgb_data , dim=1 )
        image % cols  = size( rgb_data , dim=2 )
        
        allocate ( rgb_data_out( 3 , image % cols , image % rows ) )
        
        ! write file header
        call write_netpbm_header ( image )
        
        ! check data
        call check_rgb_data ( rgb_data(:,:,:) , image , rgb_data_out(:,:,:) )
        
        ! write file body
        write( image % file_unit ) rgb_data_out
        deallocate ( rgb_data_out )
        
        ! close file if necessary
        if ( ( .not. image % write_movie ) .or. ( image % write_movie .and.  close_file_unit ) ) then
          close( image % file_unit )
          image % file_closed = .true.
        end if
      
      end if
      !!******
      
    end subroutine write_netpbm_rgb
    
    
    subroutine check_bw_data ( bw , image , bw_out )
      !!***is* src/utils/m_netpbm/check_bw_data
      !!  NAME
      !!    check_bw_data  --  prepare monochrome data for writing
      !!  SYNOPSIS
      !!    call check_bw_data ( bw , image , bw_out )
      !!  DESCRIPTION
      !!    Check and prepare monochrome data for binary stream writing
      !!  INPUTS
      !!    integer          :: bw(:,:)
      !!    type(netpbm_t)   :: image
      !!  OUTPUT
      !!    integer(1)       :: bw_out(size(bw,dim=3),size(bw,dim=2),size(bw,dim=1))
      !!  MODIFICATION HISTORY
      !!    2008-04-17 -- WvH : new api
      !!  SEE ALSO
      !!    src/utils/m_netpbm/check_rgb_data
      !!    src/utils/m_netpbm/check_gs_data
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-02-17
      !!  SOURCE
      implicit none
      
      integer          , intent(in)   :: bw(:,:)
      type(netpbm_t)   , intent(in)   :: image
      integer(1)       , intent(out)  :: bw_out(int(ceiling(real(size(bw,dim=2))/8.)),size(bw,dim=1))
      
      integer                         :: bw_arr(size(bw,dim=2),size(bw,dim=1))
      integer                         :: bw_arr1(size(bw,dim=2),size(bw,dim=1))
      integer(2)       , parameter    :: powers2(8) = [128,64,32,16,8,4,2,1]
      integer                         :: i , k , idx
      integer(2)                      :: bwval(8) , seqs
      
      
      ! cols/8, rounded up
      seqs = ceiling(real(image % cols)/8.)
      
      ! local copy
      bw_arr    = bw
      
      ! set values to 0 or 1
      where ( bw_arr < 0 )                bw_arr = 0 
      where ( bw_arr > image % max_val )  bw_arr = image % max_val
      
      ! per row, put values of 8 consecutive pixels in 1 byte
      do i = 1 , image % rows
        idx = 0
        do k = 1 , seqs*8 , 8 
          idx = idx + 1
          ! end of row
          if ( k+7 > image % cols ) then
            bwval(1:image % cols-k+1) = bw_arr(i,k:image % cols)
          else
            bwval(1:8) = bw_arr(i,k:k+7)
          end if
          bw_arr1(i,idx) = dot_product(bwval,powers2)
          bwval = 0
          
        end do  
      end do
      
      ! transpose array
      bw_out = transpose( bw_arr1(:,1:idx) )
      !!******
      
    end subroutine check_bw_data
    
    
    subroutine check_gs_data ( gs , image , gs_out )
      !!***is* src/utils/m_netpbm/check_gs_data
      !!  NAME
      !!    check_gs_data  --  prepare grayscale data for writing
      !!  SYNOPSIS
      !!    call check_gs_data ( gs , image , gs_out )
      !!  DESCRIPTION
      !!    Check and prepare grayscale data for binary stream writing
      !!  INPUTS
      !!    integer          :: gs(:,:)
      !!    type(netpbm_t)   :: image
      !!  OUTPUT
      !!    integer(1)       :: gs_out(size(gs,dim=3),size(gs,dim=2),size(gs,dim=1))
      !!  SEE ALSO
      !!    src/utils/m_netpbm/check_rgb_data
      !!    src/utils/m_netpbm/check_bw_data
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-02-17
      !!  SOURCE
      implicit none
      
      integer          , intent(in)   :: gs(:,:)
      type(netpbm_t)   , intent(in)   :: image
      integer(1)       , intent(out)  :: gs_out(size(gs,dim=2),size(gs,dim=1))
      
      integer                         :: gs_arr(size(gs,dim=2),size(gs,dim=1))
      
      
      ! put contents in correct order (to write row after row, instead of
      ! column after column)
      gs_arr    = gs
      
      ! clamp values between 0 and 255
      where ( gs_arr < 0 )                gs_arr = 0 
      where ( gs_arr > image % max_val )  gs_arr = image % max_val
      
      ! copy data to 1-byte array and switch rows and columns
      gs_out   = transpose( gs_arr )
      !!******
      
    end subroutine check_gs_data
    
    
    subroutine check_rgb_data ( rgb , image , rgb_out )
      !!***is* src/utils/m_netpbm/check_rgb_data
      !!  NAME
      !!    check_rgb_data  --  prepare rgb data for writing
      !!  SYNOPSIS
      !!    call check_rgb_data ( rgb , image , rgb_out )
      !!  DESCRIPTION
      !!    Check and prepare rgb data for binary stream writing
      !!  INPUTS
      !!    integer           :: rgb(:,:,:)
      !!    type(netpbm_t)    :: image
      !!  OUTPUT
      !!    integer(1)        :: rgb_out(size(rgb,dim=3),size(rgb,dim=2),size(rgb,dim=1))
      !!  MODIFICATION HISTORY
      !!    2008-04-17 -- WvH : derived type changes
      !!  SEE ALSO
      !!    src/utils/m_netpbm/check_gs_data
      !!    src/utils/m_netpbm/check_bw_data
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-02-17
      !!  SOURCE
      implicit none
      
      integer          , intent(in)   :: rgb(:,:,:)
      type(netpbm_t)   , intent(in)   :: image
      integer(1)       , intent(out)  :: rgb_out(3,size(rgb,dim=2),size(rgb,dim=1))
      
      integer                         :: rgb_arr(3,size(rgb,dim=2),size(rgb,dim=1))
      
      
      rgb_arr     = reshape( rgb , shape(rgb_arr) , order=[3,2,1] )
      
      ! clamp pixel values to range 0 -> max_rgb
      where ( rgb_arr > image % max_val ) rgb_arr = image % max_val
      where ( rgb_arr < 0 )               rgb_arr = 0
      
      ! copy data to 1-byte array
      rgb_out  = rgb_arr
      !!******
      
    end subroutine check_rgb_data
    
    
    
    subroutine write_netpbm_header ( image )
      !!***is* src/utils/m_netpbm/write_netpbm_header
      !!  NAME
      !!    write_netpbm_header  --  write header of netpbm image
      !!  SYNOPSIS
      !!    call write_netpbm_header ( image )
      !!  DESCRIPTION
      !!    Write the header (length = 20) of the image based on some input
      !!    
      !!    Files can have 99999 columns and rows. The maximum value is 255 (1
      !!    byte)
      !!  INPUTS
      !!    type(netpbm_t)    :: image
      !!  MODIFICATION HISTORY
      !!    2008-02-14 -- WvH : Monochrome output works too
      !!    2008-04-17 -- WvH : derived type changes, name change
      !!  SEE ALSO
      !!    src/utils/m_netpbm/write_netpbm
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-02-09
      !!  SOURCE
      implicit none
      
      type(netpbm_t)    , intent(in)  :: image
      
      character(len=20)               :: header_string
      
      
      header_string      = " "
      header_string(1:2) = image % netpbm_id
      
      write ( header_string(4:8)   , fmt="(I5)" ) image % cols
      write ( header_string(10:14) , fmt="(I5)" ) image % rows
      
      if ( image % netpbm_id /= "P4" ) then
        write ( header_string(16:19) , fmt="(I4)" ) image % max_val
      else
        header_string(15:15) = char(10)
      end if
      ! end header with linefeed
      header_string(20:20) = char(10)
      
      if ( image % netpbm_id /= "P4" ) then
        write ( image % file_unit ) header_string
      else
        write ( image % file_unit ) header_string(1:15)
      end if
      !!******
        
    end subroutine write_netpbm_header
    
end module m_netpbm

