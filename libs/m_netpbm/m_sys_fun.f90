module m_sys_fun
  !!****h* src/utils/m_sys_fun
  !!  NAME
  !!    m_sys_fun  --  some system-related functions
  !!  SYNOPSIS
  !!    use m_sys_fun
  !!  DESCRIPTION
  !!    Group some system-related functionality such as:
  !!    * splitting a path+filename into its path and the filename.
  !!    * determining wether or not the input is a separator (tab or blank)
  !!    * general string splitting into parts, where the separator can be chosen
  !!      freely
  !!    * string manipulations:
  !!      * conversion to upper and lower case
  !!      * reversing a string
  !!    * inquire for a free file unit
  !!    * displaying a progress bar
  !!  MODIFICATION HISTORY
  !!    2007-12-26 -- WvH : String manipulation functions added: string_to_upper,
  !!                        string_to_lower and string_reverse
  !!    2008-03-03 -- WvH : Removed use of m_precision
  !!    2008-04-03 -- WvH : Progress bar functions added, works mostly.
  !!    2009-02-03 -- WvH : Left- (and right) padding function to be added.
  !!  COPYRIGHT
  !!    Copyright (c) 2007-2009 Wim Van Hoydonck
  !!  AUTHOR
  !!    Wim Van Hoydonck
  !!  CREATION DATE
  !!    2007-09-02
  !!******
  implicit none
  
  private
  
  character(len=*) , parameter :: upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  character(len=*) , parameter :: lower = "abcdefghijklmnopqrstuvwxyz"
  
  public :: get_path_and_filename , string_split , string_reverse , & 
            string_to_upper , string_to_lower , string_replace ,    &
            string_remove , find_substring , get_free_file_unit ,   &
            print_progress_bar , clear_progress_bar , finish_progress_bar , &
            determine_eol , string_pad
  
  contains
    
    subroutine get_path_and_filename ( path_filename , pf_out , path_sep )
      !!****s* src/utils/m_sys_fun/get_path_and_filename
      !!  NAME
      !!    get_path_and_filename  --  split a string in its path and filename
      !!  SYNOPSIS
      !!    call get_path_and_filename ( path_filename , pf_out , path_sep )
      !!  DESCRIPTION
      !!    Split the input 'path_filename' in two parts, its path, and a
      !!    filename. If no path separator is given, use '/' as path separator.
      !!    Otherwise, '\' could be used as pathseparator (as on M$ Window$).
      !!    Everything (path and filename) is returned in the character array
      !!    pf_out.
      !!  INPUTS
      !!    character(len=*)              :: path_filename
      !!    character(len=1) , optional   :: path_sep
      !!  OUTPUT
      !!    character(len=len(path_filename)) , optional :: pf_out(:)
      !!  MODIFICATION HISTORY
      !!    2007-09-02 -- WvH : Initial version, without do-loop
      !!    2007-09-03 -- WvH : Now also without if-statement.
      !!    2007-10-07 -- WvH : Removed unused variable
      !!    2007-10-19 -- WvH : Rewrote this one to use string_split
      !!  SEE ALSO
      !!    src/utils/m_sys_fun/string_split
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2007-09-02
      !!  SOURCE
      implicit none
      
      character(len=*)                  , intent(in)             :: path_filename
      character(len=len(path_filename)) , intent(out) , allocatable :: pf_out(:)
      character(len=1)                  , intent(in)  , optional :: path_sep
      
      character           :: ps           ! path separator symbol
      integer             :: lpath        ! path length
      
      ! set internal path separator
      ps = "/"  ! unix default
      if ( present(path_sep) ) ps = path_sep
       
      call string_split ( path_filename , pf_out , ps , max_split=1 , back=.true. )
      
      ! add last separator again, it is removed by string_split
      lpath = len_trim(pf_out(2))
      if (trim(pf_out(2)) /= "") pf_out(2) = trim(pf_out(2))//ps
      !!******
      
    end subroutine get_path_and_filename
    
    
    
    subroutine string_split ( str_in , str_out , sep , max_split , back )
      !!****s* src/utils/m_sys_fun/string_split
      !!  NAME
      !!    string_split  --  split string in substrings based on sep
      !!  SYNOPSIS
      !!    call string_split ( str_in , str_out , sep , max_split , back )
      !!  DESCRIPTION
      !!    Generic string splitter, splits on all (if any) occurences of 'sep' if
      !!    max_split is not given.
      !!    If max_plit is given, string_in is split in max_split parts beginning
      !!    from the left, and if back is given and true, then it starts
      !!    counting at the back.
      !!    
      !!    If sep is not available, sep is assumed to be whitespace (space and
      !!    tab) of any length. In this mode, leading and/or trailing spaces are
      !!    first removed before doing anything else.
      !!  INPUTS
      !!    character(len=*)                            :: str_in
      !!    character(len=*)              , optional    :: sep
      !!    integer                       , optional    :: max_split
      !!    logical                       , optional    :: back
      !!  OUTPUT
      !!    character(len=len(string_in)) , allocatable :: str_out
      !!  MODIFICATION HISTORY
      !!    2007-10-15 -- WvH : Initial version
      !!                        Works, only the 'back' argument needs some work
      !!    2007-10-16 -- WvH : Back works now too, now only splitting on
      !!                        arbitrary-length whitespaces...
      !!    2007-10-17 -- WvH : Fixed bug in when back was true with lsep > 1.
      !!                        Splitting on whitespaces works now too.
      !!                        I was not completely sober when I found/fixed
      !!                        this, so, I can be drunk and fix bugs at the same
      !!                        time :)
      !!    2007-10-18 -- WvH : Fixed bug in whitespace splitting when len(char) == 1
      !!    2007-10-21 -- WvH : Whitespace splitting bug fixed when first or last
      !!                        char was not a blank (how did I miss that??)
      !!                        Increased size of str_cp by 1 to remove some if
      !!                        constructs. This ensures that str_cp always ends
      !!                        in a whitespace.
      !!    2007-12-07 -- WvH : Use cshift instead of shifting stuff by hand
      !!  SEE ALSO
      !!    Python string.split function : http://docs.python.org/lib/string-methods.html
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2007-10-15
      !!  SOURCE
      implicit none
      
      character(len=*)                         , intent(in)  :: str_in
      character(len=*)           , optional    , intent(in)  :: sep
      character(len=len(str_in)) , allocatable , intent(out) :: str_out(:)
      integer                    , optional    , intent(in)  :: max_split
      logical                    , optional    , intent(in)  :: back
      
      integer                             :: i , lsep , lstr , seps , focc
      character(len=len(str_in)+1)        :: str_cp
      logical                             :: bck
      integer , dimension(len(str_in)+1)  :: blank_pos , blank_prev_pos , blank_start
      integer                             :: char_start , char_end , idx
      
      
      str_cp      = " "
      lstr        = len(str_in)
      str_cp      = str_in
      
      ! for the case where sep is not present (i.e. variable length whitespace
      ! as separators), see the else part of this if-construct.
      if ( present(sep) ) then
        ! string and separator length
        lsep        = len(sep)
        bck         = .false.  ! start from front
        
        if ( present(back) ) then
          if (back)   bck = .true.
        end if
        
        ! count number of occurrences of 'sep'
        seps        = 0
        if ( lsep >= 1 ) then
          do i = 1 , lstr - lsep + 1
            if (str_cp(i:i+lsep-1) == sep) seps = seps + 1
          end do
        end if
        seps        = seps + 1
        
        ! if max_split is present and smaller than seps, add 1 to seps
        if (present(max_split) .and. max_split < seps) seps = max_split + 1
        
        ! allocate str_out with length = seps + 1
        allocate(str_out(seps))
        
        str_out     = ""  ! init output string
        focc        = 1   ! first occurrence
        
        do i = 1 , seps
          
          ! find index of the next separator
          focc    = index( str_cp , sep , bck ) 
          
          ! split the str_cp in 2 parts, before and after sep
          if (focc == 0) then ! sep not found
            str_out(i)  = trim(str_cp)
            exit
          else
            ! max_split exception
            if (i==seps) then ! last separator
              str_out(i)  = trim(str_cp)
            else
              if (bck) then ! extract from back of string
                str_out(i)  = trim(str_cp(focc+lsep:))
              else          ! extract from front of string
                str_out(i)  = trim(str_cp(1:focc-1))
              end if
              !if (.not. bck) then ! start from front
              !  str_out(i)  = trim(str_cp(1:focc-1))
              !else                ! start from back
              !  str_out(i)  = trim(str_cp(focc+lsep:))
              !end if
            end if
          end if
          
          ! remove the identified part of str_cp
          if ( .not. bck) then
            str_cp(1:focc+lsep-1)   = ""
            str_cp                  = adjustl(str_cp)
          else
            str_cp(focc:) = ""
          end if
          
        end do
        
      ! no separator present, split on blanks (tabs and spaces)
      else
        
        ! 1) replace all tabs with spaces (and remove LF's and CR's)
        !    & record space positions
        blank_pos           = 0
        blank_prev_pos      = 0
        do i = 1 , lstr + 1
          if (is_separator(str_cp(i:i))) then
            ! tab or space
            str_cp(i:i)       = " "
            blank_pos(i)      = 1
          elseif (is_LF_or_CR(str_cp(i:i))) then
            ! carriage return or line feed
            str_cp(i:i)       = " "
            blank_pos(i)      = 1
          end if
        end do
        
        ! shift indices 1 forward
        blank_prev_pos            = cshift(blank_pos,-1)
        
        ! blank_start contains begin positions of characters (-1) and of
        ! white spaces (1)
        blank_start = blank_pos - blank_prev_pos
        
        
        ! make sure that when str_in starts with character
        if (str_cp(1:1) /= " ") blank_start(1) = -1
        
        char_start    = 0
        char_end      = 0
        
        idx           = count(blank_start==-1)

        
        allocate(str_out(idx))
        idx = 1 ! re-use idx
        
        ! find start and end indices of characters
        ! and put them in str_out
        ! take case of len(str) == 1 into account (with char_end >= char_start,
        ! since then, a char starts and ends at the same index).
        do i = 1 , lstr + 1
          if (blank_start(i) == -1) char_start = i
          if (blank_start(i) == 1) char_end = i-1
          if (char_start /= 0 .and. char_end /= 0 .and. char_end >= char_start) then
            ! if the current index i is a blank and the previous one was a
            ! character, then the previous index was the end of the character
            ! string we want to put in str_out(idx)
            if (char_end == i - 1 ) then
              
              str_out(idx)  = trim(str_cp(char_start:char_end))
              idx           = idx + 1
              
            end if
          end if
        end do
      end if
      !!******
      
    end subroutine string_split
    
    
    elemental subroutine string_replace ( str_in , from , to ) 
      !!****s* src/utils/m_sys_fun/string_replace
      !!  NAME
      !!    string_replace  --  replace every 'from' in 'to' in str_in
      !!  SYNOPSIS
      !!    call string_replace ( str_in , from , to )
      !!  DESCRIPTION
      !!    Replace every occurence of 'from' with 'to' in the input string 'str_in'
      !!  INPUTS
      !!    character(len=*) :: str_in
      !!  OUTPUT
      !!    character(len=*) :: str_in
      !!    character(len=1) :: from , to
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-05-24
      !!  SOURCE
      implicit none
      
      character(len=*) , intent(inout)  :: str_in
      character(len=1) , intent(in)     :: from , to
      
      integer :: i
      
      do i = 1, len(str_in)
        if ( str_in(i:i) == from ) str_in(i:i) = to
      end do
      !!******
      
    end subroutine string_replace
    
     
    elemental function string_reverse ( str_in ) result ( str_out )
      !!****f* src/utils/m_sys_fun/string_reverse
      !!  NAME
      !!    string_reverse  --  reverse the character order of the input string
      !!  SYNOPSIS
      !!    str_out = string_reverse ( str_in )
      !!  DESCRIPTION
      !!    Reverse the order of the characters of the input string str_in and
      !!    put those in the output string str_out.
      !!  INPUTS
      !!    character(len=*)            :: str_in
      !!  OUTPUT
      !!    character(len=len(str_in))  :: str_out
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2007-12-26
      !!  SOURCE
      implicit none
      
      character(len=*) , intent(in) :: str_in
      character(len=len(str_in))    :: str_out
      
      integer                       :: lstr , i , idx
      
      lstr  = len(str_in)
      
      select case (lstr)
        case (0)
          str_out = ""
          return
        case (1)
          str_out = str_in
          return
        case default
          do i = 1 , lstr
            idx = lstr - i + 1
            str_out(i:i) = str_in(idx:idx)
          end do
      end select
      !!******
      
    end function string_reverse
    
    
    elemental function string_to_upper ( str_in ) result ( str_out )
      !!****f* src/utils/m_sys_fun/string_to_upper
      !!  NAME
      !!    string_to_upper  --  convert a string to upper case characters
      !!  SYNOPSIS
      !!    str_out = string_to_upper ( str_in )
      !!  DESCRIPTION
      !!    Convert all characters of the input string str_in to upper case if
      !!    they appear in the internal character string 'lower'.
      !!  INPUTS
      !!    character(len=*)            :: str_in
      !!  OUTPUT
      !!    character(len=len(str_in))  :: str_out
      !!  SEE ALSO
      !!    src/utils/m_sys_fun/string_to_lower
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2007-12-26
      !!  SOURCE
      implicit none
      
      character(len=*) , intent(in) :: str_in
      character(len=len(str_in))    :: str_out
      
      integer :: lstr , i , idx
      
      lstr    = len(str_in)
      str_out = str_in
      
      do i = 1 , lstr
        idx = index( lower , str_in(i:i) )
        if (idx > 0) str_out(i:i) = upper(idx:idx)
      end do
      !!******
      
    end function string_to_upper
    
    
    elemental function string_to_lower ( str_in ) result ( str_out )
      !!****f* src/utils/m_sys_fun/string_to_lower
      !!  NAME
      !!    string_to_lower  --  convert a string to lower case characters
      !!  SYNOPSIS
      !!    str_out = string_to_lower ( str_in )
      !!  DESCRIPTION
      !!    Convert all characters of the input string str_in to lower case if
      !!    they appear in the internal character string 'upper'.
      !!  INPUTS
      !!    character(len=*)            :: str_in
      !!  OUTPUT
      !!    character(len=len(str_in))  :: str_out
      !!  SEE ALSO
      !!    src/utils/m_sys_fun/string_to_upper
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2007-12-26
      !!  SOURCE
      implicit none
      
      character(len=*) , intent(in) :: str_in
      character(len=len(str_in))    :: str_out
      
      integer :: lstr , i , idx
      
      lstr    = len(str_in)
      str_out = str_in
      
      do i = 1 , lstr
        idx = index( upper , str_in(i:i) )
        if (idx > 0) str_out(i:i) = lower(idx:idx)
      end do
      !!******
      
    end function string_to_lower
    
    
    subroutine string_remove ( str_in , sub_str , str_out )
      !!****s* src/utils/m_sys_fun/string_remove
      !!  NAME
      !!    string_remove  --  remove all occurrences of sub_str in str_in
      !!  SYNOPSIS
      !!    call string_remove ( str_in , sub_str )
      !!  DESCRIPTION
      !!    Remove all occurrences of 'sub_str' in 'str_in' and return
      !!    the resulting (possibly unmodified) string.
      !!  INPUTS
      !!    character(len=*)  :: str_in
      !!    character(len=*)  :: sub_str
      !!  OUTPUT
      !!    character(len=len(str_in)) :: str_out
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-06-21
      !!  SOURCE
      implicit none
      
      character(len=*)           , intent(in)   :: str_in
      character(len=*)           , intent(in)   :: sub_str
      character(len=len(str_in)) , intent(out)  :: str_out
      
      integer     :: lstr_in , lsub_str , idx , subs
      
      
      lstr_in   = len(str_in)
      lsub_str  = len(sub_str)
      str_out = str_in
      ! zero-length substring cannot be removed
      if ( lstr_in == 0 ) return
      
      ! its an error if sub_str is longer than str_in
      if ( lsub_str > lstr_in ) then
        write(*,*) "ERROR : 'sub_str' should not be longer than 'str_in'"
        str_out = str_in
        return
      end if
      
      subs = 0    ! # of substitutions
      idx  = 1    ! index of str_in where sub_str was found
      do
        ! find first occurrence of sub_str
        idx = find_substring( str_out , sub_str )
        if (idx < 1) exit
        subs = subs + 1
        str_out(idx:) = str_out(idx+lsub_str:)
      end do
      
      ! put blanks at end (to be on safe side, I don't know if a compiler
      ! must add blacks at end in the above loop)
      str_out(lstr_in-subs+1:) = ""
      !!******
      
    end subroutine string_remove
    
    
    function find_substring ( str_in , sub_str ) result ( idx )
      !!****f* src/utils/m_sys_fun/find_substring
      !!  NAME
      !!    find_substring  --  return starting index of first sub_str in str_in
      !!  SYNOPSIS
      !!    idx = find_substring ( str_in , sub_str )
      !!  DESCRIPTION
      !!    Return the starting index of sub_str,
      !!    if nonzero, positive and smaller that len(str_in), sub_str was found,
      !!    else, it was not found
      !!    only the index of the first occurrence is returned
      !!  INPUTS
      !!    character(len=*) :: str_in
      !!    character(len=*) :: sub_str
      !!  OUTPUT
      !!    integer          :: idx
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-06-22
      !!  SOURCE
      implicit none
      
      character(len=*) , intent(in) :: str_in
      character(len=*) , intent(in) :: sub_str
      integer                       :: idx
      
      integer                       :: i , lstr_in , lsub_str
      
      lsub_str  = len(sub_str)
      lstr_in   = len(str_in)
      
      idx       = 0
      if ( lsub_str < 1 ) then
        return
      elseif ( lsub_str == 1 ) then
        idx = scan( str_in , sub_str )
      end if
      
      do i = 1 , lstr_in-lsub_str+1
        if ( str_in(i:i+lsub_str-1) == sub_str ) then
          idx = i
          return
        end if
      end do
      !!******
      
    end function find_substring
    
    
    elemental function is_LF_or_CR ( c ) result ( res )
      !!****f* src/utils/m_sys_fun/is_LF_or_CR
      !!  NAME
      !!    is_LF_or_CR  --  check wether the input is a line feed or a carriage return
      !!  SYNOPSIS
      !!    res = is_LF_or_CR ( c )
      !!  DESCRIPTION
      !!    Return a logical that is true in case the input 'c' is a carriage return (CR)
      !!    or a line feed (LF). In the ascii table, 'CR' has position 10 and
      !!    'LF' has position 13.
      !!  INPUTS
      !!    character(len=1)        :: c
      !!  OUTPUT
      !!    logical                 :: res
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2007-10-16
      !!  SOURCE
      implicit none
      
      character(len=1) , intent(in)     :: c
      logical                           :: res
      
      !       carriage return        line feed
      res = ( ( c == achar(13) ) .or. ( c == achar(10) ) )
      !!******
      
    end function is_LF_or_CR
    
    
    
    elemental function is_separator ( c ) result ( res )
      !!****f* src/utils/m_sys_fun/is_separator
      !!  NAME
      !!    is_separator  --  check wether the input is a separator (space or tab)
      !!  SYNOPSIS
      !!    res = is_separator ( c )
      !!  DESCRIPTION
      !!    Return a logical that is true in case the input 'c' is a space
      !!    or a tab character. In the ascii table, 'space' has position 32 and
      !!    'tab' has position 9.
      !!  INPUTS
      !!    character(len=1)        :: c
      !!  OUTPUT
      !!    logical                 :: res
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2007-10-16
      !!  SOURCE
      implicit none
      
      character(len=1) , intent(in)     :: c
      logical                           :: res
      
      !              tab                  space
      res = ( ( c == achar(9) ) .or. ( c == achar(32) ) )
      !!******
      
    end function is_separator
    
    
    
    subroutine get_free_file_unit ( file_unit )
      !!****s* src/utils/m_sys_fun/get_free_file_unit
      !!  NAME
      !!    get_free_file_unit  --  return the first free unit >= 10
      !!  SYNOPSIS
      !!    call get_free_file_unit ( file_unit )
      !!  DESCRIPTION
      !!    Returns the first file unit >= 10 and < 100 that is not associated
      !!    with a file. If file_unit has value 0 on return, this means that
      !!    there was no free unit.
      !!    Units smaller than 10 are not returned as some of the units in the
      !!    range 1 to 10 are considered special by (some) compilers.
      !!  OUTPUT
      !!    integer         :: file_unit
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-02-05
      !!  SOURCE
      implicit none
      
      integer , intent(out) :: file_unit
      
      integer               :: i , ios
      logical               :: iop
      
      
      file_unit = 0
      
      do i = 10 , 99
        inquire ( unit = i , opened = iop , iostat = ios )
        ! no errors
        if ( ios == 0 ) then
          if ( .not. iop ) then
            file_unit = i
            return
          end if
        end if
      end do
      !!******
      
    end subroutine get_free_file_unit
    
    
    subroutine print_progress_bar( steps , i , bar_width )
      !!****s* src/utils/m_sys_fun/print_progress_bar
      !!  NAME
      !!    print_progress_bar  --  print part of a progress bar
      !!  SYNOPSIS
      !!    call print_progress_bar( steps , i , bar_width )
      !!  DESCRIPTION
      !!    Print part of a progress bar with the percentage at the left end.
      !!    If a call to this subroutine is followed by a call to
      !!    clear_progress_bar with the same arguments, the bar-part of the line
      !!    will be erased.
      !!  INPUTS
      !!    integer             :: steps
      !!    integer             :: i
      !!    integer , optional  :: bar_width
      !!  USES
      !!    iso_fortran_env
      !!  MODIFICATION HISTORY
      !!    2008-04-05 -- WvH : optional argument that sets the width of the bar
      !!  SEE ALSO
      !!    src/utils/m_sys_fun/clear_progress_bar
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-04-03
      !!  SOURCE
      use , intrinsic :: iso_fortran_env, only: output_unit
      
      implicit none
      
      integer            , intent(in) :: steps
      integer            , intent(in) :: i
      integer , optional , intent(in) :: bar_width
      
      character(len=1) , parameter    :: bar = "="
      integer                         :: k
      integer                         :: bw
      
      bw    = 50
      if ( present( bar_width ) ) bw = bar_width
      
      write( output_unit , "(2x,i3,a1,2x,a1,256a1)", advance="no" ) &
              100*i/steps, "%" , "|" , ( bar , k=1,bw*i/steps )
      close( output_unit )
      open ( output_unit )
      !!******
      
    end subroutine print_progress_bar
    
    
    subroutine clear_progress_bar ( width , i , bar_width )
      !!****s* src/utils/m_sys_fun/clear_progress_bar
      !!  NAME
      !!    clear_progress_bar  --  clear part of a progress bar
      !!  SYNOPSIS
      !!    call clear_progress_bar( width , i , bar_width )
      !!  DESCRIPTION
      !!    Clear part of a progress bar with the percentage at the left end.
      !!    A call to this subroutine should be preceded by a call to
      !!    print_progress_bar
      !!  INPUTS
      !!    integer :: width
      !!    integer :: i
      !!    integer , optional  :: bar_width
      !!  USES
      !!    iso_fortran_env
      !!  MODIFICATION HISTORY
      !!    2008-04-05 -- WvH : optional argument that sets the width of the bar
      !!  SEE ALSO
      !!    src/utils/m_sys_fun/print_progress_bar
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-04-03
      !!  SOURCE
      use , intrinsic :: iso_fortran_env, only: output_unit
      
      implicit none
      
      integer , intent(in)            :: width
      integer , intent(in)            :: i
      integer , optional , intent(in) :: bar_width
      
      character(len=1) , parameter    :: back = char(8)
      integer                         :: k
      integer                         :: bw
      
      bw    = 50
      if ( present( bar_width ) ) bw = bar_width
      
      write( output_unit , "(256a1)", advance="no" ) ( back , k=1,bw*i/width+9 )
      !!******
      
    end subroutine clear_progress_bar
    
    
    subroutine finish_progress_bar
      !!****s* src/m_sys_fun/finish_progress_bar
      !!  NAME
      !!    finish_progress_bar  --  add trailing | and close output
      !!  SYNOPSIS
      !!    call finish_progress_bar
      !!  DESCRIPTION
      !!    
      !!  USES
      !!    iso_fortran_env, only: output_unit
      !!  MODIFICATION HISTORY
      !!    2008-12-?? -- WvH : docs added
      !!  SEE ALSO
      !!    src/utils/m_sys_fun/clear_progress_bar
      !!    src/utils/m_sys_fun/print_progress_bar
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-12-??
      !!  SOURCE
      use , intrinsic :: iso_fortran_env, only: output_unit
      
      implicit none
      
      write( output_unit , "(a)" ) "|"
      close( output_unit )
      !!******
      
    end subroutine finish_progress_bar
    
    
    pure function string_pad ( str_in , width , pad_char , pad_right ) result ( str_out )
      !!****f* src/utils/m_sys_fun/string_pad
      !!  NAME
      !!    string_pad  --  pad the input string with 0's or pad_char
      !!  SYNOPSIS
      !!    str_out = string_pad ( str_in , width , pad_char , pad_right )
      !!  DESCRIPTION
      !!    (Left) pad the input string with zeros or, if supplied, pad_char,
      !!    up to a total width of width and put that into str_out.
      !!    If pad_right is supplied, the padding is done on the right side of
      !!    the input string.
      !!    If the input string is longer than the supplied width, the output
      !!    string will only contain the characters from start to width of
      !!    the input string.
      !!    This function must be pure for it to be able to call itself recursively.
      !!  INPUTS
      !!    character(len=*)            :: str_in
      !!    integer                     :: width
      !!    character(len=1) , optional :: pad_char
      !!    logical          , optional :: pad_right
      !!  OUTPUT
      !!    character(len=width)        :: str_out
      !!  MODIFICATION HISTORY
      !!    2009-02-02 -- WvH : Initial version
      !!    2009-02-03 -- WvH : renamed to string_pad, both left and right
      !!                        padding is supported
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2009-02-03
      !!  SOURCE
      implicit none
      
      character(len=*)            , intent(in)  :: str_in
      integer                     , intent(in)  :: width
      character(len=1) , optional , intent(in)  :: pad_char
      logical          , optional , intent(in)  :: pad_right
      character(len=width)                      :: str_out
      
      integer                                   :: lstr , s , i
      character(len=1)                          :: pad_c
      logical                                   :: pad_r
      
      ! check pad character, if not present, it defaults to zeros (0)
      pad_c = "0"
      if ( present(pad_char) ) pad_c = pad_char
      
      pad_r = .false.
      if ( present(pad_right) ) then
        if ( pad_right ) pad_r = .true.
      end if
      
      ! width of input string
      lstr = len(str_in)
      
      ! if width < lstr, part of str_in (str_in(width+1:lstr)) will be lost
      if ( width <= lstr ) then
        str_out(1:width)  = str_in(1:width)
        return
      end if
      
      ! normal cases, width > len(str_in)
      if ( .not. pad_r ) then ! left pad
        s                   = width - lstr
        forall(i=1:s) str_out(i:i) = pad_c
        !str_out(1:s)        = pad_c
        str_out(s+1:width)  = str_in(1:lstr)
      else  ! right pad
        str_out(1:lstr) = str_in(1:lstr)
        forall(i=lstr+1:width) str_out(i:i) = pad_c
      end if
      !!******
      
    end function string_pad
    
      
    subroutine determine_eol ( eol , verbose )
      !!****s* src/utils/m_sys_fun/determine_eol
      !!  NAME
      !!    determine_eol  --  determine end-of-line character used
      !!  SYNOPSIS
      !!    call determine_eol ( eol , verbose )
      !!  DESCRIPTION
      !!    Return a string that contains the name of the end-of-line character
      !!    used by the current OS to end a line.
      !!  INPUTS
      !!    logical , optional :: verbose
      !!  OUTPUT
      !!    character(len=4)   :: eol
      !!  MODIFICATION HISTORY
      !!    2008-12-05 -- WvH : test program converted to a subroutine
      !!  AUTHOR
      !!    Wim Van Hoydonck
      !!  CREATION DATE
      !!    2008-12-05
      !!  SOURCE
      implicit none
      
      character(len=4)   , intent(out)  :: eol
      logical , optional , intent(in)   :: verbose
      
      character(len=1)                  :: single_char = "a"
      integer                           :: ios
      integer(kind=1)                   :: n1 , n2 , n3
      logical                           :: verb
      
      n1   = 0
      n2   = 0
      n3   = 0
      verb = .false.
      
      ! process optional arguments
      if ( present( verbose ) ) then
        if (verbose) verb = .true.
      end if
      
      ! open a file, write a single character and close it again
      open(unit=10,file="eol.out",status="unknown")
      write(10,"(a)") single_char
      close(10)
      
      ! open the file unformatted, stream
      open(unit=11,file="eol.out",status="old",form="unformatted",access="stream",iostat=ios)
      
      ! read first character (should be an a)
      read(11,iostat=ios) n1 ! should be 97 (ascii a)
      if ( ios < 0 ) then
        if (verb) print *, "end of file reached after 0 characters"
      else
        ! read second character, should be lf (ascii 10) on dos and unix,
        ! and cr on mac (ascii 13)
        read(11,iostat=ios) n2
        if ( ios < 0 ) then
          ! this should not happen now, there are at least 2 characters in the file
          if (verb) print *, "end of file reached after 1 character"
        else
          ! read third character if we're on dos
          read(11,iostat=ios) n3
          if ( ios < 0 ) then
            if (verb) print *, "end of file reached after 2 characters"
          end if
        end if
      end if
      
      ! close the file again
      close(11)
      
      ! analyse n2 and n3 to determine end-of-line character
      if ( n2 == 10 .and. n3 == 0 ) then
        if (verb) print *, "unix system : end-of-line = LF"
        eol = "LF  "
      elseif ( n2 == 13 .and. n3 == 10 ) then
        if (verb) print *, "dos system : end-of-line = CRLF"
        eol = "CRLF"
      elseif ( n2 == 13 .and. n3 == 0 ) then
        if (verb) print *, "mac system : end-of-line = CR"
        eol = "CR  "
      else
        if (verb) print *, "ERROR : something is wrong, your compiler did not add an end-of-line character to the file 'eol.out'"
        if (verb) print *, "ERROR : cannot determine end-of-line character"
        eol = "    "
      end if
      !!******
      
    end subroutine determine_eol
    
end module m_sys_fun

