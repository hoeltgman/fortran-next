if ( ENABLE_GUI )
    add_subdirectory ( f03gl )
endif (ENABLE_GUI )

if ( ENABLE_TESTS )
    add_subdirectory ( fruit )
endif ( ENABLE_TESTS )

add_subdirectory ( pixfiles )
add_subdirectory ( m_option_parser )
