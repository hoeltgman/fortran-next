# README #

### What is this repository for? ###

Mostly Fortran implementation of https://link.springer.com/chapter/10.1007/978-3-642-40395-8_12 and
https://link.springer.com/chapter/10.1007/978-3-319-14612-6_7 as well as related algorithms.

### Dependencies and Requirements ###

* Necessary Tools

- A fortran compiler whith support for the 2018 standard.

  Development is done with [gfortran](https://gcc.gnu.org/wiki/GFortran) (usually the latest stable
  release). Compilation is done with Fortran 2018 standard flag. Fortran 2008 is probably enough,
  though.

- fypp <https://github.com/aradi/fypp> :

  [fypp](https://github.com/aradi/fypp) is a preprocessor for Fortran files. It is used to generate
  the code for various types of precisions. It can easily be installed through python pip.

  ```
  pip install fypp
  ```

  Version 2.1 should work

- cmake <https://cmake.org/> :

  [cmake](https://cmake.org/) is used to build the software.

  Version 3.15 should work.

- vcpkg <https://vcpkg.readthedocs.io/en/latest/>

  The code has several external dependencies which can easily be installed
  with [vcpkg](https://github.com/microsoft/vcpkg). Using other package managers might work as well
  if [cmake](https://cmake.org/) can find the dependencies. Development is done with the latest
  available package versions from [vcpkg](https://github.com/microsoft/vcpkg).

- ford <https://github.com/cmacmackin/ford> :

  The documentation is automatically extracted from the Fortran source code files with
  [ford](https://github.com/cmacmackin/ford). It can be installed with (python) pip.

  ```
  pip install ford
  ```

  Version 6.0.0 should work

* Optional Tools

- gcovr <http://gcovr.com/> :

  Code coverage

- fortls <https://github.com/hansec/fortran-language-server> :

  [fortls](https://github.com/hansec/fortran-language-server) is a Fortran language server. Not
  really needed for compiling the code but very helpful for working with the code.
  [fortls](https://github.com/hansec/fortran-language-server) can be used e.g. in conjunction with
  [emacs](https://www.gnu.org/software/emacs/) and [VS Code](https://code.visualstudio.com/).

* Dependencies

- [fruit](https://sourceforge.net/projects/fortranxunit/)

  For unit testing. [fruit](https://sourceforge.net/projects/fortranxunit/) v.3.4.3 is integrated
  into the repository. fruit is released under a
  [BSD 3 clause License](https://opensource.org/licenses/BSD-3-Clause)

- [OpenGL](https://www.opengl.org/)/[freeGLUT](http://freeglut.sourceforge.net/)

  For showing the results. Some functions from [freeGLUT](http://freeglut.sourceforge.net/) are
  used, too. [freeGLUT](http://freeglut.sourceforge.net/) is released under the X-Consortium
  license.

- [OpenBLAS](https://www.openblas.net/) (or any other
  [BLAS](https://en.wikipedia.org/wiki/Basic_Linear_Algebra_Subprograms) and
  [LAPACK](https://en.wikipedia.org/wiki/LAPACK) library)

- f03gl <http://www-stone.ch.cam.ac.uk/pub/f03gl/index.xhtml> or
  <https://www.12000.org/my_notes/faq/fortran/index.htm>

  Code is included in libs/f03gl. [f03gl](http://www-stone.ch.cam.ac.uk/pub/f03gl/index.xhtml)
  is licensed [GPLv3](https://opensource.org/licenses/GPL-3.0). Requires freeglut or openglut

- libpixfiles <https://sourceforge.net/projects/libpixfiles/files/libpixfiles/libpixfiles-1.1.0b/> :

  The software can currently only read/write pnm (pbm, pgm, and ppm) files by using
  [pixfiles](https://sourceforge.net/projects/libpixfiles/files/libpixfiles/libpixfiles-1.1.0b/).
  Code is included in libs/pixfiles. Downloaded version 1.1.0b on 2017-07-13. Licensed
  [LGPLv3](https://opensource.org/licenses/lgpl-license)

- suitesparse <http://faculty.cse.tamu.edu/davis/suitesparse.html> :

  UMFPACK is used to solve the occurring linear systems. Any recent version should work.

  Licensed [BSD 3 clause License](https://opensource.org/licenses/BSD-3-Clause),
  [(L)GPLv2.1 or later](https://opensource.org/licenses/lgpl-license)

- option_parser <http://users.telenet.be/tuinbels/fortran/m_option_parser.html>

  option_parser is integrated into the repository in libs/m_option_parser. It is used to parse the
  command line parameters for the command line programs.

  Licensed under [BSD 3 clause License](https://opensource.org/licenses/BSD-3-Clause).

* Summary of set up

- install vcpkg
- install all dependencies with vcpkg (openBLAS, suitesparse, freeGLUT). freeGLUT might require
  additional packages on your system that you should install as well.
- install fypp and ford. This step requires python and pip installed on your system.
- checkout code from [gitlab](https://gitlab.com/hoeltgman/fortran-next).
- go to root folder of repository and enter

```
mkdir build
cmake ../ -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_TOOLCHAIN_FILE=~/vcpkg/scripts/buildsystems/vcpkg.cmake -DCMAKE_BUILD_TYPE=DEBUG
cmake --build .
```

- The unit tests can be run from bin/tests/ with

```
ctest --progress -VV -C Debug
```

### Who do I talk to? ###

* Laurent Hoeltgen <contact@laurenthoeltgen.name>
